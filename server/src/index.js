const express = require('express')
const favicon = require('serve-favicon')
const router = require('./routes')
const path = require('path')

const app = express()

const STATIC_DIR = process.env.STRATO_STATIC_FOLDER
const PORT = process.env.STRATO_PORT || 8080

// Security best practices
// -----------------------
// https://expressjs.com/en/advanced/best-practice-security.html

// Do not tell we are using Express.
app.disable('x-powered-by')

// -----------------------
// Security best practices END

// Serve favicon
app.use(favicon(path.join(__dirname, 'client', 'favicon.ico')))

// Heartbeat endpoint for checking server is live
app.get('/healthz', (req, res) => {
  return res.sendStatus(200)
})

// Serve static assets
app.use('/static', express.static(STATIC_DIR))

// Server HTTP routes here after static asset routing.
app.use('/', router)

// Start the server
app.listen(PORT, () => {
  console.log(`Express listening on port ${PORT}`)
})
