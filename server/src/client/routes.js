const handlers = require('./handlers')
const express = require('express')
const router = express.Router()

// Route all else to the client app
router.get('/*', handlers.get)

module.exports = router
