const ejs = require('ejs')
const fs = require('fs')
const path = require('path')
const config = require('../config')

const precompile = () => {
  // Precompile templates and index.html once at the server start.
  // Include config and other variables for the client.

  const p = path.resolve(__dirname, './template.ejs')
  const f = fs.readFileSync(p, 'utf8')
  const template = ejs.compile(f)

  // Pass public configuration
  const stratocumulus = {
    title: 'LINCS Explorer',
    // Facet nodes smaller than this will show entity facets
    categoryThreshold: 25,
    // Content type indices available in the API
    contentTypes: config.contentTypes
  }

  return template({ stratocumulus })
}

const indexHtml = precompile()

// Serve the client page.
exports.get = (req, res) => {
  // Send precompiled index page.
  return res
    .status(200)
    .send(indexHtml)
}
