const fs = require('fs')
const path = require('path')
const YAML = require('yaml')

// Corpus data model
const cp = path.resolve(__dirname, '..', '..', 'corpus-config.yml')
const cf = fs.readFileSync(cp, 'utf8')

exports.contentTypes = YAML.parse(cf)
exports.contentTypeMap = exports.contentTypes.reduce((acc, c) => {
  acc[c.content_type] = c
  return acc
}, {})

// Corpus access
const CORPORA_HOST = process.env.STRATO_CORPORA_HOST
const CORPUS_ID = process.env.STRATO_CORPORA_CORPUS_ID
// Redis access
const REDIS_URL = process.env.REDIS_URL || 'redis://redis'
const REDIS_CACHE_TTL = process.env.REDIS_CACHE_TTL || 3600 // seconds

exports.corporaHost = CORPORA_HOST
exports.corpusId = CORPUS_ID
exports.corporaUiUrl = CORPORA_HOST
exports.corporaApiUrl = (new URL('api/', CORPORA_HOST)).href
exports.corporaCorpusApiUrl = (new URL('api/corpus/' + CORPUS_ID + '/', CORPORA_HOST)).href
exports.corporaCorpusUiUrl = (new URL('corpus/' + CORPUS_ID + '/', CORPORA_HOST)).href

exports.redisUrl = REDIS_URL
exports.redisCacheDuration = REDIS_CACHE_TTL
