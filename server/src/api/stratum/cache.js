// This module handles reading and writing cached stratum objects.
// Cache operations should be non-blocking upon network issues
// and therefore they should not wait if cache is unavailable.
//
const redis = require('../../redis')

exports.read = async (key) => {
  // Read a cached stratum object if any.
  //
  // Parameters:
  //   key
  //     a string, the stratum key
  //
  // Return:
  //   an object or null if not found or not available.
  //
  const cacheKey = 'strato_cache_' + key
  let cacheResult = null

  try {
    const redisClient = await redis.client()
    cacheResult = await redisClient.get(cacheKey)
  } catch (error) {
    // Issue with cache. Continue with full stratum build but log an error.
    console.warn('Failed to read Redis cache: ' + error.message)
  }

  return cacheResult
}

exports.write = async (key, stratum) => {
  // Cache a stratum object.
  //
  // Parameters:
  //   key
  //     a string, the stratum key
  //   stratum
  //     an object, the stratum object
  //
  const cacheKey = 'strato_cache_' + key

  // Mark for caching. Use Object.assing to prevent side effects.
  const cacheObject = Object.assign({}, stratum, {
    provenance: 'cache'
  })

  const redisClient = await redis.client()

  try {
    await redisClient.set(cacheKey, JSON.stringify(cacheObject), {
      EX: redis.cacheDuration, // cache duration in seconds
      NX: true // only set if does not exist
    })
  } catch (error) {
    // We cannot anymore respond to client with HTTP 500 or similar.
    // The best we can do is to log an error.
    console.warn('Failed to write to Redis cache: ' + error.message)
  }
}
