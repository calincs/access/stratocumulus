const config = require('../../config')
const fetchCount = require('./fetchCount')
const queryHelpers = require('./queryHelpers')
const cache = require('./cache')

module.exports = async (req, res) => {
  // Get a prospect on supported content types.
  //

  // Check for quick cached response.
  const stratumKey = queryHelpers.toStratumKey(req.query)
  const cacheResult = await cache.read(stratumKey)
  if (cacheResult) {
    return res.type('json').send(cacheResult)
  }
  // Otherwise continue with full stratum build.

  const activeFilters = {}

  // Count entities in each content type
  const tasks = config.contentTypes.map(typeConfig => {
    const entityType = typeConfig.content_type
    const task = fetchCount(entityType, activeFilters)
      .then(count => {
        return { entityType, count }
      })
    return task
  })

  // Execute in parallel
  let counts
  try {
    counts = await Promise.all(tasks)
  } catch (err) {
    console.warn('Unable to fetch counts. ' + err.message)
    const statusCode = err.statusCode || 500
    return res.sendStatus(statusCode)
  }

  // Total number of entities.
  // TODO This might be misleading for users?
  const total = counts.reduce((acc, countItem) => {
    return acc + countItem.count
  }, 0)

  // Build count map for easier access.
  const countMap = counts.reduce((acc, countItem) => {
    acc[countItem.entityType] = countItem.count
    return acc
  }, {})

  // Build a facet node for each content type.
  const nodes = config.contentTypes.map(typeConfig => {
    return {
      id: typeConfig.id,
      role: 'facet',
      type: 'ContentType',
      field: 'content_type',
      contentType: typeConfig.content_type,
      label: typeConfig.label_plural,
      labelPrefix: 'Type',
      labelPostfix: null,
      count: countMap[typeConfig.content_type] || 0,
      facetParam: 'content_type',
      facetValue: typeConfig.content_type,
      parent: 'self'
    }
  })

  const stratum = {
    path: stratumKey,
    provenance: 'corpora',
    count: total,
    label: 'Entities',
    context: [], // TODO respect filters, e.g keyword search 'q'
    nodes,
    edges: [],
    layout: 'polar'
  }

  // We cache the result after response. No need for the client to wait.
  res.json(stratum)
  await cache.write(stratumKey, stratum)
}
