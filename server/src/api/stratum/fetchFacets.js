const config = require('../../config')
const slugify = require('slugify')
const baseUrl = config.corporaCorpusApiUrl

const aggregationValue = (field, xrefType) => {
  if (xrefType) {
    return `${field}.id,${field}.label.raw`
  }
  return field
}

module.exports = async (contentType, filters, field, xrefType) => {
  // Fetch counts of entities when aggregated over the values of the given field.
  //
  // Parameters:
  //   contentType
  //     a string
  //   filters
  //     an object
  //   field
  //     a string, a column name.
  //   xrefType
  //     a string
  //       The content type of the cross referenced entity.
  //       Empty string if the field is not modeled as a cross reference.
  //
  // Return
  //   a Promise, resolved with an array of facet objects:
  //     id
  //       a string, the URL-friendly identity of the facet.
  //     value
  //       a string, the exact facet value.
  //     label
  //       a string, the human-readable name for the facet.
  //     count
  //       an integer
  //
  const targetUrl = new URL(contentType, baseUrl)

  // Apply filters
  for (const key in filters) {
    targetUrl.searchParams.append(key, filters[key])
  }

  // Aggregation a'la Corpora
  const aggrParam = 'a_terms_' + field
  const aggrValue = aggregationValue(field, xrefType)
  targetUrl.searchParams.append(aggrParam, aggrValue)

  console.log('api.stratum.fetchFacets: fetch', targetUrl.href)
  const facetsPromise = fetch(targetUrl)
    .then(response => {
      if (response.status === 200) {
        return response.json()
      }
      let err
      if (response.status === 404) {
        err = new Error('Not found')
        err.statusCode = 404
      } else if (response.status === 500) {
        err = new Error('Error response from Corpora')
        err.statusCode = 502
      } else {
        err = new Error('Unexpected response from Corpora')
        err.statusCode = response.status
      }
      throw err
    })
    .then(data => {
      // Select aggregations
      if (data && data.meta && data.meta.aggregations) {
        if (data.meta.aggregations[field]) {
          return data.meta.aggregations[field]
        }
      }
      return {}
    })
    .then(fieldMap => {
      // Convert to array
      return Object.keys(fieldMap).map(fieldValue => {
        if (xrefType) {
          const parts = fieldValue.split('|||')
          return {
            id: parts[0], // corpus-specific id
            value: parts[0],
            label: parts[1],
            count: fieldMap[fieldValue]
          }
        }
        // no xref
        return {
          id: slugify(fieldValue, { trim: false }), // no trim to preserve negative years
          value: fieldValue,
          label: fieldValue,
          count: fieldMap[fieldValue]
        }
      })
    })

  return facetsPromise
}
