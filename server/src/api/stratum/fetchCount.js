const config = require('../../config')
const baseUrl = config.corporaCorpusApiUrl

module.exports = (contentType, filters) => {
  // Fetch a count of matching rows of the given content type.
  //
  // Parameters:
  //   contentType
  //     a string
  //   filters
  //     an object
  //
  // Return
  //   a Promise
  //
  const targetUrl = new URL(contentType, baseUrl)

  for (const key in filters) {
    targetUrl.searchParams.append(key, filters[key])
  }
  // No results, just the count.
  targetUrl.searchParams.append('page-size', 0)

  console.log('api.stratum.fetchCount: fetch', targetUrl.href)
  const countPromise = fetch(targetUrl)
    .then(response => {
      if (response.status === 200) {
        return response.json()
      }
      let err
      if (response.status === 404) {
        err = new Error('Nothing found at ' + targetUrl.href)
        err.statusCode = 404
      } else if (response.status === 500) {
        err = new Error('Error response from Corpora for ' + targetUrl.href)
        err.statusCode = 502
      } else {
        err = new Error('Unexpected response from Corpora for ' + targetUrl.href)
        err.statusCode = response.status
      }
      throw err
    })
    .then(data => {
      if (data && 'meta' in data) {
        if ('total' in data.meta) {
          return data.meta.total
        }
      }
      // Invalid response.
      const err = new Error(`Bad response from Corpora for ${targetUrl.href}`)
      err.statusCode = 502
      throw err
    })

  return countPromise
}
