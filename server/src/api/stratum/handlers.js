const getTypeProspect = require('./getTypeProspect')
const getEntityProspect = require('./getEntityProspect')
const getFacetProspect = require('./getFacetProspect')

exports.get = async (req, res) => {
  // Get a prospect on something.
  //

  if (!('content_type' in req.query)) {
    // For all content types.
    return getTypeProspect(req, res)
  }

  if ('entity' in req.query) {
    // Prospect on single entity
    return getEntityProspect(req, res)
  }

  // Prospect on entities of given content type
  return getFacetProspect(req, res)
}
