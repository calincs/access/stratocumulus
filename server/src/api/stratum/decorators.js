exports.dateToYear = (date) => {
  // Parameters:
  //   date
  //     a string, ISO date
  //
  // Returns
  //   a string, the year
  //
  return date.substring(0, 4)
}

exports.yearsToRange = (years) => {
  // Compact range from array of years.
  //
  // Parameter:
  //   years
  //     an array of integers
  //
  // Return
  //   a string
  //
  if (years.length === 0) {
    return '-'
  }
  if (years.length === 1) {
    return '' + years[0]
  }

  // Detect range
  let minYear = years[0]
  let maxYear = years[years.length - 1]

  for (let i = 0; i < years.length; i += 1) {
    if (years[i] < minYear) {
      minYear = years[i]
    }
    if (years[i] > maxYear) {
      maxYear = years[i]
    }
  }

  if (minYear < 0 && maxYear < 0) {
    return Math.abs(minYear) + 'BC–' + Math.abs(maxYear) + 'BC'
  } else if (minYear < 0) {
    return Math.abs(minYear) + 'BC–' + maxYear + 'AD'
  } else {
    return minYear + '–' + maxYear
  }
}
