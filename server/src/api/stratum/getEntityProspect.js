const config = require('../../config')
const fetchContext = require('./fetchContext')
const fetchEntity = require('../entity/fetchEntity')
const fetchRelatedEntities = require('../entity/fetchRelatedEntities')
const queryHelpers = require('./queryHelpers')
const entitiesToNodes = require('./entitiesToNodes')

module.exports = async (req, res) => {
  // Get a prospect on stuff associated with an entity.
  //

  // Check for quick cached response.
  const stratumKey = queryHelpers.toStratumKey(req.query)
  // TODO enable cache
  // const cacheResult = await cache.read(stratumKey)
  // if (cacheResult) {
  //   return res.type('json').send(cacheResult)
  // }
  // Otherwise continue with full stratum build.

  // Ensure entity is fully specified.
  if (!('entity' in req.query)) {
    return res.status(400).send('Missing query parameter: entity')
  }
  const entitySelector = queryHelpers.pickLast(req.query, 'entity')
  const entityIdentifier = queryHelpers.extractEntitySelector(entitySelector)
  if (!entityIdentifier) {
    return res.status(400).send('Invalid query value for entity: ' + entitySelector)
  }
  const entityId = entityIdentifier.id
  const entityType = entityIdentifier.type

  // Ensure valid type
  const typeConfig = config.contentTypeMap[entityType]
  if (!typeConfig) {
    return res.status(400).send('Unknown content type: ' + entityType)
  }

  // Find context labels to return with the stratum.
  let contextCandidate
  try {
    contextCandidate = await fetchContext(req.query)
  } catch (err) {
    // TODO detect bad gateway
    console.warn('Unable to fetch context: ' + err.message)
    // Fall back to empty context.
    contextCandidate = []
  }
  const context = contextCandidate

  // We first need to fetch the entity to get its relations.
  let entity
  try {
    entity = await fetchEntity(entityType, entityId)
  } catch (err) {
    console.warn('Unable to fetch entity: ' + err.message)
    const statusCode = err.statusCode || 500
    return res.sendStatus(statusCode)
  }

  // Fetch related entities.
  let related
  try {
    related = await fetchRelatedEntities(entity)
  } catch (err) {
    console.warn('Unable to fetch related entities: ' + err.message)
    const statusCode = err.statusCode || 500
    return res.sendStatus(statusCode)
  }

  // Build nodes.
  let nodes = []

  // Group related entities to nodes.
  // Generate grouping nodes.
  const groupingNodes = related.reduce((acc, relent) => {
    const groupKey = relent.relationshipLabel
    if (groupKey in acc) {
      acc[groupKey].count += 1
    } else {
      const targetType = relent.content_type
      acc[groupKey] = {
        id: groupKey,
        role: 'grouping',
        type: targetType + 'Group',
        field: 'relationship',
        contentType: targetType,
        label: relent.relationshipLabel,
        labelPrefix: relent.relationshipLabelPrefix,
        labelPostfix: null,
        count: 1, // first entity
        parent: 'self'
      }
    }
    return acc
  }, {})
  // Join grouping nodes to nodes
  Object.keys(groupingNodes).forEach((key) => {
    const node = groupingNodes[key]
    nodes.push(node)
  })

  // Convert related entities to facet nodes
  const getParentNodeId = ent => ent.relationshipLabel
  const facetNodes = entitiesToNodes(related, getParentNodeId)
  // Join to nodes
  nodes = nodes.concat(facetNodes)

  // Convert facetable entity properties to nodes.
  typeConfig.facets.forEach(facetConfig => {
    // Pick facetable entity property into array.
    let property = entity[facetConfig.field] || []
    if (!Array.isArray(property)) {
      property = [property]
    }

    // Prevent empty property groups.
    if (property.length === 0) {
      return
    }

    const groupingNode = {
      id: facetConfig.id,
      role: 'grouping',
      type: facetConfig.label + 'Group', // TODO rename label to content_type
      field: facetConfig.field,
      contentType: facetConfig.label, // TODO rename label to content_type
      label: (property.length > 1 ? facetConfig.label_plural : facetConfig.label),
      labelPrefix: 'Found in',
      labelPostfix: null,
      count: property.length,
      parent: 'self'
    }

    nodes.push(groupingNode)

    // Create facet nodes.
    property.forEach(fieldItem => {
      let id, label, type, facetValue
      if (typeof fieldItem === 'object') {
        // Is a cross reference object.
        id = groupingNode.id + '/' + fieldItem.id
        label = fieldItem.label
        type = fieldItem.content_type
        facetValue = fieldItem.id
      } else {
        // Is a string value.
        id = groupingNode.id + '/' + fieldItem
        label = '' + fieldItem
        type = facetConfig.label // TODO rename label to content_type
        facetValue = fieldItem
      }

      const facetNode = {
        id,
        role: 'warp',
        type,
        field: facetConfig.field,
        contentType: type,
        label,
        labelPrefix: facetConfig.label,
        labelPostfix: null,
        count: -1, // -1 = unknown count
        parent: facetConfig.id,
        facetParam: facetConfig.parameter,
        facetValue
      }

      nodes.push(facetNode)
    })
  })

  // Sort the nodes so that
  // 1. the grouping nodes come first. This helps in layout hierarchy.
  // 2. nodes with higher counts come first.
  // 3. alphabetically
  nodes.sort((a, b) => {
    return (a.role !== b.role ? (a.role === 'grouping' ? -1 : 1) : 0) ||
      b.count - a.count || a.label.localeCompare(b.label)
  })

  // Pack into stratum
  const stratum = {
    path: stratumKey,
    provenance: 'corpora',
    count: nodes.length,
    label: entity.label,
    context,
    nodes,
    edges: [],
    layout: 'grouped-circlepack'
  }

  return res.json(stratum)
}
