// Valid query parameters
const extraParams = ['content_type', 'entity', 'facet_threshold']
const pageParams = ['page-size', 'page']
const corporaParams = ['q']
const corporaParamPrefixes = ['q_', 'f_', 'r_']

exports.pickCorporaFilters = (query) => {
  // Map stratocumulus filter parameters to corpora query parameters.
  //
  // Return
  //   an object
  //
  return Object.keys(query).reduce((params, key) => {
    if (corporaParams.includes(key) || corporaParamPrefixes.includes(key.substring(0, 2))) {
      params[key] = query[key]
    }
    return params
  }, {})
}

exports.pickFilters = (query) => {
  // Pick all known stratocumulus filters from a query object.
  return Object.keys(query).reduce((params, key) => {
    if (extraParams.includes(key) ||
        pageParams.includes(key) ||
        corporaParams.includes(key) ||
        corporaParamPrefixes.includes(key.substring(0, 2))) {
      params[key] = query[key]
    }
    return params
  }, {})
}

exports.pickLast = (query, parameter) => {
  // Handle query values that can be strings or arrays.
  // Returns the string or the last string item of the array.
  // If array is empty, returns null.
  //
  const value = query[parameter]

  // Express.js parses duplicate parameters into array. Pick last.
  if (Array.isArray(value)) {
    const len = value.length
    if (len === 0) {
      return null
    }
    return value[len - 1]
  }

  // Express.js can parse square bracket subparameters into objects.
  if (typeof value === 'object') {
    const subargs = Object.values(value)
    const len = subargs.length
    if (len === 0) {
      return null
    }
    return subargs[len - 1]
  }

  // Likely a string.
  return value
}

exports.toStratumKey = (query) => {
  // Get a canonical stratum identifier key from Express' request.query
  //

  // Pick known and relevant parameters into array.
  // Relevant parameters are those that affect stratum content.
  // This prevents some random parameter causing cache miss.
  const relevantKeys = Object.keys(query).reduce((acc, key) => {
    if (extraParams.includes(key) ||
        pageParams.includes(key) ||
        corporaParams.includes(key) ||
        corporaParamPrefixes.includes(key.substring(0, 2))) {
      acc.push(key)
    }
    return acc
  }, [])

  // Previously,
  // for canonicality, we sorted the keys in alphabetical order.
  // This way two queries with different order of parameters
  // would not produce duplicate cache entries.
  // We used the default order: ascending raw UTF-16 code,
  // so that a simple `relevantKeys.sort()` did the job.
  // However, later, we found that we want to keep the order
  // if possible because it is meaningful for the client.

  // Let URLSearchParams handle escaping and such.
  const params = new URLSearchParams()
  for (let i = 0; i < relevantKeys.length; i += 1) {
    const key = relevantKeys[i]
    const value = query[key]
    params.append(key, value)
  }

  // We add slash by project convention. TODO maybe remove slash for simpler code.
  const qs = params.toString()
  if (qs.length > 0) {
    return '/?' + params.toString()
  }
  return '/'
}

exports.extractEntitySelector = (queryValue) => {
  // Extract entity type and id from the query value.
  //
  // Parameters:
  //   queryValue
  //     a string, e.g. 'Thing-6595be681a18712f1f6b135b'
  //
  // Return:
  //   an object { type, id } or null if not valid.
  //
  const parts = queryValue.split('-')
  if (parts.length < 2) {
    return null
  }
  return {
    type: parts[0],
    id: parts[1]
  }
}
