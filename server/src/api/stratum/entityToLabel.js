module.exports = (entity) => {
  // Build a human-readable label for entity.
  //
  // Parameters:
  //   entity
  //     a corpora entity object
  //
  // Return
  //   a string
  //

  let label = '-'
  if (entity.names) {
    // Pick the longest name available.
    label = entity.names.reduce((acc, name) => {
      return acc.length > name.length ? acc : name
    }, label)
  } else if (entity.name) {
    label = entity.name
  }

  return label
}
