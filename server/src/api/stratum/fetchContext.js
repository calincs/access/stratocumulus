const config = require('../../config')
const fetchEntity = require('../entity/fetchEntity')
const queryHelpers = require('./queryHelpers')

module.exports = (filters) => {
  // @stratum.fetchContext(filters)
  //
  // Fetch labels and other user readable stuff for active filters.
  // This enables clients to display the current faceting context
  // without cryptic id numbers and parameters.
  //
  // Parameters:
  //   contentType
  //     a string
  //   filters
  //     an object, Express.js req.query object.
  //
  // Return
  //   a Promise, resolved with an array of objects:
  //     { parameter, value, parameterLabel, valueLabel, active, imageUrl }
  //

  // Remove unwanted filters
  filters = queryHelpers.pickFilters(filters)

  // Collect filter label data here.
  const context = []

  // Find content type
  const contentType = filters.content_type

  // Select on or many types from which to search facet labels.
  let typeConfigs = []
  if (contentType) {
    // Select one if available.
    const typeConfig = config.contentTypeMap[contentType]
    if (typeConfig) {
      typeConfigs = [typeConfig]
    }
  } else {
    // No type specified. Select all.
    typeConfigs = config.contentTypes
  }

  // Handle unknown content type.
  if (typeConfigs.length === 0) {
    return Promise.resolve(context)
  }

  // Construct a filter entry for the only type.
  if (typeConfigs.length === 1) {
    const tc = typeConfigs[0]
    context.push({
      parameter: 'content_type',
      value: tc.content_type,
      parameterLabel: 'Type',
      valueLabel: tc.label_singular,
      imageUrl: tc.image_url || null
    })
  }

  // Gather all facet configs.
  const facetConfigs = typeConfigs.reduce((acc, tc) => {
    return acc.concat(tc.facets)
  }, [])

  // For non-xref filters, we can just use the value as the label.
  // For xref filters, we need to get their labels from database.
  const contextToFetch = []
  for (const parameter in filters) {
    const facetConfig = facetConfigs.find(f => f.parameter === parameter)
    if (facetConfig) {
      if (facetConfig.cross_reference_type) {
        // We need to fetch the entity name for the label.
        const type = facetConfig.cross_reference_type
        const id = filters[parameter]
        const parameterLabel = facetConfig.label
        contextToFetch.push({ parameter, type, id, parameterLabel })
      } else {
        // No need to fetch. Construct already.
        context.push({
          parameter,
          value: filters[parameter],
          parameterLabel: facetConfig.label,
          valueLabel: filters[parameter],
          imageUrl: null // falls back to content type image
        })
      }
    } else if (parameter === 'entity') {
      // Can be an array
      let entitySelectors
      if (Array.isArray(filters.entity)) {
        entitySelectors = filters.entity
      } else {
        entitySelectors = [filters.entity]
      }
      // For each entity, prepare a fetch.
      entitySelectors.forEach(selector => {
        const typeId = queryHelpers.extractEntitySelector(selector)
        if (typeId) {
          const type = typeId.type
          const id = typeId.id
          const parameterLabel = typeId.type
          contextToFetch.push({ parameter, type, id, parameterLabel })
        }
        // else invalid entity selector, skip.
      })
    }
  }

  // Convert to tasks
  const tasks = contextToFetch.map(l => {
    return fetchEntity(l.type, l.id)
      .then(entity => {
        let valueLabel = entity.label
        // HACK prettify entity label if they have name or names.
        if (entity.names && entity.names.length > 0) {
          // Pick the longest name available.
          valueLabel = entity.names.reduce((acc, name) => {
            return acc.length > name.length ? acc : name
          }, '')
        } else if (entity.name) {
          valueLabel = entity.name
        }

        // Pick an image if available.
        let imageUrl = null
        if (entity.images) {
          if (Array.isArray(entity.images)) {
            if (entity.images.length > 0) {
              imageUrl = entity.images[0]
            }
          } else if (typeof entity.images === 'string') {
            imageUrl = entity.images
          }
        }

        // Pick a label for entity type if available.
        const typeConfig = config.contentTypeMap[l.type]
        let typeLabel = null
        if (typeConfig) {
          typeLabel = typeConfig.label_singular || null
        }

        // Serve the full entity.
        const details = entity

        return {
          parameter: l.parameter,
          value: l.id,
          parameterLabel: l.parameterLabel,
          valueLabel,
          type: l.type,
          typeLabel,
          imageUrl,
          details
        }
      })
  })

  // Wait for tasks to finish.
  return Promise.all(tasks)
    .then(fetchedContext => {
      // Join the context
      fetchedContext.forEach(c => context.push(c))

      // Decide context activity.
      // If context is under entity, only the entity filter is active.
      const entityPresent = context.some(c => c.parameter === 'entity')
      if (entityPresent) {
        context.forEach(c => { c.active = c.parameter === 'entity' })
      } else {
        context.forEach(c => { c.active = true })
      }

      return context
    })
}
