const config = require('../../config')
const fetchContext = require('./fetchContext')
const fetchCount = require('./fetchCount')
const fetchFacets = require('./fetchFacets')
const queryHelpers = require('./queryHelpers')
const fetchEntities = require('../entity/fetchEntities')
const entitiesToNodes = require('./entitiesToNodes')
const upgradeContextImage = require('./upgradeContextImage')
const cache = require('./cache')

module.exports = async (req, res) => {
  // Get a prospect on entities or facets that proxy aggregates of entities
  // of the given content type.
  //

  // Ensure content_type is given
  if (!('content_type' in req.query)) {
    return res.status(400).send('Missing query parameter: content_type')
  }

  // Find configuration of the given content type
  const selectedType = req.query.content_type
  const selectedConfig = config.contentTypeMap[selectedType]
  if (!selectedConfig) {
    return res.status(400).send('Unknown content type: ' + selectedType)
  }

  // Check for quick cached response.
  const stratumKey = queryHelpers.toStratumKey(req.query)
  const cacheResult = await cache.read(stratumKey)
  if (cacheResult) {
    return res.type('json').send(cacheResult)
  }
  // Otherwise continue with full stratum build.

  // Pick filters supported by this content type
  const activeFilters = queryHelpers.pickCorporaFilters(req.query)

  // Find context labels to return with the stratum.
  let contextCandidate
  try {
    contextCandidate = await fetchContext(req.query)
  } catch (err) {
    // TODO detect bad gateway
    console.warn('Unable to fetch context: ' + err.message)
    // Fall back to empty context.
    contextCandidate = []
  }
  const context = contextCandidate

  // Decice when to return facets and when entities.
  let facetThresholdCandidate
  if (req.query.facet_threshold) {
    facetThresholdCandidate = parseInt(req.query.facet_threshold)
  } else {
    facetThresholdCandidate = 100
  }
  const facetThreshold = facetThresholdCandidate

  // Find remaining free facets.
  // User might have already selected some facets. In order to avoid confusion
  // allow picking just one facet from each facet group.
  const allFacetModels = selectedConfig.facets
  const facetModels = allFacetModels.filter(model => {
    // Keep only facet groups that are not already in active filters.
    return !(model.parameter in activeFilters)
  })

  // If there are no free facets left, we return entities.
  if (facetModels.length === 0) {
    const pageSize = 50
    const pageIndex = 1
    let entities
    try {
      entities = await fetchEntities(selectedType, activeFilters, pageSize, pageIndex)
    } catch (err) {
      console.warn('Unable to fetch entities: ' + err.message)
      const statusCode = err.statusCode || 500
      return res.sendStatus(statusCode)
    }
    const nodes = entitiesToNodes(entities)
    upgradeContextImage(context, entities)

    const stratum = {
      path: stratumKey,
      provenance: 'corpora',
      count: entities.length,
      label: selectedConfig.label_plural,
      context,
      nodes,
      edges: [],
      layout: 'circlepack'
    }

    // We cache the result after response. No need for the client to wait.
    res.json(stratum)
    await cache.write(stratumKey, stratum)

    return
  }

  // There are free facets left.
  // Get the number of entities that match these filters.
  let countTotal
  try {
    countTotal = await fetchCount(selectedType, activeFilters)
  } catch (err) {
    console.warn('Unable to fetch count: ' + err.message)
    const statusCode = err.statusCode || 500
    return res.sendStatus(statusCode)
  }

  // If there are only a few entities, return the entities instead of facets.
  // TODO read the limit from config
  if (countTotal < facetThreshold) {
    const pageSize = 50
    const pageIndex = 1
    let entities
    try {
      entities = await fetchEntities(selectedType, activeFilters, pageSize, pageIndex)
    } catch (err) {
      console.warn('Unable to fetch entities: ' + err.message)
      const statusCode = err.statusCode || 500
      return res.sendStatus(statusCode)
    }
    const nodes = entitiesToNodes(entities)
    upgradeContextImage(context, entities)

    const stratum = {
      path: stratumKey,
      provenance: 'corpora',
      count: entities.length,
      label: selectedConfig.label_plural,
      context,
      nodes,
      edges: [],
      layout: 'circlepack'
    }

    // We cache the result after response. No need for the client to wait.
    res.json(stratum)
    await cache.write(stratumKey, stratum)

    return
  }
  // There are too many entities and we have some facets to group them.

  // For each configured facet parameter, fetch values and their counts.
  // Run in parallel with Promise.all. It runs an array of Promises.
  const tasks = facetModels.map(facetModel => {
    const field = facetModel.field
    const xrefType = facetModel.cross_reference_type
    const task = fetchFacets(selectedType, activeFilters, field, xrefType)
      .then(facets => {
        return {
          config: facetModel,
          countMax: facets.reduce((accMax, facet) => {
            return Math.max(accMax, facet.count)
          }, 0),
          facets
        }
      })

    return task
  })
  // Wait for tasks to finish.
  let facetGroups
  try {
    facetGroups = await Promise.all(tasks)
  } catch (err) {
    console.warn('Unable to fetch facets: ' + err.message)
    const statusCode = err.statusCode || 500
    return res.sendStatus(statusCode)
  }

  // We do not want to serve facet groups that have no facets.
  // This can happen if the remaining entities do not have necessary data for them.
  // Also, we do not want to serve facet groups that have only single facet left.
  // The single facet does not bring much value for the client.
  const properFacetGroups = facetGroups.filter(facetGroup => {
    const hasMultipleGroups = facetGroup.facets.length > 1
    const forceGroup = facetGroup.facets.length > facetThreshold // allow client to force faceting
    return hasMultipleGroups || forceGroup
  })

  // If there are no proper facet groups, we again serve the entities directly.
  if (properFacetGroups.length === 0) {
    const pageSize = 50
    const pageIndex = 1
    let entities
    try {
      entities = await fetchEntities(selectedType, activeFilters, pageSize, pageIndex)
    } catch (err) {
      console.warn('Unable to fetch entities: ' + err.message)
      const statusCode = err.statusCode || 500
      return res.sendStatus(statusCode)
    }
    const nodes = entitiesToNodes(entities)
    upgradeContextImage(context, entities)

    const stratum = {
      path: stratumKey,
      provenance: 'corpora',
      count: entities.length,
      label: selectedConfig.label_plural,
      context,
      nodes,
      edges: [],
      layout: 'circlepack'
    }

    // We cache the result after response. No need for the client to wait.
    res.json(stratum)
    await cache.write(stratumKey, stratum)

    return
  }

  // Okay, there are facet groups with actual facets.
  // Convert groups and facets into nodes.
  const nodes = []
  // Generate grouping nodes.
  properFacetGroups.forEach(facetGroup => {
    nodes.push({
      id: facetGroup.config.id,
      role: 'grouping',
      type: facetGroup.config.label + 'Group', // e.g. ProfessionGroup
      field: facetGroup.config.field,
      contentType: selectedType,
      label: facetGroup.config.label,
      labelPrefix: selectedConfig.label_plural + ' by',
      labelPostfix: null,
      count: facetGroup.countMax,
      parent: 'self'
    })
  })
  // Generate facet nodes.
  properFacetGroups.forEach(facetGroup => {
    facetGroup.facets.forEach(facet => {
      nodes.push({
        id: facetGroup.config.id + '/' + facet.id,
        role: 'facet',
        type: facetGroup.config.label, // e.g. Profession
        field: facetGroup.config.field,
        contentType: selectedType,
        label: facet.label,
        labelPrefix: facetGroup.config.label,
        labelPostfix: null,
        count: facet.count,
        facetParam: facetGroup.config.parameter,
        facetValue: facet.value,
        parent: facetGroup.config.id
      })
    })
  })
  // Pack into stratum.
  const stratum = {
    path: stratumKey,
    provenance: 'corpora',
    count: countTotal,
    label: selectedConfig.label_plural,
    context,
    nodes,
    edges: [],
    layout: 'grouped-circlepack'
  }

  // We cache the result after response. No need for the client to wait.
  res.json(stratum)
  await cache.write(stratumKey, stratum)
}
