const handlers = require('./handlers')
const express = require('express')
const router = express.Router()

router.get('/', handlers.get)

module.exports = router
