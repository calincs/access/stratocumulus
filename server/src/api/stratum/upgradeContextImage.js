const entityToLabel = require('./entityToLabel')

module.exports = (context, entities) => {
  // Upgrade the last context item with the first image found in the set of
  // raw entities. If no image found, no changes are made to the context.
  // Modifies the given context in place.
  //
  // Parameters:
  //   context
  //     an array of context items. See fetchContext.
  //   entities
  //     an array of raw entities from corpora. See fetchEntities.
  //

  // Pick an image if available.
  const entityWithImage = entities.find(ent => {
    return Array.isArray(ent.images) && ent.images.length > 0
  })

  if (entityWithImage) {
    const imageUrl = entityWithImage.images[0]

    const clen = context.length
    if (clen > 0) {
      const ultimate = context[clen - 1]
      ultimate.imageUrl = imageUrl
      ultimate.imageTitle = entityToLabel(entityWithImage)
    }
  }
}
