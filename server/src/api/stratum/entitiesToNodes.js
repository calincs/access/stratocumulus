const decorators = require('./decorators')
const config = require('../../config')
const entityToLabel = require('./entityToLabel')

module.exports = (entities, parent) => {
  // Convert an array of entities to stratum nodes.
  //
  // Parameters:
  //   entities
  //     an array of Corpora entity objects.
  //   parent
  //     an optional parent node id or function that returns the id.
  //
  // Return
  //   an object
  //

  return entities.map(entity => {
    // Decide label
    const label = entityToLabel(entity)

    // Decide label prefix
    const typeConfig = config.contentTypeMap[entity.content_type]
    const labelPrefix = typeConfig.label_singular

    // Decide count. Unknown (-1) by default.
    let count = -1
    if (entity.incoming_connections) {
      count = entity.incoming_connections
    }

    // Decide label postfix. E.g. year range.
    let labelPostfix = null
    // Detect birth and death dates of people
    if (entity.birth_date || entity.death_date) {
      let birthYear = '?'
      if (entity.birth_date) {
        birthYear = decorators.dateToYear(entity.birth_date)
      }
      let deathYear = '?'
      if (entity.death_date) {
        deathYear = decorators.dateToYear(entity.death_date)
      }
      labelPostfix = birthYear + '–' + deathYear
    }
    // Detect year range e.g. of human-made objects
    if (!labelPostfix && entity.years && entity.years.length > 0) {
      if (entity.years.length === 1) {
        labelPostfix = entity.years[0]
      } else {
        labelPostfix = decorators.yearsToRange(entity.years)
      }
    }

    // Decide parent
    let nodeParent = 'self'
    if (parent) {
      if (typeof parent === 'function') {
        nodeParent = parent(entity)
      } else {
        nodeParent = parent
      }
    }

    return {
      id: entity.id,
      role: 'entity',
      type: entity.content_type,
      field: 'entity', // TODO is the field needed anymore?
      label,
      labelPrefix,
      labelPostfix,
      contentType: 'Connection', // TODO is needed?
      count, // TODO rename to contentCount
      facetParam: 'entity',
      facetValue: `${entity.content_type}-${entity.id}`,
      parent: nodeParent
      // TODO attach full data
    }
  })
}
