const stratumRouter = require('./stratum/routes')
const entityRouter = require('./entity/routes')
const express = require('express')
const router = express.Router()

router.use('/entity', entityRouter)

// Route all else to the client app
router.use('/stratum', stratumRouter)

// Catch all
router.get('*', (req, res) => {
  res.status(404).json({ status: 404, message: 'Route not found.' })
})

module.exports = router
