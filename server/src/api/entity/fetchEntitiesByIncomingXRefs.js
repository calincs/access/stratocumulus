const config = require('../../config')
const baseUrl = config.corporaCorpusApiUrl

module.exports = async (entity) => {
  // Fetch related entities via their incoming cross references.
  // Incoming cross references are those where an exteral entity refers
  // to this entity.
  //
  // Parameters:
  //   entity
  //     an object, the host entity.
  //
  // Result
  //   a Promise, resolved with an array of entities with added properties:
  //     relationshipType
  //       a string, relationship type, e.g. 'Creator'
  //     relationshipLabel
  //       a string, human-readable relationship label, e.g. 'Creations'
  //     relationshipLabelPrefix
  //       a string, additional human-readable label preceding the primary label.
  //
  // If the entity type has no CrossReferenceIn relations configured,
  // this resolves with an empty array.
  //

  // Find config for the junction tables that model the relations.
  const entityType = entity.content_type
  const typeConfig = config.contentTypeMap[entityType]
  if (!typeConfig) {
    return Promise.reject(new Error('Unknown entity type: ' + entityType))
  }
  const relConfigs = typeConfig.relations

  // Relations not configured for this entity type = no connections available.
  if (!relConfigs) {
    return Promise.resolve([])
  }

  const xrefConfigs = relConfigs.filter(c => c.relation_type === 'CrossReferenceIn')

  // For each configured cross reference relation, fetch entities.
  // Run in parallel with Promise.all. It takes an array of Promises.
  const tasks = xrefConfigs.map(relConf => {
    const structure = relConf.structure
    // Build URL
    const sourceType = structure.source_type
    const targetUrl = new URL(sourceType, baseUrl)
    // Build query parameters
    const queryParameter = 'f_' + structure.source_field + '.id' // e.g. f_places.id
    const queryValue = entity.id
    targetUrl.searchParams.append(queryParameter, queryValue)
    // In case there are lots of entities, only the first page is returned.
    // Therefore ensure to return the most connected entities.
    if (structure.priority_field) {
      targetUrl.searchParams.append('s_' + structure.priority_field, 'DESC')
    }

    console.log('api.entity.fetchEntitiesByIncomingXRefs: fetch', targetUrl.href)
    const task = fetch(targetUrl)
      .then(response => {
        if (response.status === 200) {
          return response.json()
        }
        if (response.status === 500 || response.status === 404) {
          // Corpora responses with 500 on missing entities. Should be 404.
          const err = new Error('Content type not found: ' + sourceType)
          err.statusCode = 404
          throw err
        }
        const err = new Error('Unexpected response from Corpora')
        err.statusCode = 502
        throw err
      })
      .then(data => {
        // Extract array of entities
        const entities = data.records || []

        // Add content type data.
        // While single-entity responses from Corpora contain content_type,
        // multi-entity responses do not.
        // This is useful whenever we group multiple entity types e.g. for relation.
        entities.forEach(ent => {
          ent.content_type = sourceType
        })

        // Add relationship data.
        entities.forEach(ent => {
          ent.relationshipType = relConf.label // TODO should be removed or something else than label?
          ent.relationshipLabel = relConf.label
          ent.relationshipLabelPrefix = relConf.label_prefix
        })

        return entities
      })

    return task
  })

  // Wait for tasks to finish
  // and join the resulting entities.
  const result = Promise.all(tasks)
    .then(subresults => {
      return subresults.reduce((acc, sub) => acc.concat(sub), [])
    })

  return result
}
