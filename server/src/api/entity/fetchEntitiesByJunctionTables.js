const fetchJunctionTables = require('./fetchJunctionTables')
const config = require('../../config')
const baseUrl = config.corporaCorpusApiUrl

module.exports = async (entity) => {
  // Fetch all entities that are related to the given entity via junction tables.
  //
  // Parameters:
  //   entity
  //     an entity object, the host entity.
  //
  // Returns:
  //   a Promise, resolved with
  //     an array of entity objects with extra properties:
  //       relationshipType
  //         a string, e.g. 'Creator' or 'Owner'
  //       relationshipLabel
  //         a string, direction-aware human-readable label, e.g. 'Creations'.
  //       relationshipLabelPrefix
  //         a string, additional human-readable label preceding the primary label.
  //
  let relations
  try {
    relations = await fetchJunctionTables(entity)
  } catch (err) {
    return Promise.reject(err)
  }

  // We need to query each content type separately.
  // We can get all the entities of the same content type with a single query
  // by using query parameters f_iri and operator with double underscore separator
  // like f_iri=foo__bar&operator=or.

  // Group relations by their target entity type.
  const relationGroups = relations.reduce((acc, rel) => {
    const entityType = rel.type
    if (entityType in acc) {
      acc[entityType].push(rel)
    } else {
      acc[entityType] = [rel]
    }
    return acc
  }, {})

  // Form tasks to get the entities for each type.
  const tasks = Object.keys(relationGroups).map(entityType => {
    // Prepare query values
    const iris = relationGroups[entityType].map(rel => rel.iri)
    // Build URL
    const targetUrl = new URL(entityType, baseUrl)
    targetUrl.searchParams.append('f_iri', iris.join('__'))
    targetUrl.searchParams.append('operator', 'or')
    // Prepare relationship data to be added to result. iri -> relation
    const iriToRelation = relationGroups[entityType].reduce((acc, rel) => {
      acc[rel.iri] = rel
      return acc
    }, {})

    console.log('api.entity.fetchEntitiesByJunctionTables: fetch', targetUrl.href)
    const task = fetch(targetUrl)
      .then(response => {
        if (response.status === 200) {
          return response.json()
        }
        if (response.status === 500 || response.status === 404) {
          // Corpora responses with 500 on missing entities. Should be 404.
          const err = new Error('Endpoint not found')
          err.statusCode = 404
          throw err
        }
        const err = new Error('Unexpected response from Corpora')
        err.statusCode = 502
        throw err
      })
      .then(data => {
        const entities = data.records

        // Add content type data. While single-entity responses from Corpora contain content_type,
        // multi-entity responses do not.
        entities.forEach(ent => {
          ent.content_type = entityType
        })

        // Add relationship data
        entities.forEach(ent => {
          const rel = iriToRelation[ent.iri]
          if (rel) {
            ent.relationshipType = rel.relationshipType
            ent.relationshipLabel = rel.relationshipLabel
            ent.relationshipLabelPrefix = rel.relationshipLabelPrefix
          }
        })

        return entities
      })

    return task
  })

  // Run tasks in parallel
  const result = Promise.all(tasks)
    .then(subresults => {
      // Join the results.
      return subresults.reduce((acc, sub) => acc.concat(sub), [])
    })

  return result
}
