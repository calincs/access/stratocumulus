const config = require('../../config')
const baseUrl = config.corporaCorpusApiUrl

module.exports = async (entity) => {
  // Fetch junction table entries connected to the given entity.
  //
  // Parameters:
  //   entity
  //     an object, the host entity.
  //
  // Result
  //   a Promise, resolved with an array of related entities with properties:
  //     iri
  //       a string
  //     type
  //       a string, the content type
  //     relationshipType
  //       a string, relationship type, e.g. 'Creator'
  //     relationshipLabel
  //       a string, human-readable relationship label, e.g. 'Creations'
  //     relationshipLabelPrefix
  //       a string, additional human-readable label preceding the primary label.
  //
  // If the entity type has no relations configured, resolves with an empty array.
  //

  // Find config for the junction tables that model the relations.
  const entityType = entity.content_type
  const typeConfig = config.contentTypeMap[entityType]
  if (!typeConfig) {
    return Promise.reject(new Error('Unknown entity type: ' + entityType))
  }
  const relConfigs = typeConfig.relations

  // Relations not configured for this entity type = no connections available.
  if (!relConfigs) {
    return Promise.resolve([])
  }

  const junctionConfigs = relConfigs.filter(c => c.relation_type === 'JunctionTable')

  // For each configured junction table relation, fetch junction table records.
  // Run in parallel with Promise.all. It runs an array of Promises.
  const tasks = junctionConfigs.map(relConf => {
    const structure = relConf.structure
    // Build URL
    const junctionType = structure.junction_type
    const targetUrl = new URL(junctionType, baseUrl)
    // Build query parameters
    const queryParameter = 'f_' + structure.junction_source_iri_field // e.g. f_person_iri
    const queryValue = entity[structure.source_field] // e.g. entity.iri
    targetUrl.searchParams.append(queryParameter, queryValue)
    // Is relationship limited
    if (structure.relationship_type) {
      targetUrl.searchParams.append('f_relationship', structure.relationship_type)
    }

    console.log('api.entity.fetchJunctionTables: fetch', targetUrl.href)
    const task = fetch(targetUrl)
      .then(response => {
        if (response.status === 200) {
          return response.json()
        }
        if (response.status === 500 || response.status === 404) {
          // Corpora responses with 500 on missing entities. Should be 404.
          const err = new Error('Junction not found')
          err.statusCode = 404
          throw err
        }
        const err = new Error('Unexpected response from Corpora')
        err.statusCode = 502
        throw err
      })
      .then(data => {
        // Build stripped entities from the junction rows.
        return data.records.map(row => {
          const targetEntity = {}

          targetEntity.type = structure.target_type
          targetEntity.iri = row[structure.junction_target_iri_field]
          targetEntity.relationshipType = structure.relationship_type || 'related'
          targetEntity.relationshipLabel = relConf.label
          targetEntity.relationshipLabelPrefix = relConf.label_prefix

          return targetEntity
        })
      })

    return task
  })

  // Wait for tasks to finish.
  // And join the resulting relations.
  const result = Promise.all(tasks)
    .then(subresults => {
      return subresults.reduce((acc, sub) => acc.concat(sub), [])
    })

  return result
}
