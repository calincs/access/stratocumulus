const fetchEntitiesByJunctionTables = require('./fetchEntitiesByJunctionTables')
const fetchEntitiesByIncomingXRefs = require('./fetchEntitiesByIncomingXRefs')

module.exports = async (entity) => {
  // Fetch all entities that are related to the given entity.
  //
  // Parameters:
  //   entity
  //     an entity object, the host entity.
  //
  // Returns:
  //   a Promise, resolved with
  //     an array of entity objects with extra properties:
  //       relationshipType
  //         a string, e.g. 'Creator' or 'Owner'
  //       relationshipLabel
  //         a string, direction-aware human-readable label, e.g. 'Creations'.
  //       relationshipLabelPrefix
  //         a string, additional human-readable label preceding the primary label.
  //

  const tasks = [
    fetchEntitiesByJunctionTables(entity),
    fetchEntitiesByIncomingXRefs(entity)
  ]

  // Run tasks in parallel
  const result = Promise.all(tasks)
    .then(subresults => {
      // Join the results.
      return subresults.reduce((acc, sub) => acc.concat(sub), [])
    })

  return result
}
