const fetchEntity = require('./fetchEntity')
const fetchEntities = require('./fetchEntities')
const queryHelpers = require('../stratum/queryHelpers')

exports.getOne = async (req, res) => {
  // Get one entity.
  // TODO convert entity to a facets.
  //

  const contentType = req.params.contentType
  const entityId = req.params.entityId

  let entity
  try {
    entity = await fetchEntity(contentType, entityId)
  } catch (err) {
    if (err.statusCode) {
      return res.status(err.statusCode).send(err.message)
    }
    return res.sendStatus(502)
  }

  return res.json(entity)
}

exports.getMany = async (req, res) => {
  // Get a page of entities.
  // TODO convert entities to facets.
  //

  let pageSize = 50
  let pageIndex = 1 // 1 is the first page in Corpora

  if ('pageSize' in req.query) {
    const parsed = parseInt(req.query.pageSize)
    if (!isNaN(parsed)) {
      pageSize = Math.max(0, parsed)
    }
  }

  if ('page' in req.query) {
    const parsed = parseInt(req.query.page)
    if (!isNaN(parsed)) {
      pageIndex = Math.max(1, parsed)
    }
  }

  const contentType = req.params.contentType
  const filters = queryHelpers.pickCorporaFilters(req.query)

  let entities
  try {
    entities = await fetchEntities(contentType, filters, pageSize, pageIndex)
  } catch (err) {
    return res.sendStatus(502)
  }

  return res.json(entities)
}
