const config = require('../../config')

module.exports = async (contentType, filters, pageSize, pageIndex) => {
  // Fetch matching rows of the given content type.
  //
  // Parameters:
  //   contentType
  //     a string
  //   filters
  //     an object
  //   pageSize
  //     an integer, the number of rows.
  //   pageIndex
  //     an integer, 1 for the first page, 2 for the second etc.
  //
  // Return
  //   a Promise, resolved with an array of entity objects
  //
  const baseUrl = config.corporaCorpusApiUrl
  const targetUrl = new URL(contentType, baseUrl)

  for (const key in filters) {
    targetUrl.searchParams.append(key, filters[key])
  }

  // Paging
  targetUrl.searchParams.append('page-size', pageSize)
  targetUrl.searchParams.append('page', pageIndex)

  console.log('api.entity.fetchEntities: fetch', targetUrl.href)
  const rowsPromise = fetch(targetUrl)
    .then(response => response.json())
    .then(data => {
      if (!data || !data.records) {
        throw new Error(`Failed to fetch entities for ${contentType}`)
      }

      const entities = data.records

      // Add content type data. While single-entity responses from Corpora contain content_type,
      // multi-entity responses do not.
      // This is useful whenever we group multiple entity types e.g. for relation.
      entities.forEach(ent => {
        ent.content_type = contentType
      })

      return entities
    })

  return rowsPromise
}
