const handlers = require('./handlers')
const express = require('express')
const router = express.Router()

router.get('/:contentType/:entityId', handlers.getOne)
router.get('/:contentType', handlers.getMany)

module.exports = router
