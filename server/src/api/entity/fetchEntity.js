const config = require('../../config')
const baseUrl = config.corporaCorpusApiUrl

module.exports = async (contentType, entityId) => {
  // Fetch entity of the given content type and ID.
  //
  // Parameters:
  //   contentType
  //     a string
  //   entityId
  //     a string
  //
  // Return
  //   a Promise, resolved with an entity object
  //
  const targetUrl = new URL(contentType + '/' + entityId + '/', baseUrl)

  console.log('api.entity.fetchEntity: fetch', targetUrl.href)
  const entityPromise = fetch(targetUrl)
    .then(response => {
      if (response.status === 200) {
        return response.json()
      }
      if (response.status === 500 || response.status === 404) {
        // Corpora responses with 500 on missing entities. Should be 404.
        const err = new Error('Entity not found at ' + targetUrl.href)
        err.statusCode = 404
        throw err
      }
      const err = new Error('Unexpected response from Corpora for ' + targetUrl.href)
      err.statusCode = response.status
      throw err
    })
    .then(data => {
      if (typeof data !== 'object') {
        const err = new Error('Bad response from Corpora for ' + targetUrl.href)
        err.statusCode = 502
        throw err
      } else if (Object.keys(data).length === 0) {
        // Sometimes Corpora returns an empty object.
        // Assume this means that the person does not exist.
        const err = new Error('No entity found at ' + targetUrl.href)
        err.statusCode = 404
        throw err
      }
      // Else likely valid entity.
      return data
    })

  return entityPromise
}
