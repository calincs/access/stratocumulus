// Redis client. Singleton pattern.
//
const redis = require('redis')
const config = require('./config')

// Redis client configuration.
const clientConfig = {
  // Where to connect
  url: config.redisUrl,
  // As we use Redis primarily for caching, we want operations to fail quickly
  // in case Redis goes offline. Disabling offline queue does the trick.
  disableOfflineQueue: true
}

let redisClient

// Cache time-to-live in seconds
exports.cacheDuration = config.redisCacheDuration

exports.client = async () => {
  // Get the redis client. Create and connect if not yet so.
  //
  if (!redisClient) {
    redisClient = redis.createClient(clientConfig)
    redisClient.on('error', (error) => console.error(`Redis error: ${error}`))
    await redisClient.connect()
  }

  return redisClient
}
