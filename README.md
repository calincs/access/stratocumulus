# Stratocumulus

Millions of linked data documents and huge databases of semantic networks ask for a browser, a way to approach and explore the mass of entities. Stratocumulus approaches the task with a zoomable nested groups of facets. Each facet shaves the mass, a facet by facet, until you reach the entities and results which you are looking for. Just dive in. Stratocumulus lets you build your facet sequence by diving - and zooming - through facet nodes.

## Examples

[![LINCS Stratocumulus](docs/stratocumulus_1.0.0_lincs_example.png)](https://stratocumulus.lincsproject.ca/)
This [LINCS](https://lincsproject.ca/) corpus explorer is powered by Stratocumulus. It combines four separate entity indices into one navigable space.

[![Stratocumulus Prototype](docs/stratocumulus_0.3.0_example.png)](http://prototype.bigdiva.org/)
Here is the [ARC](https://arc.dh.tamu.edu/) database visualised with Stratocumulus. Two of the entity collections are faceted and their facet subgraphs rendered and zoomable.

## Resources

- [Live app](https://stratocumulus.lincsproject.ca/)
- [Git repository](https://gitlab.com/calincs/access/stratocumulus)
- [Documentation](#documentation)
- [LINCS Project](https://lincsproject.ca/)
- [BigDIVA](https://bigdiva.org/)
- [Center of Digital Humanities Research](https://codhr.dh.tamu.edu/)

## Browser and Research Features

- explore documents and categories via fractal zooming
- search and filter by keywords

## Technical Features

- [SPA](https://en.wikipedia.org/wiki/Single-page_application) single page application
- HTML CSS JS [Tapspace.js](https://taataa.github.io/tapspace/) [Graphology](https://graphology.github.io/) [Webpack](https://webpack.js.org/) [StandardJS](https://standardjs.com/)
- Python3 [Flask](https://flask.palletsprojects.com/en/) [Celery](https://docs.celeryq.dev/en/stable/) [Redis](https://redis.io/)

## Building and Running the App

- Have [Docker Desktop](https://www.docker.com/products/docker-desktop/) installed on your development machine and make sure it's running
- Clone the repo by `git clone git@github.com:ARC-code/stratocumulus.git`
- Locate `docker-compose.yml` at the root of this cloned repository.
- Create a `.env` file next to `docker-compose.yml` for your environment variables such as Corpora endpoint details. See `.env.example` for an example and `docker-compose.yml` for how they are used.
- In a terminal, navigate to the repo directory (the same directory as docker-compose.yml) and issue this command: `docker compose up`. If you use a custom env file like `.env.arc` issue this instead: `docker compose --env-file .env.arc up`.
- Visit the prototype by opening a browser and navigating to [localhost](localhost)

## Development Workflow

For development we recommend running Docker. Start up the containers with `docker compose up` as described above and then open the application at [localhost](localhost).

When making changes to the server-side code, those changes won't reflect on the running application until you shut down the Docker Compose stack by going to the terminal running the Docker app, hitting ctrl+c, and then restarting the stack by running `docker compose up` again. Often it is necessary to remove the containers with `docker compose down` before `up` to ensure cached but outdated build files and dependencies. Also, if you modify static files to be served by the backend, the volumes from which these static files are served need a reset with `docker compose down --volumes`. Also, an abrupt killing of the docker stack can cause volumes to corrupt, requiring a reset with the `--volumes` flag.

The server-side adapter-specific stylesheets make an exception. Those are refreshed at every page refresh and therefore do not require restarting the Docker stack. This makes working with stylesheets much more efficient.

Changes to frontend code do not require container restarts or image rebuilds, only a few seconds and a page refresh. This is because the changes under `client/lib` are automatically watched and rebuilt by webpack running in the frontend container. However, any changes outside `client/lib` – for example in `client/index.js` or `client/package.json` – require an image rebuild with `docker compose up --build` to take effect. Installing or upgrading a client-side dependency require such a rebuild.

To inspect frontend build errors in development, see the console output of the frontend container via Docker Desktop or in the terminal in which you are running `docker compose`.

To lint the frontend code to catch unused variables and other issues, cd to `client/` and run `npm run lint`. For continuous linting, run `npm run lint:watch`. The frontend code follows [JavaScript Standard Style](https://standardjs.com/).

## Documentation

For further information on working with the Stratocumulus backend and frontend, see:

- [Stratocumulus Server documentation](https://gitlab.com/calincs/access/stratocumulus/-/blob/main/docs/server.md)
- [Stratocumulus Client documentation](https://gitlab.com/calincs/access/stratocumulus/-/blob/main/client/docs/architecture.md)

## Tools

These tools have been useful for development although Stratocumulus is not dependent on them.

- [CIELab Color Picker](https://rufflewind.com/_urandom/colorpicker/)

## Licence

[MIT](LICENSE)
