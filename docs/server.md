# Stratocumulus Backend

[Overview](#) – [Structure](#structure) – [HTTP API](#http-api) – [Environment Variables](#environment-variables)

The backend – also known as the server-side – acts as a data broker and a caching layer between third-party data sources and the frontend application. It's a Flask app (Python) that makes use of a Celery task queue and a Redis message broker for handling Server Sent Events. The Redis database also functions as a fast in-memory cache for computationally demanding build tasks.


## Structure

The backend for this app is located under `server` directory. The script `server/stratocumulus.py` provides the major HTTP endpoints and is responsible of adding build tasks to the Celery queue.

There are `server/adapters` and `server/templates` that together implement an adapter architecture that allows Stratocumulus to support multiple data sources. Stratocumulus instances choose the active adapter with `STRATO_PLUGIN` environment variable.


## HTTP API

The backend features the following HTTP endpoints reachable with `GET` and `POST` methods.

### GET /

This endpoint serves the client-side app intended to be accessed via browser.

**Parameters:** optionally clients can specify query parameters to control the initial faceting context of the app, for example `GET /?content_type=Person&f_professions.id=ABC123`. The available query parameters are adapter specific.

**Response:** `HTTP 200` with content type `text/html`

### GET /build_stratum

The client app should call this endpoint in order to initiate the build of a stratum graph. The build job is added to Celery queue and completed by the active adapter. The resulting graph will be sent to the client via Server Sent Events (SSE) over one or multiple events. The client is expected to subscribe to a SSE channel before requesting this endpoint.

**Parameters:** the endpoint supports the following query parameters
- `path`: the value must be a string that identifies the stratum to be built. The path consists of an encoded set of key-value pairs that determine the faceting and filtering context of the stratum.

**Response:** `HTTP 200` with message body `Building.`

**Events:** The resulting SSE events have the following structure encoded in JSON:
- `path`: a string, the stratum identifier
- `stage`: a string, either `initial`, `update`, `final`, or `error`. Reveals the stage of the build process.
- `provenance`: a string, either `corpora` or `cache`. Reveals the data source.
- `size`: an integer, the number of entities aggregated into the stratum.
- `label`: a string, a user-friendly name for the stratum.
- `nodes`: an array of node objects. Each node object has properties:
  - `id`: a string, unique within the stratum graph but not among all strata.
  - `kind`: a string, either `facet` or `grouping`
  - `content_type`: a string, the type of entities the node aggregates.
  - `label`: a string, the user-friendly name of the node.
  - `parent`: a string, the node id of the parent node.
  - `value`: an integer, the number of entities aggregated as a node.
  - `field`: optional string, the corpus field name of the facet parameter.
  - `facet_param`: optional string, the parameter the node can to the faceting context.
  - `facet_value`: optional string, the value of the facet parameter.
- `edges`: an array of edge objects. Each edge object has properties:
  - `from`: a string, a node id
  - `to`: a string, a node id

### POST /publish

A server-side build task should call this endpoint in order to publish its finished subgraph to clients as an SSE event.


## Environment Variables

The backend expects the following environment variables to be set.

- `STRATO_SECRET_KEY`: The value must be a string. The value is used for encryption. Every Stratocumulus instance should set an unique secret key.
- `STRATO_PLUGIN`: The value must be a string. The value determines the active adapter. For available adapter names, see directories under `server/adapters`.
- `STRATO_STATIC_FOLDER`: The value must be an absolute directory path, for example `/usr/src/static`. The backend will store static assets to this directory and will serve them under at the URL `/static`.
- `STRATO_CORPORA_HOST`: The value must be a valid URL consisting of the protocol `http://` or `https://`, the host name and must have a trailing slash `/`, for example `https://example.com/`. The value determines the third-party host serving the corpus data to be processed and displayed.
- `STRATO_CORPORA_CORPUS_ID`: The value must be a string. The value determines the corpus to use among the available corpora served by the host.
- `REDIS_URL`: Optional URL, defaults to `redis://redis`. The location of the Redis database.
- `REDIS_CACHE_TTL`: Optional integer, defaults to `3600` that is one hour. The value determines the number of seconds cached objects will be stored before discarded. Small values can practical for development.

For development, it is advisable to use a secure `.env` file to store the variables and then start up the Docker stack with `--env-file` flag, for example `docker compose --env-file .env up`.
