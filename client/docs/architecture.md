<a name="top"></a>
# Stratocumulus Client Documentation v1.0.0


Welcome to Stratocumulus client-side documentation.

This document follows these naming conventions: `ClassName`, `namespace`, `.CONSTANT`, `.classMethod()`, `:instanceProperty`, `:instanceMethod()`, and `[optionalParameter]`.

See also: [Introduction](https://gitlab.com/calincs/access/stratocumulus) – [Examples](https://gitlab.com/calincs/access/stratocumulus#examples) - [GitHub](https://gitlab.com/calincs/access/stratocumulus)

This document is generated with [yamdog](https://github.com/axelpale/yamdog).

## Overview

The frontend code is located under `client` directory.
As the frontend will consists of multiple javascript-files, stylesheets, and images,
they must be bundled together and be served as static files by the backend.
For the bundling process, the frontend has a dedicated Docker container that
runs [webpack](https://webpack.js.org/).
Webpack places the finished bundle to a volume that is shared with the container running the backend.
In addition to the bundle, the backend serves an HTML page defined in `server/templates/<plugin>/index.html`.
When a user opens the page, the browser requests the bundle and other assets from the backend.

In the most general level, the client does the following:
- Loading the page causes the client to a) subscribe to the SSE channel and dictate what happens when subgraph messages are received on the channel's stream, and then b) call the /build_stratum endpoint. The code for these actions resides in the `client/lib/io` module.
- Upon receiving subgraph messages on the SSE channel, the client attempts to perform a circle pack layout of the graph. This is intended to provide initial x/y coordinates for each node, grouping nodes together according to its parent. The code for this is in the `render` method of each stratum class.
- Nodes in the graph are openable and will load and display a faceted version of the same stratum. Users can navigate further down inside the nodes to inspect the results. The code that handles automatic opening of nodes and recursive loading of nested strata resides in the `client/lib/Sky` component.



<a name="0stratocumulus"></a>
## [0](#0) [Stratocumulus](#0stratocumulus)

<p style="margin-bottom: 0">Abstract Components:</p>

- [StratumNode](#stratumnode), a node on a [Stratum](#stratum)


<p style="margin-bottom: 0">Components:</p>

- [CategoryNode](#categorynode), an openable node that represents a collection of entities.
- [Stratum](#stratum), a network graph of facetable nodes
- [Context](#context)Form, a widget to display the current navigation path and filters.
- [SearchForm](#searchform), a text search form
- [Sky](#sky), a loader for nested strata
- [ViewportManager](#viewportmanager), a helper class to setup [Sky](#sky) viewport.


<p style="margin-bottom: 0">Data Models:</p>

- [Context](#context), an ordered set of filtering and faceting parameters and values
- [ReduxStore](#reduxstore), a minimal redux-like state management


<p style="margin-bottom: 0">Adapters:</p>

- [io](#io).entityApi, methods to fetch entities
- [io](#io).stratumApi, methods to fetch stratum graphs


<p style="margin-bottom: 0">Libraries:</p>

- [tapspace](https://axelpale.github.io/tapspace/api/v2/), provides zoomable user interface
- [graphology](https://graphology.github.io/), provides graph models


Source: [client/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/index.js)

<a name="categorynode"></a>
## [CategoryNode](#categorynode)(context, key, attrs)

Inherits [StratumNode](#stratumnode)

A node in a stratum. [Stratum](#stratum) maintains set of nodes.
A slave component, causes only visual side-effects, model-ignorant.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context), the faceting context
- *key*
  - string, graph node key, e.g. "/arc/genres" or "/arc/genres/1234"
- *attrs*
  - object, the node attributes from the graph.


<p style="margin-bottom: 0">Emits:</p>

- openingrequest `{ nodeKey: <string>, item: <Component> }`
  - when the user interacts with the node in order to open something.



<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [CategoryNode:adjustLabelSize](#categorynodeadjustlabelsize)
- [CategoryNode:isVisited](#categorynodeisvisited)
- [CategoryNode:render](#categorynoderender)
- [CategoryNode:requestOpening](#categorynoderequestopening)
- [CategoryNode:requestWarping](#categorynoderequestwarping)
- [CategoryNode:setLoadingState](#categorynodesetloadingstate)


Source: [CategoryNode/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/CategoryNode/index.js)

<a name="categorynodeadjustlabelsize"></a>
## [CategoryNode](#categorynode):[adjustLabelSize](#categorynodeadjustlabelsize)()

Refresh the node label font size dynamically
based on its distance to viewer.

Source: [adjustLabelSize.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/CategoryNode/adjustLabelSize.js)

<a name="categorynodeisvisited"></a>
## [CategoryNode](#categorynode):[isVisited](#categorynodeisvisited)()

Check if the node is visited.

Source: [isVisited.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/CategoryNode/isVisited.js)

<a name="categorynoderender"></a>
## [CategoryNode](#categorynode):[render](#categorynoderender)()

Refresh the contents.

Source: [render.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/CategoryNode/render.js)

<a name="categorynoderequestopening"></a>
## [CategoryNode](#categorynode):[requestOpening](#categorynoderequestopening)()

Ask to open the node. This makes the node emit 'openingrequest' via DOM.

Source: [requestOpening.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/CategoryNode/requestOpening.js)

<a name="categorynoderequestwarping"></a>
## [CategoryNode](#categorynode):[requestWarping](#categorynoderequestwarping)()

Ask to warp to the node's context.

Source: [requestWarping.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/CategoryNode/requestWarping.js)

<a name="categorynodesetloadingstate"></a>
## [CategoryNode](#categorynode):[setLoadingState](#categorynodesetloadingstate)(stage)

Use this method to visually open a node when its
substratum is loading or loaded.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *stage*
  - an enum string: 'loading', 'loaded', 'closing', or 'closed'


Source: [setLoadingState.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/CategoryNode/setLoadingState.js)

<a name="context"></a>
## [Context](#context)(keys, values)

Class and methods for handling Corpora context objects.
Keeps the keys and values in the correct order.
Immutable.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *keys*
  - array of string
- *values*
  - array of string



<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [Context:add](#contextadd)
- [Context:copy](#contextcopy)
- [Context:drop](#contextdrop)
- [Context:each](#contexteach)
- [Context:equals](#contextequals)
- [Context:filter](#contextfilter)
- [Context:getIntegerValue](#contextgetintegervalue)
- [Context:getLast](#contextgetlast)
- [Context:getRangeValue](#contextgetrangevalue)
- [Context:getValue](#contextgetvalue)
- [Context:hasParameter](#contexthasparameter)
- [Context:hasValue](#contexthasvalue)
- [Context:map](#contextmap)
- [Context:merge](#contextmerge)
- [Context:pick](#contextpick)
- [Context:remove](#contextremove)
- [Context:removeLast](#contextremovelast)
- [Context:sort](#contextsort)
- [Context:toArray](#contexttoarray)
- [Context:toContextObject](#contexttocontextobject)
- [Context:toQueryString](#contexttoquerystring)
- [Context:toUrlPath](#contexttourlpath)
- [Context.fromContextObject](#contextfromcontextobject)
- [Context.fromQueryString](#contextfromquerystring)
- [Context.fromUrlPath](#contextfromurlpath)
- [ContextView:getElement](#contextviewgetelement)
- [ContextView:render](#contextviewrender)


Source: [Context/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/index.js)

<a name="contextadd"></a>
## [Context](#context):[add](#contextadd)(key, value)

Add a context parameter as the last parameter. Creates a new [Context](#context).

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *key*
  - a string
- *value*
  - a string


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context).


Source: [append.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/append.js)

<a name="contextcopy"></a>
## [Context](#context):[copy](#contextcopy)()

Clone the context.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [copy.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/copy.js)

<a name="contextdrop"></a>
## [Context](#context):[drop](#contextdrop)(blacklist)

Get a subset of this context that excludes the blacklisted parameters.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *blacklist*
  - an array of string, the list of parameters to exclude.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [drop.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/drop.js)

<a name="contexteach"></a>
## [Context](#context):[each](#contexteach)(fn)

Call the function for each key-value pair.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *fn*
  - a function (key, value) => any


Source: [each.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/each.js)

<a name="contextequals"></a>
## [Context](#context):[equals](#contextequals)(ctx)

Test if two contexts are equal. Two context are equal if
they have the same parameters and values in same order.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *ctx*
  - a [Context](#context)


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a boolean


Source: [equals.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/equals.js)

<a name="contextfilter"></a>
## [Context](#context):[filter](#contextfilter)(fn)

Filter the context using a boolean function.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *fn*
  - a function (key, value) => bool


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [filter.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/filter.js)

<a name="contextgetintegervalue"></a>
## [Context](#context):[getIntegerValue](#contextgetintegervalue)(param, defaultValue)

Get integer from the parameter.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *param*
  - a string
- *defaultValue*
  - an integer, the default value to use if the real value is invalid or does not exist.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- an integer or the default value, regardless of is the default value an integer or not.


Source: [getIntegerValue.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/getIntegerValue.js)

<a name="contextgetlast"></a>
## [Context](#context):[getLast](#contextgetlast)([selection])

Get the last parameter-value pair as an object.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *selection*
  - optional array of string, the subset of parameters.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- an object { parameter, value } or null if empty


Source: [getLast.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/getLast.js)

<a name="contextgetrangevalue"></a>
## [Context](#context):[getRangeValue](#contextgetrangevalue)(param)

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a range object { rangeStart, rangeEnd }
- null if param not found.


Source: [getRangeValue.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/getRangeValue.js)

<a name="contextgetvalue"></a>
## [Context](#context):[getValue](#contextgetvalue)(param)

**Returns:** first value of the given parameter. Null if not found.

Source: [getValue.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/getValue.js)

<a name="contexthasparameter"></a>
## [Context](#context):[hasParameter](#contexthasparameter)(param)

Source: [hasParameter.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/hasParameter.js)

<a name="contexthasvalue"></a>
## [Context](#context):[hasValue](#contexthasvalue)(param, value)

Test if given param-value pair exists in the context.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- *boolean*


Source: [hasValue.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/hasValue.js)

<a name="contextmap"></a>
## [Context](#context):[map](#contextmap)(fn)

Map the context to an array.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *fn*
  - a function (key, value, i) => any


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- an array


Source: [map.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/map.js)

<a name="contextmerge"></a>
## [Context](#context):[merge](#contextmerge)(ctx)

Merge with another context.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *ctx*
  - a [Context](#context)


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [merge.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/merge.js)

<a name="contextpick"></a>
## [Context](#context):[pick](#contextpick)(selection)

Get a subset of this context that includes only the selected parameters.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *selection*
  - an array of string, the list of selected parameters.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [pick.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/pick.js)

<a name="contextremove"></a>
## [Context](#context):[remove](#contextremove)(key[, value])

Remove a context parameter.
If there are multiple instances of the same
parameter all of them will be removed.
Creates a new [Context](#context).

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *key*
  - a string
- *value*
  - optional string. If given, remove only this value.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context).


Source: [remove.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/remove.js)

<a name="contextremovelast"></a>
## [Context](#context):[removeLast](#contextremovelast)([selection])

Remove the last parameter, if any.
Useful for building broader context step by step.
Creates a new [Context](#context).

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *selection*
  - optional array of string. If not given, returns the last parameter of them all. If given, returns the last parameter matching the selection.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context).


Source: [removeLast.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/removeLast.js)

<a name="contextsort"></a>
## [Context](#context):[sort](#contextsort)(selection)

Get this context where the selected parameters are sorted
and placed at the end.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *selection*
  - an array of string, an ordered list of selected parameters.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [sort.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/sort.js)

<a name="contexttoarray"></a>
## [Context](#context):[toArray](#contexttoarray)()

Get the context as a serialized array.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- an array of parameter objects. Each object has properties:
  - *parameter*
    - a string
  - *value*
    - a string
  - *type*
    - an enum char, one of 'f', 'q', 'r'.


**Example:**
```
> context.toArray()
[{ parameter, value, type, typeChar }]
```

Source: [toArray.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/toArray.js)

<a name="contexttocontextobject"></a>
## [Context](#context):[toContextObject](#contexttocontextobject)()

Get as a plain context object.
If the context has duplicate keys, their values are joined with '__'.
Note that a plain object does not maintain the key order.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- an object


Source: [toContextObject.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/toContextObject.js)

<a name="contexttoquerystring"></a>
## [Context](#context):[toQueryString](#contexttoquerystring)()

Get the context as a query string, e.g. "f_genres.id=123&r_years=100to400"

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a string. Empty context returns empty query string `''`.


Source: [toQueryString.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/toQueryString.js)

<a name="contexttourlpath"></a>
## [Context](#context):[toUrlPath](#contexttourlpath)()

Build a local URL path and query from the context.
A path begins with a slash.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a string, for example "/?f_genres.id=123"


Source: [toUrlPath.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/toUrlPath.js)

<a name="contextfromcontextobject"></a>
## [Context](#context).[fromContextObject](#contextfromcontextobject)(obj)

Create a [Context](#context) from an object.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *obj*
  - an object with string keys and string values.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [fromContextObject.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/fromContextObject.js)

<a name="contextfromquerystring"></a>
## [Context](#context).[fromQueryString](#contextfromquerystring)(query)

Create a [Context](#context) from a query string, e.g. "f_genres.id=ABC"

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *query*
  - a string


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [fromQueryString.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/fromQueryString.js)

<a name="contextfromurlpath"></a>
## [Context](#context).[fromUrlPath](#contextfromurlpath)(path)

Create a context from a URL path, e.g. "/?f_genres.id=ABC"

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *path*
  - a string


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [fromUrlPath.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Context/fromUrlPath.js)

<a name="contextview"></a>
## [ContextView](#contextview)()

[ContextView](#contextview) is a viewer and editor for the current [Context](#context).


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [ContextView:getElement](#contextviewgetelement)
- [ContextView:render](#contextviewrender)


Source: [ContextView/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ContextView/index.js)

<a name="contextviewgetelement"></a>
## [ContextView](#contextview):[getElement](#contextviewgetelement)()

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [HTMLElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement)


Source: [getElement.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ContextView/getElement.js)

<a name="contextviewrender"></a>
## [ContextView](#contextview):[render](#contextviewrender)()

Render the current context.

Source: [render.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ContextView/render.js)

<a name="exporttools"></a>
## [ExportTools](#exporttools)()

Actions for the current set of entities.


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [ExportTools:getElement](#exporttoolsgetelement)
- [ExportTools:render](#exporttoolsrender)


Source: [ExportTools/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ExportTools/index.js)

<a name="exporttoolsgetelement"></a>
## [ExportTools](#exporttools):[getElement](#exporttoolsgetelement)()

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [HTMLElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement)


Source: [getElement.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ExportTools/getElement.js)

<a name="exporttoolsrender"></a>
## [ExportTools](#exporttools):[render](#exporttoolsrender)()

Render the current context.

Source: [render.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ExportTools/render.js)

<a name="listview"></a>
## [ListView](#listview)

Render the stratum as a list.


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [ListView:getElement](#listviewgetelement)
- [ListView:render](#listviewrender)


Source: [ListView/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ListView/index.js)

<a name="listviewgetelement"></a>
## [ListView](#listview):[getElement](#listviewgetelement)()

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [HTMLElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement)


Source: [getElement.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ListView/getElement.js)

<a name="listviewrender"></a>
## [ListView](#listview):[render](#listviewrender)()

Render current context as a list.

Source: [render.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ListView/render.js)

<a name="pathmanager"></a>
## [PathManager](#pathmanager)

The path manager handles URL navigation and browser history.pushState.


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [PathManager:readContext](#pathmanagerreadcontext)
- [PathManager:writeContext](#pathmanagerwritecontext)


Source: [PathManager/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/PathManager/index.js)

<a name="pathmanagerreadcontext"></a>
## [PathManager](#pathmanager):[readContext](#pathmanagerreadcontext)()

Read a [Context](#context) from the current page URL.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context)


Source: [readContext.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/PathManager/readContext.js)

<a name="pathmanagerwritecontext"></a>
## [PathManager](#pathmanager):[writeContext](#pathmanagerwritecontext)(context)

Rewrite the current URL to reflect the given [Context](#context).

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context)


Source: [writeContext.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/PathManager/writeContext.js)

<a name="reduxstore"></a>
## [ReduxStore](#reduxstore)

This is a Redux-like store for updating a [Context](#context) object
and reacting to its changes.


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [ReduxStore:dispatch](#reduxstoredispatch)
- [ReduxStore:getState](#reduxstoregetstate)
- [ReduxStore:hibernate](#reduxstorehibernate)
- [ReduxStore:hydrate](#reduxstorehydrate)
- [ReduxStore:select](#reduxstoreselect)
- [ReduxStore:subscribe](#reduxstoresubscribe)


Source: [ReduxStore/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ReduxStore/index.js)

<a name="reduxstoredispatch"></a>
## [ReduxStore](#reduxstore):[dispatch](#reduxstoredispatch)(action)

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *action*
  - an object to be processed by the reducer.


Source: [dispatch.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ReduxStore/dispatch.js)

<a name="reduxstoregetstate"></a>
## [ReduxStore](#reduxstore):[getState](#reduxstoregetstate)()

Get current state. In order to modify the state, dispatch actions.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- an object, the current state.


Source: [getState.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ReduxStore/getState.js)

<a name="reduxstorehibernate"></a>
## [ReduxStore](#reduxstore):[hibernate](#reduxstorehibernate)(storageKey, mapper)

Store current state to a persistent storage.
Use optional mapper function to control and limit what is stored.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *storageKey*
  - a string
- *mapper*
  - optional function (state) -> state. The output becomes stored.


Source: [hibernate.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ReduxStore/hibernate.js)

<a name="reduxstorehydrate"></a>
## [ReduxStore](#reduxstore):[hydrate](#reduxstorehydrate)(storageKey, mapper)

Restore state from persistent storage.
Use an optional mapper function to remove any unwanted stored properties.
The mapper is useful to sanitize the storage after schema changes.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *storageKey*
  - a string
- *mapper*
  - optional function (state) -> state. The output is hydrated.


Source: [hydrate.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ReduxStore/hydrate.js)

<a name="reduxstoreselect"></a>
## [ReduxStore](#reduxstore):[select](#reduxstoreselect)(selector, handler)

Run handler if there was a change in the selected substate.

<p style="margin-bottom: 0"><strong>Example:</strong></p>

- store.subscribe(store.select(state => state.msg, (msg) => {
  - console.log(msg)
- }))


<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *selector*
  - a function (state) => substate
- *handler*
  - a function (substate)


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a function


Source: [select.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ReduxStore/select.js)

<a name="reduxstoresubscribe"></a>
## [ReduxStore](#reduxstore):[subscribe](#reduxstoresubscribe)(handler)

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *handler*
  - a function () that is called when an action changes the state.


Source: [subscribe.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ReduxStore/subscribe.js)

<a name="searchform"></a>
## [SearchForm](#searchform)()

A search bar with autocompletion.

<p style="margin-bottom: 0">Emits</p>

- *submit*
  - when a non-empty search keyword is submitted



<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [SearchForm:getElement](#searchformgetelement)
- [SearchForm:initialize](#searchforminitialize)


Source: [SearchForm/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/SearchForm/index.js)

<a name="searchformgetelement"></a>
## [SearchForm](#searchform):[getElement](#searchformgetelement)()

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [HTMLElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement)


Source: [getElement.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/SearchForm/getElement.js)

<a name="searchforminitialize"></a>
## [SearchForm](#searchform):[initialize](#searchforminitialize)()

Initialize the autocomplete component and related event listeners.
This must be called once after element is mounted to the DOM.

Source: [initialize.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/SearchForm/initialize.js)

<a name="sidebar"></a>
## [Sidebar](#sidebar)()

A component for search tools and list view.


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [Sidebar:getElement](#sidebargetelement)
- [SidebarHandle:getElement](#sidebarhandlegetelement)
- [SidebarLite:getElement](#sidebarlitegetelement)


Source: [Sidebar/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sidebar/index.js)

<a name="sidebargetelement"></a>
## [Sidebar](#sidebar):[getElement](#sidebargetelement)()

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [HTMLElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement)


Source: [getElement.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sidebar/getElement.js)

<a name="sidebarhandle"></a>
## [SidebarHandle](#sidebarhandle)()

A component for search tools and list view.


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [SidebarHandle:getElement](#sidebarhandlegetelement)


Source: [SidebarHandle/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/SidebarHandle/index.js)

<a name="sidebarhandlegetelement"></a>
## [SidebarHandle](#sidebarhandle):[getElement](#sidebarhandlegetelement)()

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [HTMLElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement)


Source: [getElement.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/SidebarHandle/getElement.js)

<a name="sidebarlite"></a>
## [SidebarLite](#sidebarlite)()

A component for search and breadcrumbs when the sidebar is closed.


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [SidebarLite:getElement](#sidebarlitegetelement)


Source: [SidebarLite/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/SidebarLite/index.js)

<a name="sidebarlitegetelement"></a>
## [SidebarLite](#sidebarlite):[getElement](#sidebarlitegetelement)()

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [HTMLElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement)


Source: [getElement.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/SidebarLite/getElement.js)

<a name="sidebarmanager"></a>
## [SidebarManager](#sidebarmanager)(viewport)

TODO rename to ControlsManager

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *viewport*
  - a [tapspace](https://axelpale.github.io/tapspace/api/v2/) Viewport


<p style="margin-bottom: 0">The sidebar manager is responsible of interaction between:</p>

- *Sidebar*
- *SidebarHandle*
- Slimbar (search tools when sidebar closed)


Source: [SidebarManager/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/SidebarManager/index.js)

<a name="sky"></a>
## [Sky](#sky)(viewport)

[Sky](#sky) manages stratum objects and their loading.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *viewport*
  - a [tapspace](https://axelpale.github.io/tapspace/api/v2/).components.Viewport



<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [Sky:findLocalStratum](#skyfindlocalstratum)
- [Sky:findNodeByLabel](#skyfindnodebylabel)
- [Sky:findOpenableNode](#skyfindopenablenode)
- [Sky:getCurrentStratum](#skygetcurrentstratum)
- [Sky:zoomToNodeByLabel](#skyzoomtonodebylabel)
- [Sky:zoomToStratum](#skyzoomtostratum)
- [Sky.driver](#skydriver)
- [Sky.generator](#skygenerator)


Source: [Sky/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sky/index.js)

<a name="skyfindlocalstratum"></a>
## [Sky](#sky):[findLocalStratum](#skyfindlocalstratum)()

Find the stratum the user is likely currently looking at.
Can be null.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Stratum](#stratum)
- or null, if no stratum found.


Source: [findLocalStratum.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sky/findLocalStratum.js)

<a name="skyfindnodebylabel"></a>
## [Sky](#sky):[findNodeByLabel](#skyfindnodebylabel)(context, label)

Search for a facet node with the given label.
Prioritize nodes in the stratum of the given context.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context), defines the current stratum.
- *label*
  - a string, the label of the desired node


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [CategoryNode](#categorynode) or null if not found


Source: [findNodeByLabel.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sky/findNodeByLabel.js)

<a name="skyfindopenablenode"></a>
## [Sky](#sky):[findOpenableNode](#skyfindopenablenode)(subcontext)

Search for an openable node with the given subcontext.
In other words, find a node that opens to a stratum of the given context.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context), the subcontext of the node.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [CategoryNode](#categorynode) or null if not found


Source: [findOpenableNode.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sky/findOpenableNode.js)

<a name="skygetcurrentstratum"></a>
## [Sky](#sky):[getCurrentStratum](#skygetcurrentstratum)()

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Stratum](#stratum).
- a null if no stratum exist.


Source: [getCurrentStratum.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sky/getCurrentStratum.js)

<a name="skyzoomtonodebylabel"></a>
## [Sky](#sky):[zoomToNodeByLabel](#skyzoomtonodebylabel)(context, label)

Jump viewport to certain stratum node if such node is available
and open the node.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context), defines the current stratum.
- *label*
  - a string, the label of the node


Source: [zoomToNodeByLabel.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sky/zoomToNodeByLabel.js)

<a name="skyzoomtostratum"></a>
## [Sky](#sky):[zoomToStratum](#skyzoomtostratum)(context)

Jump viewport to certain stratum and load the path if needed.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context), defines the current stratum.


Source: [zoomToStratum.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sky/zoomToStratum.js)

<a name="skydriver"></a>
## [Sky](#sky).[driver](#skydriver)(sky, loader)

Driver for TreeLoader.
Driver is a handler ran at each viewport idle event.
Driver is responsible of triggering semantic zooming effects.
Driver initiates the loading of sub- and superstrata.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *sky*
- *loader*


Source: [driver.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sky/driver.js)

<a name="skygenerator"></a>
## [Sky](#sky).[generator](#skygenerator)(sky, loader)

Generator for TreeLoader. Generator defines how the loaded spaces
are constructed and destructed.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *sky*
- *loader*


Source: [generator.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Sky/generator.js)

<a name="spinner"></a>
## [Spinner](#spinner)([options])

N nodes rotating.
[Spinner](#spinner) provides a transparent and easy to configure loading animation
that is resistant to zooming without pixel resolution issues.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *options*
  - optional object with properties
    - *diameter*
      - a number, the spinner diameter in pixels. Half of the value equals the rotation radius.
    - *circles*
      - a positive integer, the number of rotating circles.
    - *circleDiameter*
      - a number, the diameter of rotating circles.



<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [Spinner:start](#spinnerstart)
- [Spinner:stop](#spinnerstop)


Source: [Spinner/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Spinner/index.js)

<a name="spinnerstart"></a>
## [Spinner](#spinner):[start](#spinnerstart)()

Start spinning, reveal the spinner.

Source: [start.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Spinner/start.js)

<a name="spinnerstop"></a>
## [Spinner](#spinner):[stop](#spinnerstop)()

Stop spinning, hide the spinner.

Source: [stop.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Spinner/stop.js)

<a name="stratum"></a>
## [Stratum](#stratum)(context)

Inherits [Emitter](https://www.npmjs.com/package/component-emitter)

[Stratum](#stratum) is a layer in a nested hierarhcy of graphs.
[Stratum](#stratum) is a graph laid on a plane.

The constructor does not add the graph to the document.
Append stratum.space to a parent space in order to do that.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context). The context gives identity to the stratum and defines the faceting and filtering of its content.


<p style="margin-bottom: 0"><a href="#stratum">Stratum</a> emits:</p>

- *final*
  - when the stratum has been loaded and rendered.
- *substratumrequest*
  - when the stratum would like one of its nodes to be opened as a new stratum. Emitted with an object { context } where the context is the requested context of the substratum.
- *layout*
  - when the stratum layout changes



<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [Stratum:addVirtualNode](#stratumaddvirtualnode)
- [Stratum:enableFaceting](#stratumenablefaceting)
- [Stratum:findNearbyNode](#stratumfindnearbynode)
- [Stratum:getBasisForSubstratum](#stratumgetbasisforsubstratum)
- [Stratum:getEverySubcontext](#stratumgeteverysubcontext)
- [Stratum:getNodes](#stratumgetnodes)
- [Stratum:getOpenableNode](#stratumgetopenablenode)
- [Stratum:getOrigin](#stratumgetorigin)
- [Stratum:getSpace](#stratumgetspace)
- [Stratum:getSupercontext](#stratumgetsupercontext)
- [Stratum:limitZoom](#stratumlimitzoom)
- [Stratum:load](#stratumload)
- [Stratum:remove](#stratumremove)
- [Stratum:render](#stratumrender)
- [Stratum:renderEdges](#stratumrenderedges)
- [Stratum:renderNodes](#stratumrendernodes)
- [Stratum:revealDetails](#stratumrevealdetails)
- [Stratum:scaleNodesToFit](#stratumscalenodestofit)
- [Stratum:triggerSubstratum](#stratumtriggersubstratum)
- [StratumNode:getOrigin](#stratumnodegetorigin)
- [StratumNode:getRadius](#stratumnodegetradius)
- [StratumNode:getSubcontext](#stratumnodegetsubcontext)
- [StratumNode:remove](#stratumnoderemove)
- [StratumNode:render](#stratumnoderender)
- [StratumNode:setScale](#stratumnodesetscale)
- [StratumNode:translateTo](#stratumnodetranslateto)
- [StratumNode:update](#stratumnodeupdate)


Source: [Stratum/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/index.js)

<a name="stratumaddvirtualnode"></a>
## [Stratum](#stratum):[addVirtualNode](#stratumaddvirtualnode)(subcontext, substratum)

Sometimes we need to enforce stratum to contain certain subcontext
in order to provide a way to access to certain substratum.
For that we create a virtual facet node.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *subcontext*
  - a [Context](#context)
- *substratum*
  - a [Stratum](#stratum)


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [StratumNode](#stratumnode) or null


Source: [addVirtualNode.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/addVirtualNode.js)

<a name="stratumenablefaceting"></a>
## [Stratum](#stratum):[enableFaceting](#stratumenablefaceting)()

Enable stratum faceting.
In other words, begin to listen facet nodes for opening requests.

Source: [enableFaceting.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/enableFaceting.js)

<a name="stratumfindnearbynode"></a>
## [Stratum](#stratum):[findNearbyNode](#stratumfindnearbynode)()

Find a stratum node close to viewport. The result can be null.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [StratumNode](#stratumnode)


Source: [findNearbyNode.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/findNearbyNode.js)

<a name="stratumgetbasisforsubstratum"></a>
## [Stratum](#stratum):[getBasisForSubstratum](#stratumgetbasisforsubstratum)(subcontext)

The method returns a basis for the given subcontext.
The basis equals the basis of the openable node that
represents the substratum.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *subcontext*
  - a [Context](#context)


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [tapspace](https://axelpale.github.io/tapspace/api/v2/).geometry.Basis or null if no basis found for the subcontext.


Source: [getBasisForSubstratum.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/getBasisForSubstratum.js)

<a name="stratumgeteverysubcontext"></a>
## [Stratum](#stratum):[getEverySubcontext](#stratumgeteverysubcontext)()

Get the substratum context for each openable node.
Does not return subcontext of warp nodes as they do not open deeper.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- an array of [Context](#context). The array can be empty.


Source: [getEverySubcontext.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/getEverySubcontext.js)

<a name="stratumgetnodes"></a>
## [Stratum](#stratum):[getNodes](#stratumgetnodes)()

Get stratum nodes in an array.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- array of [StratumNode](#stratumnode)


Source: [getNodes.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/getNodes.js)

<a name="stratumgetopenablenode"></a>
## [Stratum](#stratum):[getOpenableNode](#stratumgetopenablenode)(subcontext)

Get a node by its subcontext. Can be null if no node found.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *subcontext*
  - a [Context](#context). The subcontext of the node.


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [StratumNode](#stratumnode) or null


Source: [getOpenableNode.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/getOpenableNode.js)

<a name="stratumgetorigin"></a>
## [Stratum](#stratum):[getOrigin](#stratumgetorigin)()

Get the origin point tensor of the stratum.
Useful for determining the anchor point of the stratum.
In future we might want the root node be the origin.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [tapspace](https://axelpale.github.io/tapspace/api/v2/).geometry.Point


Source: [getOrigin.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/getOrigin.js)

<a name="stratumgetspace"></a>
## [Stratum](#stratum):[getSpace](#stratumgetspace)()

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [tapspace](https://axelpale.github.io/tapspace/api/v2/).components.Plane


Source: [getSpace.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/getSpace.js)

<a name="stratumgetsupercontext"></a>
## [Stratum](#stratum):[getSupercontext](#stratumgetsupercontext)()

Get faceting context for the superstratum.
Basically takes the stratum context, and returns a new context
with the last faceting parameter removed.
This method needs to perfectly reverse the getEverySubcontext
to keep the strata in stable order.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context) or null is stratum is root.


Source: [getSupercontext.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/getSupercontext.js)

<a name="stratumlimitzoom"></a>
## [Stratum](#stratum):[limitZoom](#stratumlimitzoom)([areaRatioLimit])

Limit the zoom scale by moving the viewport closer.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *areaRatioLimit*
  - optional positive number, default is 0.1


Source: [limitZoom.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/limitZoom.js)

<a name="stratumload"></a>
## [Stratum](#stratum):[load](#stratumload)()

Carry out the stratum loading process from the server.
A stratum is loaded according to its context.
This method is idempotent.

Source: [load.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/load.js)

<a name="stratumremove"></a>
## [Stratum](#stratum):[remove](#stratumremove)()

Destroy the stratum. Stop loading and kill listeners.
However, does not remove the stratum from the DOM.
Let DOM removal happen externally by driver/generator.

Source: [remove.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/remove.js)

<a name="stratumrender"></a>
## [Stratum](#stratum):[render](#stratumrender)()

Render the graph. If elements already exist, update.
This method is idempotent, thus you can call this method multiple times.

Source: [render.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/render.js)

<a name="stratumrenderedges"></a>
## [Stratum](#stratum):[renderEdges](#stratumrenderedges)()

Draw or update edges.

Source: [renderEdges.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/renderEdges.js)

<a name="stratumrendernodes"></a>
## [Stratum](#stratum):[renderNodes](#stratumrendernodes)()

Render nodes or refresh the node positions.

Source: [renderNodes.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/renderNodes.js)

<a name="stratumrevealdetails"></a>
## [Stratum](#stratum):[revealDetails](#stratumrevealdetails)()

The method computes the sizes of elements in the current
viewport scale and reveals or hides details respectively.
In other words, it refresh the label visibility in a semantic zoom manner.
It reveals and hides the labels based on their apparent size.

Source: [revealDetails.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/revealDetails.js)

<a name="stratumscalenodestofit"></a>
## [Stratum](#stratum):[scaleNodesToFit](#stratumscalenodestofit)()

Transforms the node plane so that it fits the rendering boundary
for the stratum. This aims to keep the nodes at practical size.

Source: [scaleNodesToFit.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/scaleNodesToFit.js)

<a name="stratumtriggersubstratum"></a>
## [Stratum](#stratum):[triggerSubstratum](#stratumtriggersubstratum)()

This method implements semantic zooming effects.
The effects depend on the scale, area, and position of
the stratum and its nodes.

The method can emit 'substratumrequest' that subsequently initiates
the loading of substratum, given that the zoom conditions so allow.

Source: [triggerSubstratum.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/Stratum/triggerSubstratum.js)

<a name="stratumnode"></a>
## [StratumNode](#stratumnode)

Abstract class for [CategoryNode](#categorynode).
Useful for ensuring [Sky](#sky) can handle nodes through the same interface.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context), the faceting context
- *key*
  - a string, the node ID, stratum-specific
- *data*
  - an object, the node data object required for rendering.



<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [StratumNode:getOrigin](#stratumnodegetorigin)
- [StratumNode:getRadius](#stratumnodegetradius)
- [StratumNode:getSubcontext](#stratumnodegetsubcontext)
- [StratumNode:remove](#stratumnoderemove)
- [StratumNode:render](#stratumnoderender)
- [StratumNode:setScale](#stratumnodesetscale)
- [StratumNode:translateTo](#stratumnodetranslateto)
- [StratumNode:update](#stratumnodeupdate)


Source: [StratumNode/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/StratumNode/index.js)

<a name="stratumnodegetorigin"></a>
## [StratumNode](#stratumnode):[getOrigin](#stratumnodegetorigin)()

Get node center point as a point tensor.
Useful for getting the node position in a consistent manner.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [tapspace](https://axelpale.github.io/tapspace/api/v2/).geometry.Point


Source: [getOrigin.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/StratumNode/getOrigin.js)

<a name="stratumnodegetradius"></a>
## [StratumNode](#stratumnode):[getRadius](#stratumnodegetradius)()

Get node radius as a distance tensor.
Useful for trimming edges between nodes.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [tapspace](https://axelpale.github.io/tapspace/api/v2/).geometry.Distance


Source: [getRadius.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/StratumNode/getRadius.js)

<a name="stratumnodegetsubcontext"></a>
## [StratumNode](#stratumnode):[getSubcontext](#stratumnodegetsubcontext)()

Get the faceting context of the node if any.
With the subcontext the node is able to open a stratum.

<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a [Context](#context) or null if no subcontext


<p style="margin-bottom: 0">See also</p>

- [Stratum:getSupercontext](#stratumgetsupercontext) which mirrors the logic.


Source: [getSubcontext.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/StratumNode/getSubcontext.js)

<a name="stratumnoderemove"></a>
## [StratumNode](#stratumnode):[remove](#stratumnoderemove)()

Remove the rendered node. Useful when the graph has been filtered.

Source: [remove.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/StratumNode/remove.js)

<a name="stratumnoderender"></a>
## [StratumNode](#stratumnode):[render](#stratumnoderender)()

Render the node. A placeholder that ensures subclass implements render.

Source: [render.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/StratumNode/render.js)

<a name="stratumnodesetscale"></a>
## [StratumNode](#stratumnode):[setScale](#stratumnodesetscale)(scale)

Rescale the node.
Useful when the graph model layout has changed.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *scale*
  - a non-negative number


Source: [setScale.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/StratumNode/setScale.js)

<a name="stratumnodetranslateto"></a>
## [StratumNode](#stratumnode):[translateTo](#stratumnodetranslateto)(point[, animate])

Move the node to a position.
Useful when the graph model layout has changed.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *point*
  - a [tapspace](https://axelpale.github.io/tapspace/api/v2/).geometry.Point
- *animate*
  - optional boolean. False by default = no animation.


Source: [translateTo.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/StratumNode/translateTo.js)

<a name="stratumnodeupdate"></a>
## [StratumNode](#stratumnode):[update](#stratumnodeupdate)(data)

Update node data and render.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *data*
  - an object


Source: [update.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/StratumNode/update.js)

<a name="viewportmanager"></a>
## [ViewportManager](#viewportmanager)()

The viewport manager handles the [tapspace](https://axelpale.github.io/tapspace/api/v2/).components.Viewport setup.
The manager can be used to enable or disable interaction.

For example, we would like to avoid user to interact with
the viewport before there is something to show.
Otherwise a few accidental moves could pan the viewport somewhere
where there will be no content.


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [ViewportManager:enableNavigation](#viewportmanagerenablenavigation)
- [ViewportManager:getViewport](#viewportmanagergetviewport)


Source: [ViewportManager/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ViewportManager/index.js)

<a name="viewportmanagerenablenavigation"></a>
## [ViewportManager](#viewportmanager):[enableNavigation](#viewportmanagerenablenavigation)()

Enable viewport navigation: cursor panning, wheel zooming, zoom buttons,
and others.

It is recommended to call this only after the space has
some visible content so that user does not get lost into empty space.

Source: [enableNavigation.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ViewportManager/enableNavigation.js)

<a name="viewportmanagergetviewport"></a>
## [ViewportManager](#viewportmanager):[getViewport](#viewportmanagergetviewport)()

Get the Viewport object.

Source: [getViewport.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/ViewportManager/getViewport.js)

<a name="io"></a>
## [io](#io)

Stratocumulus client-side IO.

Gather here all code that handles sending and receiving data from
servers and databases. This helps a lot when migrating to new or
updated backend APIs.


<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [io.Cache](#iocache)


Source: [io/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/index.js)

<a name="iocache"></a>
## [io](#io).[Cache](#iocache)(sizeLimit)

A helper class for caching http requests client-side.
Discards cached contents in first-in-first-out manner.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *sizeLimit*
  - a positive integer. How many values to store.



<p style="margin-bottom: 0"><strong>Contents:</strong></p>


- [io.Cache:clear](#iocacheclear)
- [io.Cache:has](#iocachehas)
- [io.Cache:read](#iocacheread)
- [io.Cache:remove](#iocacheremove)
- [io.Cache:write](#iocachewrite)


Source: [Cache/index.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/Cache/index.js)

<a name="iocacheclear"></a>
## [io](#io).[Cache](#iocache):[clear](#iocacheclear)()

Clear the cache. See :remove to clear specific key.

Source: [clear.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/Cache/clear.js)

<a name="iocachehas"></a>
## [io](#io).[Cache](#iocache):[has](#iocachehas)(key)

Check if there is a cached value for the key.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *key*
  - a string


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a boolean


Source: [has.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/Cache/has.js)

<a name="iocacheread"></a>
## [io](#io).[Cache](#iocache):[read](#iocacheread)(key)

Read a cached value. Null if not found.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *key*
  - a string


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- a value or null


Source: [read.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/Cache/read.js)

<a name="iocacheremove"></a>
## [io](#io).[Cache](#iocache):[remove](#iocacheremove)(key)

Remove a cached value and return it.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *key*
  - a string


<p style="margin-bottom: 0"><strong>Returns:</strong></p>

- the removed value or null if it did not exist.


Source: [remove.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/Cache/remove.js)

<a name="iocachewrite"></a>
## [io](#io).[Cache](#iocache):[write](#iocachewrite)(key, payload)

Cache an object or update it.
If the cache is full, this operation will discard the oldest entry.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *key*
  - a string
- *payload*
  - a value. Cannot be null.


In the rare case where the cache size limit is set to zero,
no value will be cached.

Source: [write.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/Cache/write.js)

<a name="ioentityapifetchentity"></a>
## [io](#io).[entityApi](#ioentityapi).[fetchEntity](#ioentityapifetchentity)(entityType, id, callback)

Fetch single entity from the Entity API.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *entityType*
  - a string, the entity index name.
- *id*
  - a string, the entity ID.
- *callback*
  - a function (err, entity)


Source: [entityApi.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/entityApi.js)

<a name="ioentityapifetchentitypage"></a>
## [io](#io).[entityApi](#ioentityapi).[fetchEntityPage](#ioentityapifetchentitypage)(context, callback)

Fetch single page of entities.

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context) with 'context_type' and 'page' parameters set.
- *callback*
  - a function (err, page) where
    - *page*
      - an object { pageNumber, entityIds }


Source: [entityApi.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/entityApi.js)

<a name="iostratumapifetchstratum"></a>
## [io](#io).[stratumApi](#iostratumapi).[fetchStratum](#iostratumapifetchstratum)(context, callback)

Get stratum for the give filtering context

<p style="margin-bottom: 0"><strong>Parameters:</strong></p>

- *context*
  - a [Context](#context), the filtering context
- *callback*
  - a function (err, stratum)


Source: [stratumApi.js](https://github.com/ARC-code/stratocumulus/blob/main/client/lib/io/stratumApi.js)

<p style="text-align: right">
<a href="#top">&uarr; Back To Top</a>
</p>

