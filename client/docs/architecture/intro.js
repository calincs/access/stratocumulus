module.exports = `
Welcome to Stratocumulus client-side documentation.

This document follows these naming conventions: \`ClassName\`, \`namespace\`, \`.CONSTANT\`, \`.classMethod()\`, \`:instanceProperty\`, \`:instanceMethod()\`, and \`[optionalParameter]\`.

See also: [Introduction](https://gitlab.com/calincs/access/stratocumulus) – [Examples](https://gitlab.com/calincs/access/stratocumulus#examples) - [GitHub](https://gitlab.com/calincs/access/stratocumulus)

This document is generated with [yamdog](https://github.com/axelpale/yamdog).

## Overview

The frontend code is located under \`client\` directory.
As the frontend will consists of multiple javascript-files, stylesheets, and images,
they must be bundled together and be served as static files by the backend.
For the bundling process, the frontend has a dedicated Docker container that
runs [webpack](https://webpack.js.org/).
Webpack places the finished bundle to a volume that is shared with the container running the backend.
In addition to the bundle, the backend serves an HTML page defined in \`server/templates/<plugin>/index.html\`.
When a user opens the page, the browser requests the bundle and other assets from the backend.

In the most general level, the client does the following:
- Loading the page causes the client to a) subscribe to the SSE channel and dictate what happens when subgraph messages are received on the channel's stream, and then b) call the /build_stratum endpoint. The code for these actions resides in the \`client/lib/io\` module.
- Upon receiving subgraph messages on the SSE channel, the client attempts to perform a circle pack layout of the graph. This is intended to provide initial x/y coordinates for each node, grouping nodes together according to its parent. The code for this is in the \`render\` method of each stratum class.
- Nodes in the graph are openable and will load and display a faceted version of the same stratum. Users can navigate further down inside the nodes to inspect the results. The code that handles automatic opening of nodes and recursive loading of nested strata resides in the \`client/lib/Sky\` component.
`
