// @0 Stratocumulus
//
// Components:
// - Stratum, a network graph of facetable nodes.
// - StratumNode, an openable node that represents a collection of entities.
// - ContextForm, a widget to display the current navigation path and filters.
// - SearchForm, a text search form
// - Sky, a loader for nested strata
// - ViewportManager, a helper class to setup Sky viewport.
//
// Data Models:
// - Context, an ordered set of filtering and faceting parameters and values
// - ReduxStore, a minimal redux-like state management
//
// Adapters:
// - io.entityApi, methods to fetch entities
// - io.stratumApi, methods to fetch stratum graphs
//
// Libraries:
// - tapspace, provides zoomable user interface
// - graphology, provides graph models
//

// NOTE
// This is the client root index.
// If you modify this file, you need to rebuild the docker image
// for the changes to take effect.

// Make webpack to copy assets to dist/
require('./themes/lincs_dark.css')
require.context('./themes/images/', true, /\.(jpg|png|svg|gif)$/)

const app = require('./lib')

// Start the app only after all HTML and scripts are loaded.
document.addEventListener('DOMContentLoaded', function () {
  app.start()
})
