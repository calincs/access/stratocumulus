const Cache = require('../../lib/io/Cache')

module.exports = (test) => {
  test('Cache basic usage', (t) => {
    const cache = new Cache(3)

    t.equal(cache.has('foo'), false, 'should not have key')
    cache.write('foo', {})
    t.equal(cache.has('foo'), true, 'should have key')

    cache.write('bar', { msg: 'babar' })
    cache.write('baz', {})
    cache.write('biz', { msg: 'hello' })

    t.equal(cache.has('foo'), false, 'should have discarded the oldest key')
    t.equal(cache.has('bar'), true, 'should still have second oldest key')
    t.equal(cache.has('biz'), true, 'should still have newest key')

    t.equal(cache.read('foo'), null, 'should be null if not exist')
    t.equal(cache.read('biz').msg, 'hello', 'should be readable')

    t.equal(cache.remove('bar').msg, 'babar', 'should return removed value')
    t.equal(cache.has('bar'), false, 'should have removed value')
    t.equal(cache.has('baz'), true, 'should still have other values')

    cache.clear()
    t.equal(cache.has('baz'), false, 'should have no values')
    t.equal(cache.has('biz'), false, 'should have no values')

    t.end()
  })

  test('Cache extreme parameters', (t) => {
    const zerocache = new Cache(0)

    zerocache.write('foo', {})
    t.equal(zerocache.read('foo'), null, 'should not add to zero sized cache')
    t.equal(zerocache.has('foo'), false, 'should not add to zero sized cache')

    t.throws(() => {
      const neg = new Cache(-1)
    }, /Bad cache size/, 'should detect negative cache size')

    const unicache = new Cache(1)

    t.throws(() => {
      unicache.write('foo')
    }, /Bad cache payload/, 'should detect missing payload')
    t.throws(() => {
      unicache.write('', { msg: 'hello' })
    }, /Bad cache key/, 'should detect nullish key')

    t.end()
  })

  test('Cache read should refresh position', (t) => {
    const bicache = new Cache(2)

    bicache.write('foo', { msg: 'foo' })
    bicache.write('bar', { msg: 'bar' })
    bicache.write('baz', { msg: 'baz' })

    t.equal(bicache.read('foo'), null, 'should have discarded')

    bicache.read('bar')
    bicache.write('biz', { msg: 'biz' })

    t.equal(bicache.read('baz'), null, 'should have discarded baz')
    t.equal(bicache.read('bar').msg, 'bar', 'should have kept bar')

    t.end()
  })
}
