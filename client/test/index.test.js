const test = require('tape')

const units = {
  Cache: require('./Cache'),
  Context: require('./Context')
}

Object.keys(units).forEach((unitName) => {
  units[unitName](test)
})
