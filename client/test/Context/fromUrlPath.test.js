module.exports = (test, Context) => {
  test('Context.fromUrlPath', (t) => {
    t.deepEqual(
      Context.fromUrlPath('/').toContextObject(),
      {},
      'trivial empty context'
    )

    t.throws(() => {
      Context.fromUrlPath('')
    }, 'should detect empty path')

    t.throws(() => {
      Context.fromUrlPath('a')
    }, 'should detect unexpected path')

    t.deepEqual(
      Context.fromUrlPath('/?f_genres.id=ABC').toContextObject(),
      { 'f_genres.id': 'ABC' },
      'should handle simple query'
    )

    t.deepEqual(
      Context.fromUrlPath('https://example.com?f_genres.id=ABC').toContextObject(),
      { 'f_genres.id': 'ABC' },
      'should handle full url'
    )

    const q = '/?f_genres.id=ABC&f_genres.id=BCD&f_disciplines.id=E'
    t.deepEqual(
      Context.fromUrlPath(q).toContextObject(),
      { 'f_genres.id': 'ABC__BCD', 'f_disciplines.id': 'E' },
      'should handle duplicate keys'
    )

    t.end()
  })
}
