module.exports = (test, Context) => {
  test('Context:sort', (t) => {
    const empty = new Context([], [])

    t.deepEqual(
      empty.sort([]).toContextObject(),
      {},
      'trivial empty'
    )

    const ctx = new Context(
      ['f_genres.id', 'r_years', 'q', 'f_genres.id'],
      ['ABC', '200to300', 'keyword', 'GHI']
    )

    t.equal(
      ctx.sort(['q', 'r_years']).toQueryString(),
      'f_genres.id=ABC&f_genres.id=GHI&q=keyword&r_years=200to300',
      'should have filters last and in the given order'
    )

    t.end()
  })
}
