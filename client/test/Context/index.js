// Mock environment for Context
window = {
  stratocumulus: {
    facetParameters: []
  },
  decodeURIComponent: (str) => {
    return str
  },
  encodeURIComponent: (str) => {
    return str
  }
}

const Context = require('../../lib/Context')

const units = {
  append: require('./append.test'),
  constructor: require('./constructor.test'),
  copy: require('./copy.test'),
  drop: require('./drop.test'),
  each: require('./each.test'),
  equals: require('./equals.test'),
  filter: require('./filter.test'),
  fromContextObject: require('./fromContextObject.test'),
  fromQueryString: require('./fromQueryString.test'),
  fromUrlPath: require('./fromUrlPath.test'),
  getIntegerValue: require('./getIntegerValue.test'),
  getLast: require('./getLast.test'),
  getRangeValue: require('./getRangeValue.test'),
  getValue: require('./getValue.test'),
  hasParameter: require('./hasParameter.test'),
  hasValue: require('./hasValue.test'),
  map: require('./map.test'),
  merge: require('./merge.test'),
  pick: require('./pick.test'),
  remove: require('./remove.test'),
  removeLast: require('./removeLast.test'),
  sort: require('./sort.test'),
  toArray: require('./toArray.test'),
  toQueryString: require('./toQueryString.test'),
  toUrlPath: require('./toUrlPath.test')
}

module.exports = (test) => {
  Object.keys(units).forEach((unitName) => {
    units[unitName](test, Context)
  })
}
