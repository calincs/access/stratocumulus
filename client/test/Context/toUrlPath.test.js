module.exports = (test, Context) => {
  test('Context:toUrlPath', (t) => {
    const empty = new Context([])

    t.equal(
      empty.toUrlPath(),
      '/',
      'should be root url path'
    )

    const ctx = new Context(
      ['f_genres.id', 'f_disciplines.id', 'r_years'],
      ['ABC', 'E', '200to400']
    )

    t.equal(
      ctx.toUrlPath(),
      '/?f_genres.id=ABC&f_disciplines.id=E&r_years=200to400',
      'should have correct prefix'
    )

    t.end()
  })
}
