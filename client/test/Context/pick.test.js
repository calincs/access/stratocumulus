module.exports = (test, Context) => {
  test('Context:pick', (t) => {
    const empty = new Context([], [])

    t.deepEqual(
      empty.pick([]).toContextObject(),
      {},
      'trivial empty'
    )

    const ctx = new Context(
      ['f_genres.id', 'r_years', 'q', 'f_disciplines.id'],
      ['ABC', '200to300', 'keyword', 'DEF']
    )

    t.equal(
      ctx.pick(['f_genres.id', 'f_disciplines.id']).toQueryString(),
      'f_genres.id=ABC&f_disciplines.id=DEF',
      'should have only facets in the given order'
    )

    t.equal(
      ctx.pick(['q', 'r_years']).toQueryString(),
      'r_years=200to300&q=keyword',
      'should preserve filter order'
    )

    t.end()
  })
}
