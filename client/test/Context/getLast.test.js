module.exports = (test, Context) => {
  test('Context:getLast', (t) => {
    const empty = new Context([])

    t.equal(
      empty.getLast(),
      null,
      'should be null'
    )

    const ctx = new Context(
      ['f_genres.id', 'f_disciplines.id', 'r_years'],
      ['ABC', 'E', '200to400']
    )

    t.deepEqual(
      ctx.getLast(),
      {
        parameter: 'r_years',
        value: '200to400'
      },
      'should return the last'
    )

    t.deepEqual(
      ctx.getLast(['f_disciplines.id', 'f_genres.id']),
      {
        parameter: 'f_disciplines.id',
        value: 'E'
      },
      'should have correct structure and values'
    )

    t.end()
  })
}
