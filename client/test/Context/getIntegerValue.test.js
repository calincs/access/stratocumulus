module.exports = (test, Context) => {
  test('Context:getIntegerValue', (t) => {
    const empty = new Context([])

    t.equal(
      empty.getIntegerValue('page', 1),
      1,
      'should be the default'
    )

    const ctx = new Context(
      ['f_genres.id', 'f_disciplines.id', 'page'],
      ['ABC', 'E', '2']
    )

    t.equal(
      ctx.getIntegerValue('page'),
      2,
      'should have correct value'
    )

    t.end()
  })
}
