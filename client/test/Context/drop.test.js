module.exports = (test, Context) => {
  test('Context:drop', (t) => {
    const empty = new Context([], [])

    t.deepEqual(
      empty.drop([]).toContextObject(),
      {},
      'trivial empty'
    )

    const ctx = new Context(
      ['f_genres.id', 'r_years', 'q', 'f_disciplines.id'],
      ['ABC', '200to300', 'keyword', 'DEF']
    )

    t.equal(
      ctx.drop(['f_genres.id', 'f_disciplines.id']).toQueryString(),
      'r_years=200to300&q=keyword',
      'should have dropped selected facets and preseved order'
    )

    t.end()
  })
}
