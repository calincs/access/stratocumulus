module.exports = (test, Context) => {
  test('Context:removeLast', (t) => {
    const empty = new Context([], [])

    t.deepEqual(
      empty.removeLast().toContextObject(),
      {},
      'trivial empty remove'
    )

    const ctxa = new Context(['f_genres.id', 'r_years'], ['ABC', '200to400'])

    const selection = ['f_genres.id', 'f_disciplines.id']
    t.deepEqual(
      ctxa.removeLast(selection).toContextObject(),
      { 'r_years': '200to400' },
      'should remove only selected parameter'
    )

    const ctxb = new Context(
      ['f_genres.id', 'r_years', 'f_disciplines.id', 'f_disciplines.id'],
      ['ABC', '200to400', 'BCD', 'EFG']
    )

    t.deepEqual(
      ctxb.removeLast(selection).toContextObject(),
      {
        'f_genres.id': 'ABC',
        'r_years': '200to400',
        'f_disciplines.id': 'BCD'
      },
      'should remove only last selected value'
    )

    const ctxc = ctxb.append('f_genres.id', 'BCD')
    t.deepEqual(
      ctxc.removeLast(selection).toContextObject(),
      {
        'f_genres.id': 'ABC',
        'r_years': '200to400',
        'f_disciplines.id': 'BCD__EFG'
      },
      'should remove only the last appended and selected value'
    )

    t.end()
  })
}
