const storage = window.localStorage

const defaultMapper = state => state

module.exports = function (storageKey, mapper) {
  // @ReduxStore:hydrate(storageKey, mapper)
  //
  // Restore state from persistent storage.
  // Use an optional mapper function to remove any unwanted stored properties.
  // The mapper is useful to sanitize the storage after schema changes.
  //
  // Parameters:
  //   storageKey
  //     a string
  //   mapper
  //     optional function (state) -> state. The output is hydrated.
  //

  if (!mapper) {
    mapper = defaultMapper
  }
  if (typeof mapper !== 'function') {
    throw new Error('Bad hydration mapper function: ' + mapper)
  }

  const serialized = storage.getItem(storageKey)

  if (!serialized) {
    // No persistent data found.
    return
  }

  let hydratedState
  try {
    hydratedState = JSON.parse(serialized)
  } catch (err) {
    // Hydration failed. Maybe old version or corrupted store.
    // No need to crash.
    console.warn('Hydration failed: ', err.message)
    return
  }

  const mappedState = mapper(hydratedState)

  // Replace the hydrated properties
  this.state = Object.assign({}, this.state, mappedState)
}
