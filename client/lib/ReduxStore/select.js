module.exports = function (selector, handler) {
  // @ReduxStore:select(selector, handler)
  //
  // Run handler if there was a change in the selected substate.
  //
  // Example:
  //   store.subscribe(store.select(state => state.msg, (msg) => {
  //     console.log(msg)
  //   }))
  //
  // Parameters:
  //   selector
  //     a function (state) => substate
  //   handler
  //     a function (substate)
  //
  // Returns
  //   a function
  //

  // Save the first reference point immediately
  // in order to prevent calling the handler even when nothing is changed.
  let memory = selector(this.state)

  return () => {
    const substate = selector(this.state)
    if (substate !== memory) {
      // Selected substate was changed
      memory = substate
      // Activate handler
      handler(substate)
    }
  }
}
