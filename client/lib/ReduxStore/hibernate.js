const storage = window.localStorage

const defaultMapper = state => state

module.exports = function (storageKey, mapper) {
  // @ReduxStore:hibernate(storageKey, mapper)
  //
  // Store current state to a persistent storage.
  // Use optional mapper function to control and limit what is stored.
  //
  // Parameters:
  //   storageKey
  //     a string
  //   mapper
  //     optional function (state) -> state. The output becomes stored.
  //

  if (!mapper) {
    mapper = defaultMapper
  }
  if (typeof mapper !== 'function') {
    throw new Error('Bad hibernation mapper function: ' + mapper)
  }

  const mappedState = mapper(this.state)

  const serialized = JSON.stringify(mappedState)
  storage.setItem(storageKey, serialized)
}
