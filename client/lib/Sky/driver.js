const io = require('../io')

module.exports = (sky, loader) => {
  // @Sky.driver(sky, loader)
  //
  // Driver for TreeLoader.
  // Driver is a handler ran at each viewport idle event.
  // Driver is responsible of triggering semantic zooming effects.
  // Driver initiates the loading of sub- and superstrata.
  //
  // Parameters:
  //   sky
  //   loader
  //

  sky.viewport.on('idle', () => {
    // DEBUG
    // console.log('sky.strata:', Object.keys(sky.strata).join(', '))
    // console.log('loader.spaces:', Object.keys(loader.spaces).join(', '))

    // Remove all too small spaces immediately.
    // Do this to avoid singular inversions.
    const singulars = sky.viewport.findSingular()
    singulars.forEach(space => {
      const spaceId = space.stratum.path
      space.stratum.remove()
      delete sky.strata[spaceId]
      loader.removeSpace(spaceId)
    })

    // Find closest stratum, our current location.
    const currentStratum = sky.findLocalStratum()
    if (!currentStratum) {
      // No current stratum; no stratum yet available.
      // Skip idle refresh.
      return
    }

    console.log('currently nearest stratum:', sky.currentStratumPath)
    if (sky.currentStratumPath !== currentStratum.path) {
      // Change detected. We are in a new stratum.
      const previousStratumPath = sky.currentStratumPath
      sky.currentStratumPath = currentStratum.path

      // Classify the stratum space as current. Clear others.
      const currentSpace = currentStratum.getSpace()
      currentSpace.addClass('current-stratum')
      loader.getSpaces().forEach(space => {
        if (space !== currentSpace) {
          space.removeClass('current-stratum')
        }
      })

      // Prune the spaces, i.e. strata.
      // Close all sub and superstrata a couple of steps away.
      loader.closeNeighbors(currentStratum.path, 2)

      // Expand the parent. Do not try to expand the parent of the root stratum.
      if (currentStratum.path !== '/') {
        // We want to avoid any race conditions caused by
        // the parent being finished loading before the current stratum.
        // This becomes crucial especilly when the parent needs to add
        // a virtual facet node based on the current stratum model.
        if (currentStratum.loading || !currentStratum.model) {
          // Still loading. Postpone parent opening until loading is finished.
          currentStratum.once('loaded', () => {
            const eventData = { context: currentStratum.getSupercontext() }
            loader.openParent(currentStratum.path, eventData)
          })
        } else {
          // Current stratum is already finished loading.
          // We can open the parent immediately.
          const eventData = { context: currentStratum.getSupercontext() }
          loader.openParent(currentStratum.path, eventData)
        }
      }

      // Signal about the change of the current stratum.
      io.store.dispatch({
        type: 'navigation/zoom',
        path: currentStratum.path,
        context: currentStratum.context.copy(),
        previousPath: previousStratumPath
      })
    }

    // Close all too small strata that are not current.
    const viewportArea = sky.viewport.getInnerSquare().getArea().getRaw()
    Object.values(sky.strata).forEach((stratum) => {
      // Do not close current regardless its area.
      if (stratum === currentStratum) {
        return
      }
      // Do not try to close already closed spaces.
      if (!stratum.alive || !stratum.space.getViewport()) {
        return
      }
      // Compute stratum area
      const area = stratum.boundingCircle.getArea().transitRaw(sky.viewport)
      const areaRatio = area / viewportArea

      // Compare and remove if too small.
      if (areaRatio > 0 && areaRatio < 0.002) {
        loader.closeSpace(stratum.path)
      }
    })

    // Open automatically-openable substrata
    if (currentStratum.path === '/') {
      // Open all children of root stratum. Do not open further because too much to render.
      const subctxs = currentStratum.getEverySubcontext()
      subctxs.forEach(subctx => {
        const childPath = subctx.toUrlPath()
        loader.openChild(currentStratum.path, childPath, { context: subctx })
      })
    }
    // NOTE Sematic zoom open can left users to feel not in control of things that are happening.
    // } else {
    //   // Trigger possible viewport-dependent substratum requests
    //   // on the current stratum.
    //   currentStratum.triggerSubstratum()
    // }

    // Reveal and hide node labels at each idle.
    Object.values(loader.spaces).forEach((space) => {
      if (space.stratum && space.stratum.revealDetails) {
        space.stratum.revealDetails()
      }
    })

    // Prevent viewport from getting too far from root.
    if (currentStratum.path === '/') {
      currentStratum.limitZoom(0.1)
    }
  })
}
