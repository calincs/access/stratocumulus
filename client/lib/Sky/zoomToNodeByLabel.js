module.exports = function (context, label) {
  // @Sky:zoomToNodeByLabel(context, label)
  //
  // Jump viewport to certain stratum node if such node is available
  // and open the node.
  //
  // Parameters
  //   context
  //     a Context, defines the current stratum.
  //   label
  //     a string, the label of the node
  //

  // Find the node within current stratum.
  const node = this.findNodeByLabel(context, label)

  if (!node) {
    console.warn(`No matching node found for label ${label}`)
    return
  }

  // Open the substratum immediately, so that next driver run at viewport idle
  // finds the substratum to be the current one.
  // Note that not all nodes have the method.
  if (node.requestOpening) {
    node.requestOpening()
  }

  // Zoom to node component
  const duration = 2000
  const easing = 'ease-in' // try avoid quick movements but note exponential z
  this.viewport.animateOnce({ duration, easing })
  this.viewport.zoomToFill(node.component, 0.5)
}
