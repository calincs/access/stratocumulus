const contextHelper = require('../contextHelper')

module.exports = function (subcontext) {
  // @Sky:findOpenableNode(subcontext)
  //
  // Search for an openable node with the given subcontext.
  // In other words, find a node that opens to a stratum of the given context.
  //
  // Parameters
  //   context
  //     a Context, the subcontext of the node.
  //
  // Return
  //   a StratumNode or null if not found
  //

  // Find the superstratum.
  const supercontext = contextHelper.super(subcontext)
  // Root stratum has no parent node.
  if (!supercontext) {
    return null
  }
  // Superstratum exists.
  const path = supercontext.toUrlPath()

  if (!this.loader.hasSpace(path)) {
    // Superstratum does not exist.
    console.warn('Cannot find navigable stratum: ' + path)
    return null
  }

  const stratum = this.loader.getSpace(path).stratum
  if (!stratum.alive) {
    return null
  }

  // Search the stratum for the node.
  const node = stratum.getOpenableNode(subcontext)

  return node || null
}
