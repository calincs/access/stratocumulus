module.exports = function () {
  // @Sky:getCurrentStratum()
  //
  // Returns
  //   a Stratum.
  //   a null if no stratum exist.
  //
  if (this.currentStratumPath) {
    return this.strata[this.currentStratumPath] || null
  }
  return null
}
