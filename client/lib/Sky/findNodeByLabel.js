module.exports = function (context, label) {
  // @Sky:findNodeByLabel(context, label)
  //
  // Search for a facet node with the given label.
  // Prioritize nodes in the stratum of the given context.
  //
  // Parameters
  //   context
  //     a Context, defines the current stratum.
  //   label
  //     a string, the label of the desired node
  //
  // Return
  //   a StratumNode or null if not found
  //

  // Select and order strata from which to search for the node.
  const strata = []
  // Prioritize the current stratum.
  const path = context.toUrlPath()
  if (this.loader.hasSpace(path)) {
    const stratum = this.loader.getSpace(path).stratum
    if (stratum.alive) {
      strata.push(stratum)
    }
  } else {
    console.warn('Cannot find navigable stratum: ' + path)
  }
  // Then all the other alive strata
  this.loader.getSpaces().forEach(space => {
    const stratum = space.stratum
    if (stratum.alive && !strata.includes(stratum)) {
      strata.push(stratum)
    }
  })

  // Go strata through one by one until a matching node is found.
  for (let i = 0; i < strata.length; i += 1) {
    const stratum = strata[i]
    const node = stratum.getNodes().find(n => {
      return n.data.label === label
    })
    if (node) {
      // Found. Exit.
      return node
    }
  }

  return null
}
