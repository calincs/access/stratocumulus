const tapspace = require('tapspace')
const driver = require('./driver')
const generator = require('./generator')
const Context = require('../Context')
const io = require('../io')

const LOADING_SPACE = 1
const POPULATED_SPACE = 2

const Sky = function (viewport) {
  // @Sky(viewport)
  //
  // Sky manages stratum objects and their loading.
  //
  // Parameters:
  //   viewport
  //     a tapspace.components.Viewport
  //

  // Current known set of stratum objects.
  // stratumPath -> Stratum
  // TODO remove or clarify overlap with the loader.spaces.
  this.strata = {}

  this.viewport = viewport

  // Track last known current stratum.
  // Useful to detect when current stratum changes.
  // Useful to limit strata during filtering.
  this.currentStratumPath = null

  // Use tapspace TreeLoader for loading and unloading of fractal spaces.
  this.loader = new tapspace.loaders.TreeLoader({
    viewport: this.viewport,

    mapper: (parentPath, parentSpace, childPath) => {
      // Position the child relative to the parent.
      // In other words, find a basis for the child.
      // If there is no position for the child, null.
      const parentStratum = parentSpace.stratum
      const subcontext = Context.fromUrlPath(childPath)
      return parentStratum.getBasisForSubstratum(subcontext)
    },

    backmapper: (childPath, childSpace, parentPath) => {
      // Dummy backmapper. The loader automatically inverts the mapper
      // to find the parent basis, although that delays the rendering
      // because the parent needs to be fully loaded.
      return null
    },

    tracker: (parentPath, parentSpace) => {
      // Get IDs of the children of the parent component.
      const ctxs = parentSpace.stratum.getEverySubcontext()
      return ctxs.map(ctx => ctx.toUrlPath())
    },

    backtracker: (childPath, childSpace) => {
      // Find parent id.
      // If no parent and the child is the root node, return null.
      const superctx = childSpace.stratum.getSupercontext()
      if (superctx) {
        return superctx.toUrlPath()
      }
      return null
    }
  })

  // Setup the loader
  generator(this, this.loader)
  driver(this, this.loader)

  // Zoom to initial content.
  io.store.subscribe(io.store.select(state => state.maturity, (maturity) => {
    if (maturity === LOADING_SPACE) {
      // Fit to loading animation
      const bbox = this.viewport.hyperspace.getBoundingBox()
      this.viewport.zoomToFill(bbox, 0.2)
    }

    if (maturity === POPULATED_SPACE) {
      // Fit to first content.
      // Multiple strata can be loading concurrently, including neighbors.
      // We must ensure that we fit to our landing stratum and not
      // to its quicker-to-load neighbors. Otherwise user lands to
      // unexpected stratum.
      const currentStratum = this.getCurrentStratum()
      // Find target boundary
      let bbox = null
      if (currentStratum) {
        bbox = currentStratum.boundingCircle.getBoundingBox()
      } else {
        bbox = this.viewport.hyperspace.getBoundingBox()
      }

      this.viewport.zoomToFill(bbox, 0.9)
    }
  }))

  // Propagate context state changes to the components.
  io.store.subscribe(io.store.select(state => state.wantedNode, () => {
    const state = io.store.getState()
    const context = state.context
    // Navigate to current position.
    if (state.wantedNode) {
      const nodeLabel = state.wantedNode.label
      this.zoomToNodeByLabel(context, nodeLabel)
    }
  }))

  io.store.subscribe(io.store.select(state => state.context, (context) => {
    const state = io.store.getState()
    if (state.zoomToContext) {
      // Ensure we are currently looking at the correct stratum.
      this.zoomToStratum(context)
    }
  }))
}

module.exports = Sky
const proto = Sky.prototype

// Class methods
Sky.create = (viewport) => (new Sky(viewport))

// Methods
proto.findLocalStratum = require('./findLocalStratum')
proto.findOpenableNode = require('./findOpenableNode')
proto.findNodeByLabel = require('./findNodeByLabel')
proto.getCurrentStratum = require('./getCurrentStratum')
proto.zoomToStratum = require('./zoomToStratum')
proto.zoomToNodeByLabel = require('./zoomToNodeByLabel')
