const config = require('../config')
const STRATUM_SIZE = config.rendering.stratumSize // e.g. 2560

module.exports = function (context) {
  // @Sky:zoomToStratum(context)
  //
  // Jump viewport to certain stratum and load the path if needed.
  //
  // Parameters
  //   context
  //     a Context, defines the current stratum.
  //

  const path = context.toUrlPath()

  if (this.loader.hasSpace(path)) {
    // Space for the context exists already.
    // No need to reset the spaces.

    // Not there yet. Travel to it.
    const space = this.loader.getSpace(path)
    const bbox = space.stratum.boundingCircle.getBoundingBox()

    this.viewport.animateOnce({ duration: 1500 })
    this.viewport.zoomToFill(bbox, 0.9)

    return
  }

  // That stratum does not exist yet.
  // Maybe a node for it exists?
  const openableNode = this.findOpenableNode(context)
  if (openableNode) {
    this.viewport.animateOnce({ duration: 1500 })
    this.viewport.zoomToFill(openableNode.component, 0.9)
    openableNode.requestOpening()

    return
  }

  // Space does not exist. Close all spaces and then re-initialize the loader.
  this.loader.closeAll()
  // We need to create a basis for the new stratum that fits the viewport.
  const innerSquare = this.viewport.getInnerSquare()
  const defaultBasis = this.viewport.getBasisAt(innerSquare.at(0, 0))
  // Stratum is different size than viewport. Scale the basis so that the stratum fits.
  // Note that square has equal width and height, as well as stratum has equal inner width and height.
  const targetSize = innerSquare.getWidth().transitRaw(this.viewport)
  // If the stratum size is larger than the target size, then the scale factor should shrink e.g. be < 1
  const factor = targetSize / STRATUM_SIZE
  const fittedBasis = defaultBasis.scaleBy(factor) // basis origin at (0,0)
  // Re-init the space.
  const eventData = { context: context }
  const openingDepth = 0
  this.loader.initSpace(path, fittedBasis, openingDepth, eventData)
}
