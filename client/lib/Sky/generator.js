const Stratum = require('../Stratum')
const Context = require('../Context')
const io = require('../io')

module.exports = (sky, loader) => {
  // @Sky.generator(sky, loader)
  //
  // Generator for TreeLoader. Generator defines how the loaded spaces
  // are constructed and destructed.
  //
  // Parameters:
  //   sky
  //   loader
  //

  // Generator
  loader.on('open', (ev) => {
    // DEBUG
    // console.log('space opening:', ev)

    const path = ev.id
    const context = ev.data.context // filtering context for this stratum

    // DEBUG
    if (sky.strata[path]) {
      console.warn('Attempted to recreate existing stratum: ' + path, ev)
      return
    }

    // Create
    const stratum = new Stratum(context)

    // Maintain index of create strata.
    // TODO unnecessary overlap with loader spaces?
    sky.strata[path] = stratum

    // Add stratum to space via the loader.
    const spaceAdded = loader.addSpace(path, stratum.getSpace())
    if (!spaceAdded) {
      // Likely no mapping found yet in case of a superstratum.
      // This is ok. Postpone addition to final event.
      console.warn('Could not add space', path)
    }

    // Begin loading (and rendering at each incoming event)
    stratum.load()

    // If this stratum was opened by a superstratum,
    // ensure that the associated superstratum node looks opened.
    // Also start loading animation on the node.
    if (ev.parentId) {
      const superStratum = sky.strata[ev.parentId]
      if (superStratum) {
        const openableNode = superStratum.getOpenableNode(context)
        if (openableNode) {
          openableNode.setLoadingState('loading')
        }
      }
    }
    // If this stratum was openend by a substratum,
    // ensure that the associated node looks opened.
    if (ev.childId) {
      const subcontext = Context.fromUrlPath(ev.childId)
      const openableNode = stratum.getOpenableNode(subcontext)
      if (openableNode) {
        openableNode.setLoadingState('loading')
      }
    }

    // NOTE Here we would want to test for ev.depth > 0 if we wanted to open
    // NOTE multiple pages at once. However, it might be better to have
    // NOTE large pages and empty cards.

    // Begin listening strata and nodes.
    stratum.on('substratumrequest', (rev) => {
      // This event tells us that an interaction within the stratum
      // requested a substratum to be built and rendered.
      const childPath = rev.context.toUrlPath()
      // Pass to next open
      const eventData = { context: rev.context }
      loader.openChild(path, childPath, eventData)
    })

    // Backtracked parent can be rendered only after final.
    stratum.once('loaded', () => {
      // Add the stratum to space if not yet added.
      if (ev.childId) {
        const subcontext = Context.fromUrlPath(ev.childId)
        let openableNode = stratum.getOpenableNode(subcontext)

        if (!loader.hasSpace(path)) {
          // Stratum is not yet added to the viewport space.

          // Stratum might not always contain the child context.
          // This can happen if the stratum is too small to display facets
          // or if there were too many facets and some were left out.
          // In order to ensure user can always zoom out,
          // we need to detect if the child context is not present in the stratum.
          // If so then we need to forcefully insert a virtual facet node into the stratum.
          if (!openableNode) {
            const subspace = loader.getSpace(ev.childId)
            const substratum = (subspace ? subspace.stratum : null)
            openableNode = stratum.addVirtualNode(subcontext, substratum)
          }

          // Try to add the stratum to the viewport space.
          const spaceAdded = loader.addSpace(path, stratum.getSpace())
          if (!spaceAdded) {
            // Likely no mapping found in the stratum.
            console.warn('Could not add parent space at final.\n- parent:', path, '\n- child:', ev.childId)
            return
          }
        }

        // Ensure the child node looks opened.
        // Also stop loading animation, if any.
        if (openableNode) {
          openableNode.setLoadingState('loaded')
        }
      }

      // Ensure the node in the parent stratum looks opened.
      // Also stop loading animation, if any.
      if (ev.parentId) {
        const superStratum = sky.strata[ev.parentId]
        if (superStratum) {
          const openableNode = superStratum.getOpenableNode(context)
          if (openableNode) {
            openableNode.setLoadingState('loaded')
          }
        }
      }
    })

    // When stratum layout changes, reposition the substrata
    stratum.on('layout', (ev) => {
      loader.remapChildren(path)
    })
  })

  // The first stratum and first content should
  // enable the viewport interaction. // TODO smells
  loader.once('opened', (ev) => {
    const stratum = ev.space.stratum
    // Mark as current because it is the first.
    ev.space.addClass('current-stratum')
    // Wait for stratum to load.
    stratum.once('loaded', () => {
      // Wait to ensure stratum is in DOM.
      setTimeout(() => {
        io.store.dispatch({
          type: 'stratum/first'
        })
      }, 0)
    })
  })

  loader.on('close', (ev) => {
    // console.log('space closing:', ev) // DEBUG

    // Make the associated node look closed.
    // Note that due to asynchronity, this stratum may be already removed.
    const path = ev.id
    let closingStratum = ev.space.stratum
    const superContext = closingStratum.getSupercontext()
    if (superContext) {
      const superPath = superContext.toUrlPath()
      const superStratum = sky.strata[superPath]
      if (superStratum) {
        const openableNode = superStratum.getOpenableNode(closingStratum.context)
        if (openableNode) {
          openableNode.setLoadingState('closing')
        }
      }
    }

    // Ensure the closing stratum is removed.
    // Read from sky.strata instead of event to make sure.
    closingStratum = sky.strata[path]
    if (closingStratum) {
      // Remove from DOM and stop listeners.
      closingStratum.remove()
      // Forget
      delete sky.strata[path]
    }

    // Finally, close the space.
    loader.removeSpace(path)
  })
}
