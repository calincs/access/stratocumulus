require('./style.css')
const io = require('../io')

const ExportTools = function () {
  // @ExportTools()
  //
  // Actions for the current set of entities.
  //

  // Prepare container
  this.element = document.createElement('div')
  this.element.className = 'export-tools'

  // Setup share button interaction.
  // Bind once. Delegate via root element.
  this.element.addEventListener('click', (ev) => {
    if (ev.target.classList.contains('share-button')) {
      ev.preventDefault()

      const messageEl = this.element.querySelector('.sharing .sharing-message')

      // Shareable URL for current path
      const currentUrl = window.location.href
      // Copy to clipboard
      window.navigator.clipboard.writeText(currentUrl)
      // Make message visible
      messageEl.style.display = 'block'
      // Make message disappear after a while
      // TODO animate
      setTimeout(() => {
        messageEl.style.display = 'none'
      }, 5000)
    }
  })

  // Wait for the first content before the first render.
  const POPULATED_SPACE = 2
  io.store.subscribe(io.store.select(state => state.maturity, (maturity) => {
    if (maturity >= POPULATED_SPACE) {
      this.render()
    }
  }))

  io.store.subscribe(io.store.select(state => state.context, () => {
    this.render()
  }))
}

module.exports = ExportTools
const proto = ExportTools.prototype
proto.isExportTools = true

proto.getElement = require('./getElement')
proto.render = require('./render')
