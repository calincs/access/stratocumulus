const template = require('./template.ejs')
const io = require('../io')
const BASE_URL = 'https://rs.lincsproject.ca/resource/:ThinkingFrames?uri='

module.exports = function () {
  // @ExportTools:render()
  //
  // Render the current context.
  //

  const context = io.store.getState().context

  // Stratum provides human-readable labels.
  io.stratumApi.fetchStratum(context, (err, stratum) => {
    if (err) {
      // Just skip.
      return
    }

    // Generate a LINCS' ResearchSpace URL
    // https://rs.lincsproject.ca/resource/:ThinkingFrames?uri=http://id.lincsproject.ca/0VXBuviAxIb
    let researchSpaceUrl = null
    if (stratum && stratum.context) {
      const entityContext = stratum.context.toReversed().find(c => c.details)
      if (entityContext) {
        const details = entityContext.details
        if (details && details.iri) {
          const iri = details.iri
          researchSpaceUrl = BASE_URL + iri
        }
      }
    }

    // Render
    this.element.innerHTML = template({
      researchSpaceUrl
    })
  })
}
