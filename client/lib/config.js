// Zoom navigation adjustments.
exports.navigation = {
  tapZoomStep: 0.5
}

// Rendering size.
// Large size consumes more browser memory.
// Small size might look more pixelated upon zooming.
exports.rendering = {
  stratumSize: 2560, // px
  stratumNodeSize: 256 // px
}

// Node sizing range.
exports.sizing = {
  maxNodeSize: 100, // graphology CirclePack units
  minNodeSize: 10 // graphology CirclePack units
}

// Entity pages
exports.entities = {
  pageSize: 50
}

// Build a map for easy access to type configs.
exports.contentTypes = window.stratocumulus.contentTypes
exports.contentTypeMap = exports.contentTypes.reduce((acc, c) => {
  acc[c.content_type] = c
  return acc
}, {})
window.stratocumulus.contentTypeMap = exports.contentTypeMap
// TODO Smells. Access these only through config. Access window.stratocumulus only here.

// TEMP TODO populate facetParameters and filterParameters
exports.facetParameters = exports.contentTypes.reduce((acc, typeConfig) => {
  // Pick parameter strings and make unique.
  const params = typeConfig.facets
    .map(facet => facet.parameter)
    .filter(param => !acc.includes(param))
  return acc.concat(params)
}, ['content_type', 'entity'])

exports.filterParameters = exports.contentTypes.reduce((acc, typeConfig) => {
  // Pick parameter strings and make unique.
  const params = typeConfig.filters
    .map(fil => fil.parameter)
    .filter(param => !acc.includes(param))
  return acc.concat(params)
}, [])

window.stratocumulus.facetParameters = exports.facetParameters
window.stratocumulus.filterParameters = exports.filterParameters
window.stratocumulus.queryParameters = exports.facetParameters.concat(exports.filterParameters)

// Required server-provided configuration
const required = [
  { key: 'categoryThreshold', type: 'number' },
  { key: 'facetParameters', type: 'array' },
  { key: 'filterParameters', type: 'array' },
  { key: 'contentTypes', type: 'array' }
]

exports.validate = () => {
  // @config.validate()
  //
  // Validate the server-provided configuration.
  //
  if (!window || !window.stratocumulus) {
    throw new Error('Missing window.stratocumulus configuration object.')
  }
  const conf = window.stratocumulus

  for (let i = 0; i < required.length; i += 1) {
    const schemaItem = required[i]
    const key = schemaItem.key
    const type = schemaItem.type

    if (!conf[key]) {
      throw new Error('Missing configuration: window.stratocumulus.' + key)
    }

    if (type === 'array') {
      if (!Array.isArray(conf[key])) {
        throw new Error('Invalid configuration: window.stratocumulus.' + key)
      }
    } else {
      // eslint-disable-next-line valid-typeof
      if (typeof conf[key] !== type) {
        throw new Error('Invalid configuration: window.stratocumulus.' + key)
      }
    }
  }

  // No errors, everything ok
}
