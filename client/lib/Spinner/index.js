require('./style.css')
const tapspace = require('tapspace')
const turn = 2 * Math.PI

const Spinner = function (options) {
  // @Spinner([options])
  //
  // N nodes rotating.
  // Spinner provides a transparent and easy to configure loading animation
  // that is resistant to zooming without pixel resolution issues.
  //
  // Parameters:
  //   options
  //     optional object with properties
  //       diameter
  //         a number, the spinner diameter in pixels.
  //         .. Half of the value equals the rotation radius.
  //       circles
  //         a positive integer, the number of rotating circles.
  //       circleDiameter
  //         a number, the diameter of rotating circles.
  //

  if (!options) {
    options = {}
  }

  const d = options.diameter || 50
  const n = options.circles || 5
  const c = options.circleDiameter || 10
  const r = d / 2

  const plane = tapspace.createSpace()

  const carousel = tapspace.createItem()
  carousel.setSize(d + c + c, d + c + c)
  carousel.setAnchor(carousel.atCenter())
  carousel.addClass('spinner')
  plane.addChild(carousel)
  plane.setAnchor(carousel.atCenter())

  const origin = carousel.atCenter()

  const circles = []
  for (let i = 0; i < n; i += 1) {
    const circle = tapspace.createNode(c)
    circle.addClass('spinner-circle')
    circle.setAnchor(circle.atCenter())
    circles.push(circle)

    carousel.addChild(circle)
    circle.translateTo(origin.polarOffset(r, i * turn / n))
  }

  this.carousel = carousel
  this.component = plane
}

module.exports = Spinner
const proto = Spinner.prototype
proto.isSpinner = true

proto.start = require('./start')
proto.stop = require('./stop')
