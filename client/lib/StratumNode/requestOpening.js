module.exports = function () {
  // @StratumNode:requestOpening()
  //
  // Ask to open the node. This makes the node emit 'openingrequest' via DOM.
  //

  if (this.isOpenable && !this.isOpened) {
    // Send event to be handled in Stratum
    const openingRequest = new window.CustomEvent('openingrequest', {
      bubbles: true,
      detail: this.key
    })
    this.component.element.dispatchEvent(openingRequest)
    // The node will be made look open and loading via Sky generator.
  }
}
