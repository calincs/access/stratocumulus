const scaleDuration = (scale, baseDuration) => {
  // Scale is > 0
  if (scale <= 0) {
    console.warn('Bad scale factor: ' + scale)
    return baseDuration
  }

  // Scale factor of 1 is the default.
  //     scale = 2^x
  // <=> x = log2(scale)
  const power = Math.log(Math.sqrt(scale)) / Math.log(4)
  const distance = Math.abs(power)

  return baseDuration * distance
}

const translationDuration = (distance, baseDuration) => {
  // Distance in terms of multiplier of viewport size
  return distance * baseDuration
}

module.exports = (node) => (ev) => {
  // @StratumNode.onTap(node)
  //
  // Generate a node tap event handler for the given node.
  //
  // Returns:
  //   a function that takes in a tap event.
  //

  // double tap prevention
  if (node.tapWait) return
  node.tapWait = true
  setTimeout(() => {
    node.tapWait = false
  }, 1000)
  // TODO prevent viewport interaction during animation.
  // TODO This requires tapspace capturers to implement pause and resume.

  if (node.isWarpNode) {
    // When warping to another context, do no zooming.
    // Let Sky's context listener do the zooming.
    node.requestWarping()
    // TODO maybe zoom out a bit for the effect.
    // TODO zoom into warp node if it is far away.
    return
  }
  // else, zoom into the node and request opening.

  const viewport = node.component.getViewport()
  const viewportSquare = viewport.getInnerSquare()
  const viewportSize = viewportSquare.getHeight().getRaw()
  const targetSize = node.component.getHeight().transitRaw(viewport)
  const panDistance = viewportSquare.atCenter().getDistanceTo(node.component.atCenter())
  const panDistPx = panDistance.transitRaw(viewport)

  let ratio
  if (node.isGroupingNode) {
    // Zoom towards full group.
    ratio = 3 * targetSize / viewportSize
  } else {
    // node.isFacetNode
    ratio = targetSize / viewportSize
  }

  const panDistScaled = panDistPx / Math.sqrt(ratio)
  const normPanDist = panDistScaled / viewportSize

  // Scale multiplier
  const m = ratio

  // Adjust zoom duration based on distance
  const zDur = scaleDuration(m, 800)
  const tDur = translationDuration(normPanDist, 800)
  const duration = Math.max(zDur + tDur, 500)

  // Alleviate asymmetry in how the scale direction affects rendered transition speed.
  let easing = 'ease-in'
  if (m > 1) { easing = 'ease-out' }

  // DEBUG
  // console.log('tap zoom duration', zDur, 'ms +', tDur, 'ms from sizes', targetSize, 'per', viewportSize)
  // console.log('zoom ease', easing)

  viewport.animateOnce({ duration, easing })
  viewport.translateTo(node.component.atCenter())
  viewport.scaleBy(m)

  // Try to open and load substratum via Sky generator.
  node.requestOpening()
}
