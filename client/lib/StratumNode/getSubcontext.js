module.exports = function () {
  // @StratumNode:getSubcontext()
  //
  // Get the faceting context of the node if any.
  // With the subcontext the node is able to open a stratum.
  //
  // Return
  //   a Context or null if no subcontext
  //
  // See also
  //   Stratum:getSupercontext which mirrors the logic.
  //
  if (this.subcontext) {
    return this.subcontext
  }

  return null
}
