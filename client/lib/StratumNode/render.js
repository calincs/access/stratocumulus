const template = require('./template')

module.exports = function () {
  // @StratumNode:render()
  //
  // Refresh the contents.
  //

  // Update visitation status.
  if (this.isVisited()) {
    this.component.addClass('visited-node')
  }
  // Just render again and replace
  this.component.html(template(this))
  // Update reference to label
  this.label = this.component.element.querySelector('.node-label')
  // Set font size because we will adjust it dynamically on hover.
  this.adjustLabelSize()
}
