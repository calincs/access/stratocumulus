module.exports = function () {
  // @StratumNode:remove()
  //
  // Remove the rendered node. Useful when the graph has been filtered.
  //

  // Remove component. This removes the component from the space and
  // removes its event listeners.
  this.component.remove()
}
