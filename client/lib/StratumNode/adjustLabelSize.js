module.exports = function () {
  // @StratumNode:adjustLabelSize()
  //
  // Refresh the node label font size dynamically
  // based on its distance to viewer.
  //

  // Dynamic font size based on label text length.
  // Apply magic to adjust font size for the titles to occupy roughly same area.
  // Find the numbers manually by fitting a power law to sample pairs.
  // Should be about 60px for a first+family name
  const labelLength = this.data.label.length
  const baseFontSize = Math.round(160 * Math.pow(labelLength, -0.4))

  if (this.isSelectable && this.isSelected && !this.isOpened) {
    const viewport = this.component.getViewport()
    const dilation = viewport.measureDilation(this.component)
    const fontSize = baseFontSize / Math.sqrt(dilation)
    this.label.style.fontSize = fontSize.toFixed(2) + 'px'
    const width = 256 / Math.sqrt(dilation)
    this.label.style.width = width.toFixed(0) + 'px'
  } else {
    this.label.style.fontSize = baseFontSize + 'px'
    this.label.style.width = '256px'
  }
}
