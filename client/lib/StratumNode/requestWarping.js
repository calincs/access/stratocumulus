const io = require('../io')

module.exports = function () {
  // @StratumNode:requestWarping()
  //
  // Ask to warp to the node's context.
  //

  if (this.isWarpNode) {
    io.store.dispatch({
      type: 'navigation/link',
      path: this.subcontext.toUrlPath()
    })
  }
}
