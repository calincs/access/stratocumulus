module.exports = function (stage) {
  // @StratumNode:setLoadingState(stage)
  //
  // Use this method to visually open a node when its
  // substratum is loading or loaded.
  //
  // Parameters:
  //   stage
  //     an enum string: 'loading', 'loaded', 'closing', or 'closed'
  //

  if (stage === 'loading') {
    // Ensure node is open and animation running.
    this.isOpened = true
    this.component.addClass('opened-node')
    this.component.addClass('visited-node')
    this.component.addClass('loading')
    return
  }

  if (stage === 'loaded') {
    // Ensure node is open and animation stopped.
    this.isOpened = true
    this.component.addClass('opened-node')
    this.component.addClass('visited-node')
    this.component.removeClass('loading')
    return
  }

  if (stage === 'closing' || stage === 'closed') {
    // Ensure node is closed and animation stopped.
    this.isOpened = false
    this.component.removeClass('opened-node')
    this.component.removeClass('loading')
    // return
  }
}
