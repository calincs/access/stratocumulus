const io = require('../io')

module.exports = function () {
  // @StratumNode:isVisited()
  //
  // Check if the node is visited by the user in this session.
  //

  if (!this.subcontext) {
    return false
  }

  const subpath = this.subcontext.toUrlPath()
  const state = io.store.getState()
  if (state.contextHistory[subpath]) {
    return true
  }

  return false
}
