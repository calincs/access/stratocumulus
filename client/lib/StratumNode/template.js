const buildNodeElement = (attrs) => {
  return '<div class="node-shape"></div>'
}

const buildLabelElement = (attrs) => {
  let labelPrefix = ''
  if (attrs.labelPrefix) {
    labelPrefix = `<div class="node-label-prefix">${attrs.labelPrefix}</div>`
  }

  const labelName = `<div class="node-label-name">${attrs.label}</div>`

  let labelPostfix = ''
  if (attrs.labelPostfix) {
    labelPostfix = `<div class="node-label-postfix">${attrs.labelPostfix}</div>`
  }

  let labelCount = ''
  if (attrs.role === 'facet') {
    const localCount = attrs.count.toLocaleString('en-US')
    labelCount = `<div class="node-label-count">${localCount}</div>`
  }

  return `<div class="node-label">${labelPrefix + labelName + labelPostfix + labelCount}</div>`
}

const buildHintElement = (msg) => {
  return `<div class="node-hint">${msg}</div>`
}

module.exports = (node) => {
  // @StratumNode.template(node)
  //
  // Stratum node HTML template.
  //
  // Return
  //   a string, the html for the node.
  //

  if (node.isGroupingNode) {
    // Grouping nodes lack shape
    const labelHtml = buildLabelElement(node.data)
    return labelHtml
  }

  if (node.isFacetNode || node.isEntityNode || node.isWarpNode) {
    const nodeHtml = buildNodeElement(node.data)
    const labelHtml = buildLabelElement(node.data)

    let hintMsg = 'Tap to open'
    if (node.isWarpNode) {
      hintMsg = 'Tap to jump'
    }
    const hintHtml = buildHintElement(hintMsg)
    const contentHtml = `${nodeHtml}\n${labelHtml}\n${hintHtml}`
    return contentHtml
  }

  console.warn('Bad node template.')
  return ''
}
