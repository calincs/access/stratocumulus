require('./style.css')
const tapspace = require('tapspace')
const config = require('../config')
const onTap = require('./onTap')
const contextHelper = require('../contextHelper')

const NODE_RENDER_SIZE = config.rendering.stratumNodeSize

const StratumNode = function (context, key, data) {
  // @StratumNode(context, key, data)
  //
  // Interactive node in the stratum graph.
  //
  // Parameters:
  //   context
  //     a Context, the faceting context
  //   key
  //     a string, graph node key, e.g. "/arc/genres" or "/arc/genres/1234"
  //   data
  //     an object, the node attributes from the graph.
  //
  // Emits:
  //   openingrequest `{ nodeKey: <string>, item: <Component> }`
  //     when the user interacts with the node in order to open something.
  //

  // Common properties
  this.context = context
  this.key = key
  this.data = data || null
  this.component = null

  // Node typing
  this.isGroupingNode = this.data.role === 'grouping'
  this.isFacetNode = this.data.role === 'facet'
  this.isEntityNode = this.data.role === 'entity'
  this.isWarpNode = this.data.role === 'warp'

  // Context action parameters
  this.facetParam = this.data.facetParam || null
  this.facetValue = null
  // Ensure facet value is string
  if (typeof this.data.facetValue === 'number') {
    this.facetValue = '' + this.data.facetValue
  } else if (typeof this.data.facetValue === 'string') {
    this.facetValue = this.data.facetValue
  } else if (this.data.facetValue) {
    console.warn('Bad facet value: ' + this.data.facetValue)
  }

  // Node subcontext
  this.isRepeatedNode = false
  this.subcontext = null
  if (this.facetParam && this.facetValue) {
    if (this.isWarpNode) {
      // Cut context, warp far.
      this.subcontext = contextHelper.warp(this.context, this.facetParam, this.facetValue)
    } else if (context.hasValue(this.facetParam, this.facetValue)) {
      // Node is already repeated. Give subcontext.
      this.isRepeatedNode = true
    } else {
      // Normal faceting context
      this.subcontext = contextHelper.sub(this.context, this.facetParam, this.facetValue)
    }
  }

  this.isOpenable = (this.isFacetNode || this.isEntityNode) && this.subcontext && !this.isRepeatedNode
  this.isOpened = false
  this.isSelectable = this.isOpenable || this.isWarpNode
  this.isSelected = false

  // Constant rendering size. Use scaling to "size" nodes.
  const radiusPx = NODE_RENDER_SIZE / 2
  const newItem = tapspace.createNode(radiusPx)
  newItem.addClass('category-node')
  newItem.addClass('stratum-node')
  // Make it easy to find node attributes via tapspace component.
  newItem.nodeKey = this.key

  if (this.isGroupingNode) {
    // Style grouping nodes.
    newItem.addClass('grouping-node')
  }
  if (this.isFacetNode) {
    // Style facetable nodes.
    newItem.addClass('facet-node')
  }
  if (this.isEntityNode) {
    newItem.addClass('entity-node')
  }
  if (this.isRepeatedNode) {
    newItem.addClass('repeated-node')
  }
  if (this.isWarpNode) {
    newItem.addClass('warp-node')
  }
  if (this.isOpenable) {
    newItem.addClass('openable-node')
  }
  if (this.isSelectable) {
    newItem.addClass('selectable-node')
  }

  // Build faceting field specific class names
  if (this.data.field) {
    newItem.addClass('node-' + this.data.field)
  }
  // Build data attributes
  for (const attr in this.data) {
    // TODO Skip non-serializable and large attributes
    newItem.element.dataset[attr] = this.data[attr]
  }

  // Gravity at node center
  newItem.setAnchor(newItem.atCenter())
  // Disable interaction with node content.
  newItem.setContentInput(false)

  // Setup interaction
  newItem.tappable({ preventDefault: false }) // prvnt default to allow pan
  newItem.on('tap', onTap(this))

  // Selection / hovering state for openable nodes.
  if (this.isSelectable) {
    let hoverTimeout = null // HACK to avoid timeout run after mouseout
    newItem.element.addEventListener('mouseover', () => {
      if (!this.isOpened) {
        this.isSelected = true
      }
      // Make label show on top of other nodes on hover
      this.component.bringToFront()
      // HACK see also Stratum:revealDetails
      this.component.addClass('readable')
      // HACK Use .hover cuz .bringToFront causes Firefox to forget :hover state
      // HACK Use setTimeout for hover animations to start after .bringToFront.
      hoverTimeout = setTimeout(() => {
        this.component.addClass('hover')
        this.adjustLabelSize()
      }, 1)
    })
    newItem.element.addEventListener('mouseout', () => {
      this.isSelected = false
      // HACK Use .hover cuz .bringToFront causes Firefox to forget :hover state
      if (hoverTimeout) {
        clearTimeout(hoverTimeout)
      }
      this.component.removeClass('hover')
      // Revert label sizes.
      this.adjustLabelSize()
    })
  }

  // Replace default component
  this.component = newItem
  // Quick reference to label HTMLElement. Populated in render.
  this.label = null

  this.render()
}

module.exports = StratumNode
const proto = StratumNode.prototype
proto.isStratumNode = true

// Methods
proto.adjustLabelSize = require('./adjustLabelSize')
proto.getOrigin = require('./getOrigin')
proto.getRadius = require('./getRadius')
proto.getSubcontext = require('./getSubcontext')
proto.isVisited = require('./isVisited')
proto.remove = require('./remove')
proto.render = require('./render')
proto.requestOpening = require('./requestOpening')
proto.requestWarping = require('./requestWarping')
proto.setLoadingState = require('./setLoadingState')
proto.setScale = require('./setScale')
proto.translateTo = require('./translateTo')
proto.update = require('./update')
