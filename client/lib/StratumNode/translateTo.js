module.exports = function (point, animate) {
  // @StratumNode:translateTo(point[, animate])
  //
  // Move the node to a position.
  // Useful when the graph model layout has changed.
  //
  // Parameters:
  //   point
  //     a tapspace.geometry.Point
  //   animate
  //     optional boolean. False by default = no animation.
  //
  if (animate) {
    this.component.animateOnce({ duration: 500 })
  }
  this.component.translateTo(point)
}
