require('./style.css')
const io = require('../io')

const SidebarHandle = function () {
  // @SidebarHandle()
  //
  // A component for search tools and list view.
  //
  this.element = document.createElement('button')
  this.element.className = 'sidebarhandle'

  const updateHandle = () => {
    const state = io.store.getState()
    let isOpen = false
    if (state.ui && state.ui.sidebarOpen) {
      isOpen = true
    }

    if (isOpen) {
      this.element.classList.add('sidebarhandle-open')
      this.element.innerHTML = '⟨'
    } else {
      this.element.classList.remove('sidebarhandle-open')
      this.element.innerHTML = '⟩'
    }
  }

  // Init sidebar state
  updateHandle()
  // Track sidebar state
  io.store.subscribe(io.store.select(state => state.ui.sidebarOpen, updateHandle))

  this.element.addEventListener('click', (ev) => {
    const state = io.store.getState()
    let isOpen = false
    if (state.ui && state.ui.sidebarOpen) {
      isOpen = true
    }

    io.store.dispatch({
      type: 'sidebar/toggle',
      isOpen: !isOpen
    })
  })
}

module.exports = SidebarHandle
const proto = SidebarHandle.prototype

// Methods
proto.getElement = require('./getElement')
