module.exports = function () {
  // @ViewportManager:enableNavigation()
  //
  // Enable viewport navigation: cursor panning, wheel zooming, zoom buttons,
  // and others.
  //
  // It is recommended to call this only after the space has
  // some visible content so that user does not get lost into empty space.
  //

  // Prevent double enabling
  if (this.enabled) {
    return
  }
  this.enabled = true

  // Enable viewport pan and zoom via touch, scroll, and keyboard
  this.viewport.pannable().zoomable()

  // Allow viewport to receive focus to emit keyboard events.
  this.viewport.focusable()

  // Viewport tapping
  this.viewport.tappable()
  this.viewport.on('tap', (ev) => {
    // Give viewport focus after click. When in focus, can emit keyboard events.
    this.viewport.focus({
      focusVisible: false
    })

    // Background tap zooms to all the content.
    if (ev.target === this.viewport) {
      const hyperspace = this.viewport.hyperspace
      // TODO OPTIMIZE compute hyperspace boundary using static stratum sizes
      // TODO OPTIMIZE instead of real boundaries.
      const box = hyperspace.getBoundingBox(this.viewport.getOrientation())
      this.viewport.animateOnce({ duration: 1000 })
      this.viewport.zoomToFill(box, 0.8)
    }
  })

  // Enable zoom control. Position the zoom control to bottom right corner.
  this.viewport.addControl(this.zoomControl, this.viewport.atNorm(1, 1).offset(-12, -13))
}
