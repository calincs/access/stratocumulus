require('./style.css')
const tapspace = require('tapspace')
const io = require('../io')

const POPULATED_SPACE = 2

const ViewportManager = function () {
  // @ViewportManager()
  //
  // The viewport manager handles the tapspace.components.Viewport setup.
  // The manager can be used to enable or disable interaction.
  //
  // For example, we would like to avoid user to interact with
  // the viewport before there is something to show.
  // Otherwise a few accidental moves could pan the viewport somewhere
  // where there will be no content.
  //

  this.enabled = false

  const skyElement = document.querySelector('#sky')
  this.viewport = tapspace.createView(skyElement)

  // Basic zoom control
  this.zoomControl = new tapspace.components.ZoomControl({ scaleStep: 1.5 })
  this.zoomControl.setAnchor(this.zoomControl.atNorm(1, 1))
  // HACK TODO tapspace should not set hardcoded bg color, hard to override
  this.zoomControl.element.style.backgroundColor = 'transparent'

  // Allow navigation only after the space has some content.
  io.store.subscribe(io.store.select(state => state.maturity, (maturity) => {
    if (maturity === POPULATED_SPACE) {
      this.enableNavigation()
    }
  }))
}

module.exports = ViewportManager
const proto = ViewportManager.prototype
proto.isViewportManager = true

// Methods
proto.enableNavigation = require('./enableNavigation')
proto.getViewport = require('./getViewport')
