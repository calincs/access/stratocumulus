// @io
//
// Stratocumulus client-side IO.
//
// Gather here all code that handles sending and receiving data from
// servers and databases. This helps a lot when migrating to new or
// updated backend APIs.
//

const ReduxStore = require('../ReduxStore')
const Context = require('../Context')
const reducer = require('./reducer')

exports.entityApi = require('./entityApi')
exports.stratumApi = require('./stratumApi')

// Default state for the redux store.
// This will be partly overwritten by localStorage hydration.
const defaultState = {
  context: new Context(),
  contextHistory: {},
  maturity: 0,
  ui: {
    sidebarOpen: true,
    listOrder: {
      property: 'count',
      direction: 'desc'
    }
  },
  wantedNode: null
}
exports.store = new ReduxStore(defaultState, reducer)

// Setup redux state hibernation.
const STORAGE_KEY = 'stratocumulus-state'
// Select the portion of the state to hibernate/hydrate.
// The purpose is to clean up any deprecated and conflicting state.
// Try to ensure that nullish state properties are dropped.
const sanitizeState = (state) => {
  const cleaned = {}
  if (state.ui) {
    cleaned.ui = state.ui
  }
  if (state.contextHistory) {
    cleaned.contextHistory = state.contextHistory
  }
  return cleaned
}

// Init state from storage.
exports.store.hydrate(STORAGE_KEY, sanitizeState)

// Automatically hibernate the state before tab switch or page exit.
document.addEventListener('visibilitychange', () => {
  if (document.visibilityState === 'hidden') {
    exports.store.hibernate(STORAGE_KEY, sanitizeState)
  }
})
