// Cache recent strata.
const Cache = require('./Cache')
const stratumCache = new Cache(20)

// Serve multiple callers with one response.
// This prevents duplicate fetch to the same resource.
const callers = {}

const callbackMany = (url, err, payload) => {
  if (!callers[url]) {
    return
  }

  // Take all calls for this URL.
  const calls = callers[url]
  // Detach them immediately. This adds protection for the rare case
  // where the callback calls fetchStratum again with the same URL
  // and thus would mess up the array if reachable.
  delete callers[url]

  // Call each waiting. Prevent their errors being propagated back.
  // Also, prevent error in one call blocking remaining calls.
  while (calls.length) {
    const cb = calls.pop()
    try {
      cb(err, payload)
    } catch (err) {
      console.error(err)
    }
  }
}

exports.fetchStratum = (context, callback) => {
  // @io.stratumApi.fetchStratum(context, callback)
  //
  // Get stratum for the give filtering context
  //
  // Parameters:
  //   context
  //     a Context, the filtering context
  //   callback
  //     a function (err, stratum)
  //

  // Validate
  if (!context || !context.isContext) {
    throw new Error('Cannot fetch stratum. Invalid context argument: ' + context)
  }
  if (!callback || typeof callback !== 'function') {
    throw new Error('Cannot fetch stratum. Invalid callback argument: ' + callback)
  }

  const url = '/api/stratum' + context.toUrlPath()

  // Already loading?
  if (callers[url]) {
    callers[url].push(callback)
    // Exit and wait for resolution.
    return
  }

  // Init the callback pool
  callers[url] = [callback]

  // Check cache
  const cached = stratumCache.read(url)
  if (cached) {
    setTimeout(() => {
      callbackMany(url, null, cached)
    }, 0)
    return
  }

  window.fetch(url)
    .then(response => {
      if (!response.ok) {
        const msg = 'Failed to fetch the graph. HTTP status: ' + response.status
        const err = new Error(msg)
        return callbackMany(url, err)
      }

      return response.json()
    })
    .then(stratum => {
      // Store to cache
      stratumCache.write(url, stratum)
      // Return.
      callbackMany(url, null, stratum)
    })
    .catch(err => {
      console.warn('Fetching error.')
      callbackMany(url, err)
    })
}
