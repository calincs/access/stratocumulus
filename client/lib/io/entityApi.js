// Use native fetch API
const fetch = window.fetch
// How many entities per fetch
const PAGE_SIZE = require('../config').entities.pageSize
const contentTypeMap = window.stratocumulus.contentTypeMap

// Cache recent entities.
const Cache = require('./Cache')
const entityCache = new Cache(200)

const getSupportedContext = (context) => {
  const entityType = context.getValue('content_type')
  if (!entityType) { return context }
  const typeConfig = contentTypeMap[entityType]
  if (!typeConfig) { return context }
  const supportedFacets = typeConfig.facets.map(item => item.parameter)
  const supportedFilters = typeConfig.filters.map(item => item.parameter)
  return context.filter((key, value) => {
    return (
      supportedFacets.includes(key) ||
      supportedFilters.includes(key)
    )
  })
}

exports.fetchEntity = (entityType, id, callback) => {
  // @io.entityApi.fetchEntity(entityType, id, callback)
  //
  // Fetch single entity from the Entity API.
  //
  // Parameters:
  //   entityType
  //     a string, the entity index name.
  //   id
  //     a string, the entity ID.
  //   callback
  //     a function (err, entity)
  //
  const url = `/api/entity/${entityType}/${id}/`

  // Check cache
  const cachedEntity = entityCache.read(url)
  if (cachedEntity) {
    setTimeout(() => {
      callback(null, cachedEntity)
    }, 0)
    return
  }

  // Fresh from the server
  fetch(url)
    .then(response => response.json())
    .then(entity => {
      // Patch url for debug and viewing
      entity.accessUrl = url
      // Store to cache
      entityCache.write(url, entity)
      // Return
      callback(null, entity)
    })
    .catch(err => callback(err))
}

exports.fetchEntityPage = (context, callback) => {
  // @io.entityApi.fetchEntityPage(context, callback)
  //
  // Fetch single page of entities.
  //
  // Parameters:
  //   context
  //     a Context with 'context_type' and 'page' parameters set.
  //   callback
  //     a function (err, page) where
  //       page
  //         an object { pageNumber, entityIds }
  //
  if (!context.hasParameter('content_type')) {
    console.warn('Detected an attempt to fetch page without content type')
    return callback(null, { pageNumber: 1, entityIds: [] })
  }

  const entityType = context.getValue('content_type')
  const pageBaseUrl = `/api/entity/${entityType}/`
  const pageNumber = context.getIntegerValue('page', 1)// First page is #1

  // Ensure the context contains only parameters supported by the content type.
  const supportedContext = getSupportedContext(context)
  // Extend context with pages
  const pageSearchParams = supportedContext.toContextObject()
  pageSearchParams.page = pageNumber.toString(10)
  pageSearchParams.only = 'id'
  pageSearchParams['page-size'] = PAGE_SIZE

  const pageGetParams = new URLSearchParams(pageSearchParams)
  const queryString = pageGetParams.toString()
  const pageUrl = pageBaseUrl + '?' + queryString

  console.log(`Outgoing request: entities from ${queryString}`)

  fetch(pageUrl)
    .then(response => response.json())
    .then(data => {
      console.log(`Incoming response: entities for ${queryString}`)
      const entityIds = data.map(record => record.id)
      callback(null, {
        pageNumber: pageNumber,
        entityIds: entityIds
      })
    })
    .catch(err => callback(err))
}
