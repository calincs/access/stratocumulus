module.exports = function (key) {
  // @io.Cache:read(key)
  //
  // Read a cached value. Null if not found.
  //
  // Parameters:
  //   key
  //     a string
  //
  // Return
  //   a value or null
  //
  const payload = this.storage[key]

  if (payload) {
    // Refresh the key position in the queue so that it does not get discarded.
    const i = this.keyQueue.indexOf(key)
    // If already last, no need to modify.
    if (i < this.keyQueue.length - 1) {
      // Remove and reinsert. The length stays the same.
      if (i >= 0) {
        this.keyQueue.splice(i, 1)
      }
      this.keyQueue.push(key)
    }

    return payload
  }

  return null
}
