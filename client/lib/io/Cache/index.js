const Cache = function (sizeLimit) {
  // @io.Cache(sizeLimit)
  //
  // A helper class for caching http requests client-side.
  // Discards cached contents in first-in-first-out manner.
  //
  // Parameters:
  //   sizeLimit
  //     a positive integer. How many values to store.
  //
  if (typeof sizeLimit !== 'number' || isNaN(sizeLimit) || sizeLimit < 0) {
    throw new Error('Bad cache size limit: ' + sizeLimit)
  }

  this.sizeLimit = sizeLimit
  this.storage = {} // key->object
  this.keyQueue = [] // ordered list of keys, oldest first
}

module.exports = Cache
const proto = Cache.prototype
proto.isCache = true

proto.clear = require('./clear')
proto.has = require('./has')
proto.read = require('./read')
proto.remove = require('./remove')
proto.write = require('./write')
