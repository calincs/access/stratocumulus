module.exports = function () {
  // @io.Cache:clear()
  //
  // Clear the cache. See :remove to clear specific key.
  //

  // Smash the cache object in order to ensure garbage collecting
  // just in case there are bad references.

  const len = this.keyQueue.length
  for (let i = 0; i < len; i += 1) {
    const key = this.keyQueue[i]
    delete this.storage[key]
  }

  this.keyQueue = []
  this.storage = {}
}
