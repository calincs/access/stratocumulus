module.exports = function (key) {
  // @io.Cache:remove(key)
  //
  // Remove a cached value and return it.
  //
  // Parameters:
  //   key
  //     a string
  //
  // Return
  //   the removed value or null if it did not exist.
  //

  if (!(key in this.storage)) {
    // Nothing to remove
    return null
  }

  const i = this.keyQueue.indexOf(key)

  if (i < 0) {
    // Error: key in storage but not queued.
    delete this.storage[key]
    return null
  }

  // Remove from queue
  this.keyQueue.splice(i, 1)
  const payload = this.storage[key]
  delete this.storage[key]

  return payload
}
