module.exports = function (key) {
  // @io.Cache:has(key)
  //
  // Check if there is a cached value for the key.
  //
  // Parameters:
  //   key
  //     a string
  //
  // Return
  //   a boolean
  //
  return (key in this.storage)
}
