module.exports = function (key, payload) {
  // @io.Cache:write(key, payload)
  //
  // Cache an object or update it.
  // If the cache is full, this operation will discard the oldest entry.
  //
  // Parameters:
  //   key
  //     a string
  //   payload
  //     a value. Cannot be null.
  //
  // In the rare case where the cache size limit is set to zero,
  // no value will be cached.
  //

  if (!key) {
    throw new Error('Bad cache key: ' + key)
  }
  if (!payload) {
    throw new Error('Bad cache payload: ' + payload)
  }
  if (this.sizeLimit <= 0) {
    return
  }

  if (key in this.storage) {
    // Just update
    this.storage[key] = payload

    return
  }

  if (this.keyQueue.length >= this.sizeLimit) {
    // Cache full. Discard the oldest at the beginning of the queue.
    const discardedKey = this.keyQueue.shift()
    delete this.storage[discardedKey]
  }

  // Add new entry.
  this.keyQueue.push(key)
  this.storage[key] = payload
}
