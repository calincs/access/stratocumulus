module.exports = (state, action) => {
  // User interface reducer.
  // Handle sidebar, list order, and other layout settings.
  //
  // Parameters:
  //   state
  //     an object
  //   action
  //     an object
  //
  if (!state.ui) {
    state = Object.assign({}, state, {
      ui: {
        // Keep the sidebar initally hidden. If the view is wide enough, then reveal it explicitly.
        sidebarOpen: true,
        // Default sorting order
        listOrder: {
          property: 'count', // 'label'
          direction: 'desc' // 'asc'
        }
      }
    })
  } else if (!state.ui.listOrder) {
    state = Object.assign({}, state, {
      ui: Object.assign({}, state.ui, {
        // Default sorting order
        listOrder: {
          property: 'count', // 'label'
          direction: 'desc' // 'asc'
        }
      })
    })
  }

  if (action.type === 'sidebar/toggle') {
    // Handle optional property: action.isOpen
    // Invert by default.
    let isOpen = !state.ui.sidebarOpen
    if ('isOpen' in action && typeof action.isOpen === 'boolean') {
      isOpen = action.isOpen
    }

    state = Object.assign({}, state, {
      ui: Object.assign({}, state.ui, {
        sidebarOpen: isOpen
      })
    })
  }

  if (action.type === 'list/sort') {
    if (['label', 'count'].includes(action.property) &&
        ['asc', 'desc'].includes(action.direction)) {
      state = Object.assign({}, state, {
        ui: Object.assign({}, state.ui, {
          listOrder: {
            property: action.property,
            direction: action.direction
          }
        })
      })
    }
  }

  return state
}
