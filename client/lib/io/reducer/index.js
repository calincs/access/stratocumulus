const uiReducer = require('./uiReducer')
const contextReducer = require('./contextReducer')
const maturityReducer = require('./maturityReducer')
const navigationReducer = require('./navigationReducer')

module.exports = (state, action) => {
  // Main reducer.
  // This pure function takes in the current state object and an action and
  // returns a new state object. The old state is not modified.
  //
  if (!state) {
    state = {}
  }

  // UI interface related state, such as sidebar opening
  state = uiReducer(state, action)
  // Context has its dedicated reducer.
  state = contextReducer(state, action)
  // Track if and when space has content.
  state = maturityReducer(state, action)
  // Handle navigation requests
  state = navigationReducer(state, action)

  return state
}
