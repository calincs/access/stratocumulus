module.exports = (state, action) => {
  // Navigation state reducer.
  //
  // Parameters:
  //   state
  //     an object
  //   action
  //     an object
  //
  // Returns:
  //   an object, the state
  //

  if (action.type === 'navigation/node') {
    // Validate action
    const validLabel = typeof action.label === 'string'
    if (!validLabel) {
      throw new Error('Invalid object for action navigation/node: ' + action)
    }
    // Update wanted node
    return Object.assign({}, state, {
      wantedNode: {
        label: action.label
      }
    })
  }

  // Any other navi action clears the wanted node.
  if (state.wantedNode) {
    return Object.assign({}, state, {
      wantedNode: null
    })
  }

  return state
}
