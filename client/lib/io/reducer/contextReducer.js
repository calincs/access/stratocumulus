const Context = require('../../Context')
const setAdd = require('./setAdd')
const contextHelper = require('../../contextHelper')

module.exports = (state, action) => {
  // Context reducer.
  // This pure function takes in the current state object and an action and
  // returns a new state object. The old state is not modified.
  //
  // Parameters:
  //   state
  //     an object
  //   action
  //     an object
  //
  // Return:
  //   an object, the state
  //
  if (!state.context) {
    state = Object.assign({}, state, {
      context: new Context(),
      contextHistory: {},
      zoomToContext: false
    })
  }

  // Navigation initialization
  if (action.type === 'init') {
    if (action.context) {
      return Object.assign({}, state, {
        context: action.context,
        contextHistory: setAdd(state.contextHistory, action.context.toUrlPath()),
        zoomToContext: true
      })
    }
    return state
  }

  // Navigation that the user activated by clicking a link or button or editing url.
  if (action.type === 'navigation/link') {
    // Filter known parameters.
    const context = contextHelper.fromUrl(action.path)
    const contextHistory = setAdd(state.contextHistory, context.toUrlPath())
    return Object.assign({}, state, {
      context,
      contextHistory,
      zoomToContext: true
    })
  }

  // Navigation that the user activated by zooming.
  // This should not change the zoom position.
  if (action.type === 'navigation/zoom') {
    // Filter known parameters.
    const context = contextHelper.fromUrl(action.path)
    const contextHistory = setAdd(state.contextHistory, context.toUrlPath())
    return Object.assign({}, state, {
      context,
      contextHistory,
      zoomToContext: false
    })
  }

  // Navigation that user activated by adding or removing filters.
  if (action.type === 'navigation/filter') {
    // Replace parameter value or remove if no valid value.
    let context = state.context.remove(action.parameter)
    if (typeof action.value === 'string' && action.value.length > 0) {
      context = context.append(action.parameter, action.value)
    }
    const contextHistory = setAdd(state.contextHistory, context.toUrlPath())
    return Object.assign({}, state, {
      context,
      contextHistory,
      zoomToContext: true
    })
  }

  return state
}
