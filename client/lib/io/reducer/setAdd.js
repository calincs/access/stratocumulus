module.exports = (obj, key) => {
  // Functional conditional immutable set add.
  // Copies the object and adds the key to the copy, given that key exists.
  //
  // Parameters:
  //   obj
  //     an object, acting as a set of keys.
  //   key
  //     a string, the key to add to the set.
  //
  // Return:
  //   another object with the key added if it did not exist yet.
  //
  if (key) {
    if (obj[key]) {
      return obj
    }
    const rec = {}
    rec[key] = true
    return Object.assign(rec, obj)
  }
  return obj
}
