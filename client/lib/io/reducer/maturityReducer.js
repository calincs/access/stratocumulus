// Space maturity levels
const EMPTY_SPACE = 0
const LOADING_SPACE = 1
const POPULATED_SPACE = 2

module.exports = (state, action) => {
  // State maturity reducer.
  //
  // Parameters:
  //   state
  //     an object
  //   action
  //     an object
  //
  // Returns:
  //   an object, the state
  //
  if (!state.maturity) {
    state = Object.assign({}, state, {
      maturity: EMPTY_SPACE
    })
  }

  // Track if space has enough content for navigation.
  if (action.type === 'stratum/loading') {
    if (state.maturity < LOADING_SPACE) {
      state = Object.assign({}, state, {
        maturity: LOADING_SPACE
      })
    }
  }
  if (action.type === 'stratum/first') {
    if (state.maturity < POPULATED_SPACE) {
      state = Object.assign({}, state, {
        maturity: POPULATED_SPACE
      })
    }
  }

  // HACK Allow reacting to maturity after more content
  if (action.type === 'stratum/loaded') {
    state = Object.assign({}, state, {
      maturity: state.maturity + 1
    })
  }

  return state
}
