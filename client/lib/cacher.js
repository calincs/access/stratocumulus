module.exports = (key, fn) => {
  // @cacher(key, fn)
  //
  // Returns a wrapped function. When the wrapped function is called,
  // will call the given function once and store the result.
  // Subsequent calls will return the stored value directly.
  //
  // Parameters:
  //   key
  //     a string, the property name for the cached value
  //   fn
  //     a function, not an array function and without parameters.
  //
  // Returns:
  //   a function
  //
  return function () {
    // We cache the results into 'this' of the returned function.
    if (!this[key]) {
      this[key] = fn.call(this)
    }
    return this[key]
  }
}
