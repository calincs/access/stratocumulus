module.exports = function (state) {
  // @Indicator:set(state)
  //
  // Set indicator state.
  //
  // Parameters:
  //   state
  //     a string, one of: default, empty, error
  //

  if (state === 'empty') {
    this.component.removeClass('loading-error')
    this.component.addClass('empty-result')
    this.component.html('<span class="symbol">&varnothing;</span><br>No results.<br>Zoom out or remove a filter.')
  } else if (state === 'error') {
    this.component.removeClass('empty-result')
    this.component.addClass('loading-error')
    this.component.html('<span class="symbol">&#9888;</span><br>An error occurred while fetching results. Please try again later.')
  } else {
    this.component.removeClass('empty-result')
    this.component.removeClass('loading-error')
    this.component.html('')
  }
}
