require('./style.css')
const tapspace = require('tapspace')
const STRATUM_SIZE = require('../config').rendering.stratumSize
const RENDER_SIZE = STRATUM_SIZE / 4

const Indicator = function (options) {
  // @Indicator([options])
  //
  // Stratum state indicator for empty or errored strata.
  //
  // Parameters:
  //   options
  //     optional object with properties
  //       TODO
  //

  if (!options) {
    options = {}
  }

  const item = tapspace.createItem()
  item.setSize(RENDER_SIZE, RENDER_SIZE)
  item.setAnchor(item.atCenter())
  item.addClass('stratum-state-indicator')

  this.component = item
}

module.exports = Indicator
const proto = Indicator.prototype
proto.isIndicator = true

proto.set = require('./set')
