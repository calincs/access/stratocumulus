const contextHelper = require('../contextHelper')

module.exports = function () {
  // @Stratum:getSupercontext()
  //
  // Get faceting context for the superstratum.
  // Basically takes the stratum context, and returns a new context
  // with the last faceting parameter removed.
  // This method needs to perfectly reverse the getEverySubcontext
  // to keep the strata in stable order.
  //
  // Return
  //   a Context or null is stratum is root.
  //

  // Backtrack to broader set of facets.
  return contextHelper.super(this.context)
}
