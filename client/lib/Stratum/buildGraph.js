const graphology = require('graphology')
const MAX_NODES = 128

module.exports = stratum => {
  // Build a graphology graph from stratum object.
  //
  // Parameters:
  //   stratum
  //     an object, a stratum object from stratocumulus server
  //
  // Return
  //   a graphology.Graph
  //
  const graph = new graphology.Graph()

  // Copy and sort in descending order.
  const nodes = stratum.nodes.slice().sort((a, b) => b.count - a.count)
  // Take largest nodes.
  const topNodes = nodes.slice(0, MAX_NODES)

  topNodes.forEach(node => {
    try {
      graph.addNode(node.id, node)
    } catch (err) {
      console.warn(err.message)
    }
  })

  stratum.edges.forEach(edge => {
    // Some nodes were left out so ensure before adding the edge.
    if (graph.hasNode(edge.from) && graph.hasNode(edge.to)) {
      graph.addEdge(edge.from, edge.to)
    }
  })

  return graph
}
