module.exports = function () {
  // @Stratum:revealDetails()
  //
  // The method computes the sizes of elements in the current
  // viewport scale and reveals or hides details respectively.
  // In other words, it refresh the label visibility in a semantic zoom manner.
  // It reveals and hides the labels based on their apparent size.
  //

  if (!this.alive) {
    // Stratum already removed, no need to refresh anything.
    return
  }

  const viewport = this.space.getViewport()
  const nodes = Object.values(this.renderedNodes)

  if (!viewport) {
    // Not yet in DOM
    return
  }

  nodes.forEach(node => {
    const nodeItem = node.component
    const dilation = viewport.measureDilation(nodeItem)

    if (node.isSelected) {
      // Make labels of selected nodes appear constant.
      nodeItem.addClass('readable')
      node.adjustLabelSize()
    } else if (dilation > 0.05 && dilation < 10) {
      nodeItem.addClass('readable')
    } else {
      nodeItem.removeClass('readable')
    }

    // Load loadable nodes, if any.
    if (dilation > 0.05 && dilation < 10) {
      if (node.load && !node.isLoaded) {
        node.load()
      }
    }
  })
}
