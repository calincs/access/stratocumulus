module.exports = function (subcontext) {
  // @Stratum:getOpenableNode(subcontext)
  //
  // Get a node by its subcontext. Can be null if no node found.
  //
  // Parameters:
  //   subcontext
  //     a Context. The subcontext of the node.
  //
  // Return
  //   a StratumNode or null
  //
  const node = Object.values(this.renderedNodes).find(n => {
    return n.subcontext && subcontext.equals(n.subcontext)
  })

  return node || null
}
