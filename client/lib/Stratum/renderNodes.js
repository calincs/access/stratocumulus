const layout = require('./layout')
const StratumNode = require('../StratumNode')
const config = require('../config')
const NODE_RENDER_WIDTH = config.rendering.stratumNodeSize // px

module.exports = function () {
  // @Stratum:renderNodes()
  //
  // Render nodes or refresh the node positions.
  //
  const nodePlaneOrigin = this.nodePlane.at(0, 0)

  if (!this.model) {
    // Not yet populated.
    return
  }

  const nodeCircleMap = layout(this.model)

  // Current run.
  const now = Date.now() + Math.random()

  // Render largest last so that their label visibility is ensured.
  // The largest nodes have largest labels.
  const sortedNodes = this.model.nodes.slice().sort((a, b) => a.count - b.count)

  // Map each node in graph model to a visible tapspace item.
  sortedNodes.forEach((nodeModel) => {
    const key = nodeModel.id
    let stratumNode = this.renderedNodes[key]
    let animateMove = false

    if (stratumNode) {
      // Node already exists.
      // Update content and scale according to attributes.
      stratumNode.update(nodeModel)
      animateMove = true
    } else {
      // Node does not exist. Create.
      stratumNode = new StratumNode(this.context, key, nodeModel)
      this.nodePlane.addChild(stratumNode.component)

      // Build index of rendered nodes.
      this.renderedNodes[key] = stratumNode
      // No animation, quick insert.
      animateMove = false
    }

    // Keep time since render. Helps in clearing the extra nodes.
    stratumNode.timestamp = now

    // Derive scale relative to the stratum basis.
    // The diameter distibution is mapped to 0..NODE_RENDER_WIDTH (256).
    // Therefore r=256 => scaleFactor=1; r=128 => scaleFactor=0.5; r=0 => scaleFactor=0
    const nodeCircle = nodeCircleMap[key]
    if (nodeCircle) {
      const nodeDiameter = nodeCircle.r * 2
      const scaleFactor = nodeDiameter / NODE_RENDER_WIDTH
      stratumNode.setScale(scaleFactor)
      // Update node position and scale according to the layout.
      const nodePoint = nodePlaneOrigin.offset(nodeCircle.x, nodeCircle.y)
      stratumNode.translateTo(nodePoint, animateMove)
    } else {
      console.warn('Missing circle for node:', key)
    }
  })

  // Remove all nodes with older timestamp.
  // Those nodes are not anymore in the data.
  Object.values(this.renderedNodes).forEach(n => {
    if (n.timestamp !== now) {
      n.remove()
      delete this.renderedNodes[n.key]
    }
  })
}
