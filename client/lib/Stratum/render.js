const buildGraph = require('./buildGraph')

module.exports = function () {
  // @Stratum:render()
  //
  // Render the graph. If elements already exist, update.
  // This method is idempotent, thus you can call this method multiple times.
  //

  if (!this.model) {
    // Too early to render
    return
  }

  this.graph = buildGraph(this.model)

  // Create or update nodes and remove excess.
  this.renderNodes()

  // Re-position the graph w.r.t. the stratum boundaries.
  this.scaleNodesToFit()

  this.enableFaceting()
  // this.renderEdges()

  // Signal that node positions might have changed,
  // so that possible open substrata can be repositioned.
  this.emit('layout')
}
