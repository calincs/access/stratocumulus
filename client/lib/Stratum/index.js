require('./style.css')
const emitter = require('component-emitter')
const tapspace = require('tapspace')
const Spinner = require('../Spinner')
const Indicator = require('../Indicator')
const RENDER_SIZE = require('../config').rendering.stratumSize

const Stratum = function (context) {
  // @Stratum(context)
  //
  // Inherits Emitter
  //
  // Stratum is a layer in a nested hierarhcy of graphs.
  // Stratum is a graph laid on a plane.
  //
  // The constructor does not add the graph to the document.
  // Append stratum.space to a parent space in order to do that.
  //
  // Parameters:
  //   context
  //     a Context. The context gives identity to the stratum and
  //     .. defines the faceting and filtering of its content.
  //
  // Stratum emits:
  //   final
  //     when the stratum has been loaded and rendered.
  //   substratumrequest
  //     when the stratum would like one of its nodes to be opened as
  //     .. a new stratum. Emitted with an object { context } where
  //     .. the context is the requested context of the substratum.
  //   layout
  //     when the stratum layout changes
  //

  // DEBUG validate arguments
  if (!context || !context.isContext) {
    throw new Error('Invalid stratum context: ' + context)
  }

  // Alive until removed.
  this.alive = true
  // Keep track if still loading.
  this.loading = false
  // Read-only stratum object model. Used for data.
  this.model = null

  // The faceting context.
  this.path = context.toUrlPath()
  this.context = context

  // Create container for the stratum
  this.space = tapspace.createSpace()
  this.space.addClass('stratum-plane')
  // Allow fast filtering of strata from all other components.
  this.space.isStratum = true
  // Allow references from component to Stratum.
  // This is a circular reference which usually is a bad thing.
  // However, the stratum and its plane are tightly coupled and
  // we need the ability to reference them to both directions,
  // so the circular reference here is better than some additional index.
  this.space.stratum = this

  // Content type specific classes for content type specific styles.
  if (context.hasParameter('content_type')) {
    const contentType = context.getValue('content_type')
    this.space.addClass('stratum-' + contentType.toLowerCase())
  } else {
    // The content type selector stratum
    this.space.addClass('stratum-root')
  }

  // Create containers for nodes and edges.
  // Create the containers separately so that it is easy to
  // iterate them and control their rendering order.
  this.nodePlane = tapspace.createSpace()
  this.nodePlane.addClass('stratum-plane-nodes')
  this.edgePlane = tapspace.createSpace()
  this.edgePlane.addClass('stratum-plane-edges')
  // Edges must come first in order for nodes to be rendered on top.
  this.space.addChild(this.edgePlane)
  this.space.addChild(this.nodePlane)

  // Keep track of rendered nodes. nodeKey -> StratumNode
  this.renderedNodes = {}
  // Keep track of rendered edges. edgeKey -> StratumEdge
  this.renderedEdges = {}

  // Set stratum bounding circle. This is done only once.
  // Note that this does not follow the actual size of stratum content.
  const radius = RENDER_SIZE / 2
  const circle = { x: radius, y: radius, z: 0, r: radius }
  this.boundingCircle = new tapspace.geometry.Circle(this.space, circle)

  // Spinner that can be hidden after stratum is not loading.
  this.spinner = new Spinner({
    diameter: 200,
    circles: 5,
    circleDiameter: 20
  })
  this.space.addChild(this.spinner.component)
  this.spinner.component.translateTo(this.space.at(radius, radius))

  // Indicator for an empty or errored stratum.
  // For example, try URL for empty stratum.
  // /?content_type=Person&f_decades=1720&f_countries.id=6595b7e81a18712f1f6a0a08
  this.indicator = new Indicator()
  this.space.addChild(this.indicator.component)
  this.indicator.component.translateTo(this.space.at(radius, radius))
}

module.exports = Stratum
const proto = Stratum.prototype
proto.isStratum = true

// Inherit
emitter(proto)

// Methods
proto.addVirtualNode = require('./addVirtualNode')
proto.enableFaceting = require('./enableFaceting')
proto.findNearbyNode = require('./findNearbyNode')
proto.getBasisForSubstratum = require('./getBasisForSubstratum')
proto.getEverySubcontext = require('./getEverySubcontext')
proto.getOpenableNode = require('./getOpenableNode')
proto.getNodes = require('./getNodes')
proto.getOrigin = require('./getOrigin')
proto.getSpace = require('./getSpace')
proto.getSupercontext = require('./getSupercontext')
proto.limitZoom = require('./limitZoom')
proto.load = require('./load')
proto.remove = require('./remove')
proto.render = require('./render')
proto.renderEdges = require('./renderEdges')
proto.renderNodes = require('./renderNodes')
proto.revealDetails = require('./revealDetails')
proto.scaleNodesToFit = require('./scaleNodesToFit')
proto.triggerSubstratum = require('./triggerSubstratum')
