module.exports = function (areaRatioLimit) {
  // @Stratum:limitZoom([areaRatioLimit])
  //
  // Limit the zoom scale by moving the viewport closer.
  //
  // Parameters:
  //   areaRatioLimit
  //     optional positive number, default is 0.1
  //

  const viewport = this.space.getViewport()
  const viewportArea = viewport.getArea().getRaw()
  const currentCircle = this.boundingCircle
  const area = currentCircle.getArea().transitRaw(viewport)
  const areaRatio = area / viewportArea
  const ratioLimit = areaRatioLimit || 0.1 // NOTE zero turns to 0.1
  if (areaRatio < ratioLimit) {
    viewport.animateOnce({ duration: 800 })
    viewport.zoomToFit(this.space, 0.6)
  }
}
