module.exports = function () {
  // @Stratum:remove()
  //
  // Destroy the stratum. Stop loading and kill listeners.
  // However, does not remove the stratum from the DOM.
  // Let DOM removal happen externally by driver/generator.
  //
  if (!this.alive) {
    // Already removed
    return
  }

  this.alive = false

  // Stop all listeners
  this.off()

  // TODO remove each node individually to help the garbage collector?
}
