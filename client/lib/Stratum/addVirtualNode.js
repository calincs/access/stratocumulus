const config = require('../config')
const facetParameters = window.stratocumulus.facetParameters

module.exports = function (subcontext, substratum) {
  // @Stratum:addVirtualNode(subcontext, substratum)
  //
  // Sometimes we need to enforce stratum to contain certain subcontext
  // in order to provide a way to access to certain substratum.
  // For that we create a virtual facet node.
  //
  // Parameters:
  //   subcontext
  //     a Context
  //   substratum
  //     a Stratum
  //
  // Return:
  //   a StratumNode or null
  //

  const contentType = subcontext.getValue('content_type')
  if (!contentType) {
    // Bad context
    console.warn('Cannot add virtual facet node. Bad subcontext: ' + subcontext.toUrlPath())
    return null
  }

  const typeConfig = config.contentTypeMap[contentType]
  if (!typeConfig) {
    // Bad content type
    console.warn('Cannot add virtual facet node. Bad content type: ' + contentType)
    return null
  }

  // TODO maybe server should provide the full context path in addition to params
  // TODO In that way could have any subcontext, not just the last parameter.
  const lastFacet = subcontext.getLast(facetParameters)
  const facetParam = lastFacet.parameter
  const facetValue = lastFacet.value

  // Get facet config
  const facetConfig = typeConfig.facets.find(fc => {
    return fc.parameter === facetParam
  })

  if (!facetConfig) {
    // Parameter is not supported.
    console.warn('Bad facet parameter: ' + facetParam + '. Ensure it is configured for ' + contentType)
    // We cannot get labels nor fields for the node.
    return null
  }

  // Determine labels from the substratum context.
  let label = null
  let labelPrefix = null
  const labelPostfix = null
  // Read substratum label.
  const contextLabels = substratum.model.context
  if (contextLabels && contextLabels.length > 0) {
    const ultimate = contextLabels.length - 1
    const tailContext = contextLabels[ultimate]
    label = tailContext.valueLabel
    labelPrefix = tailContext.parameterLabel
  } else {
    console.warn('Unable to generate labels for a virtual facet node.')
    label = 'Entities'
  }

  // Get count from substratum.
  const count = substratum.model.count

  // Build facet node model
  const facetNode = {
    id: facetConfig.id + '/' + facetValue,
    role: 'facet',
    // Facet node type.
    // In a perfect world, e.g. Professions could be real type
    // instead of label for query parameter.
    type: facetConfig.label,
    field: facetConfig.field,
    label,
    labelPrefix,
    labelPostfix,
    contentType,
    count,
    facetParam,
    facetValue,
    parent: 'self'
  }
  this.model.nodes.push(facetNode)

  // Render the stratum again. This creates the node instance.
  this.render()

  // Return the new node.
  const virtualNode = this.renderedNodes[facetNode.id]
  return virtualNode
}
