const config = require('../config')
const RENDER_SIZE = config.rendering.stratumSize

module.exports = function () {
  // @Stratum:scaleNodesToFit()
  //
  // Transforms the node plane so that it fits the rendering boundary
  // for the stratum. This aims to keep the nodes at practical size.
  //

  // Get current node boundary
  const graphCircle = this.nodePlane.getBoundingCircle()
  const circleCenter = graphCircle.atCenter()
  const circleRadius = graphCircle.getRadius()
  // Get control points
  const circleTop = circleCenter.polarOffset(circleRadius, -Math.PI / 2)
  const circleBottom = circleCenter.polarOffset(circleRadius, Math.PI / 2)
  const targetTop = this.space.at(RENDER_SIZE / 2, 0.09 * RENDER_SIZE)
  const targetBottom = this.space.at(RENDER_SIZE / 2, 0.94 * RENDER_SIZE)
  // Move nodes to place
  this.nodePlane.match({
    source: [circleTop, circleBottom],
    target: [targetTop, targetBottom],
    estimator: 'TS'
  })
}
