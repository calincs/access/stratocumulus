// Spira mirabilis

exports.getRadius = (radians, a, k) => {
  // @logSpiral.getRadius(rads, a, k)
  //
  // Radius at the given angle.
  //
  // Parameters:
  //   radians
  //     a number, angle in radians
  //   a
  //     a number, the scaler, the radius at rads=0
  //   k
  //     a number, the accelerator, recommended values -1 < k < 0 for inward spiral.
  //
  // Return
  //   a number, the radius
  //
  return a * Math.exp(k * radians)
}

exports.getAngle = (radius, a, k) => {
  // @logSpiral.getAngle(radius, a, k)
  //
  // Angle at the given radius.
  //
  // Parameters:
  //   radius
  //     a number
  //   a
  //     a number, the spiral scaler, the radius at angle=0
  //   k
  //     a number, the slope accelerator, recommended values -1 < k < 0
  //
  // Return:
  //   a number, the angle in radians. The angle can be larger than full circle.
  //
  return (1 / k) * Math.log(radius / a)
}
