const nodeRadiusMap = require('./nodeRadiusMap')
const circlepack = require('affineplane-circlepack')

module.exports = function (stratum) {
  // Compute a circle-packed node arrangement for the given stratum.
  //
  // Parameters:
  //   stratum
  //     an object, a plain stratum model.
  //
  // Return
  //   a map: nodeKey -> {x,y,r}. The layout positions and sizes.
  //

  // Build a map from node id to circles.
  const circleMap = {}

  // Margin of nodes: How much smaller should node radius be than their allocated space?
  const nodeRadiusAdjust = 0.8

  // Build normalized mapping from node to its radius.
  const radiusMap = nodeRadiusMap(stratum.nodes)

  // Map the nodes into circles
  const circles = stratum.nodes.map(n => {
    // TODO mapping dimensions.
    return {
      x: 0,
      y: 0,
      r: radiusMap[n.id],
      id: n.id
    }
  })

  // Pack them nicely.
  const packed = circlepack(circles)

  for (let i = 0; i < packed.length; i++) {
    const c = packed[i]
    // Bring margin around nodes.
    c.r = c.r * nodeRadiusAdjust
    // TODO clear up the objects during circlepack
    delete c.edges
    delete c.i
  }

  // Build the circle map
  for (let i = 0; i < packed.length; i++) {
    circleMap[packed[i].id] = packed[i]
  }

  return circleMap
}
