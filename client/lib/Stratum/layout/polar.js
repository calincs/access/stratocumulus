const nodeRadiusMap = require('./nodeRadiusMap')
const config = require('../../config')
const STRATUM_WIDTH = config.rendering.stratumSize
const NODE_WIDTH = config.rendering.stratumNodeSize

module.exports = function (stratum) {
  // Compute a polar node arrangement for the given stratum.
  //
  // Parameters:
  //   stratum
  //     an object, a plain stratum model.
  //
  // Return
  //   a map: nodeKey -> {x,y,r}. The layout positions and sizes.
  //

  // Build a map from node id to circles.
  const circleMap = {}

  // Place nodes along an arc.
  const numNodes = stratum.nodes.length
  const stepAngle = 2 * Math.PI / numNodes
  const poleRadius = 0.3 * STRATUM_WIDTH
  // How much space each node should take?
  // Available area: STRATUM_WIDTH^2
  // Area per node: STRATUM_WIDTH^2 / numNodes
  // Node width: sqrt(area per group)
  const nodeWidth = Math.ceil(STRATUM_WIDTH / Math.sqrt(numNodes))
  // Margin of nodes: How much smaller should node radius be than their allocated space?
  const nodeRadiusAdjust = 0.8

  // Build normalized mapping from node to its radius.
  const radiusMap = nodeRadiusMap(stratum.nodes)
  const scaler = nodeWidth / NODE_WIDTH

  // Place in size order:
  const sorted = stratum.nodes.slice().sort((a, b) => b.count - a.count)

  // Map the nodes into circles
  const circles = sorted.map((n, i) => {
    const angle = i * stepAngle
    return {
      x: poleRadius * Math.cos(angle),
      y: poleRadius * Math.sin(angle),
      r: radiusMap[n.id] * scaler * nodeRadiusAdjust,
      id: n.id
    }
  })

  // Build the circle map
  for (let i = 0; i < circles.length; i++) {
    circleMap[circles[i].id] = circles[i]
  }

  return circleMap
}
