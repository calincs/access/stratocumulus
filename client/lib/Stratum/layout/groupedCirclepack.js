const nodeRadiusMap = require('./nodeRadiusMap')
const circlepack = require('affineplane-circlepack')
const config = require('../../config')
const STRATUM_WIDTH = config.rendering.stratumSize

module.exports = function (stratum) {
  // Compute a grouped node arrangement for the given stratum.
  //
  // Parameters:
  //   stratum
  //     an object, a plain stratum model.
  //
  // Return
  //   a map: nodeKey -> {x,y,r}. The layout positions and sizes.
  //

  // Build a map from node id to circles.
  const circleMap = {}

  // Divide the nodes into groups { labelNode, nodes } .
  // TODO Do this server side?
  const groups = {}
  const n = stratum.nodes.length
  for (let i = 0; i < n; i++) {
    const node = stratum.nodes[i]
    const pid = node.parent
    if (pid) {
      if (pid === 'self') {
        if (groups[node.id]) {
          groups[node.id].labelNode = node
        } else {
          groups[node.id] = {
            labelNode: node,
            nodes: []
          }
        }
      } else {
        if (groups[pid]) {
          groups[pid].nodes.push(node)
        } else {
          groups[pid] = {
            labelNode: null,
            nodes: [node]
          }
        }
      }
    }
  }

  // Place groups along an arc.
  const numGroups = Object.keys(groups).length
  const stepAngle = 2 * Math.PI / numGroups
  const groupPoleRadius = 0.5 * STRATUM_WIDTH
  // How much space each group can take?
  // Available area: STRATUM_WIDTH^2
  // Area per group: STRATUM_WIDTH^2 / numGroups
  // Group width: sqrt(area per group)
  const groupWidth = 0.8 * Math.ceil(STRATUM_WIDTH / Math.sqrt(numGroups))
  // Margin of nodes: How much smaller should node radius be than their allocated space?
  const nodeRadiusAdjust = 0.8

  Object.keys(groups).forEach((groupId, i) => {
    // Variable: group
    //   n array of stratum node model.
    const group = groups[groupId]

    // Build normalized mapping from node to its radius.
    const radiusMap = nodeRadiusMap(group.nodes)

    const groupAngle = i * stepAngle
    const groupOrigin = {
      x: groupPoleRadius * Math.cos(groupAngle),
      y: groupPoleRadius * Math.sin(groupAngle)
    }

    // Map the nodes into circles
    const circles = group.nodes.map(n => {
      // TODO configurable mapping dimensions.
      if (n.field === 'decades') {
        const year = parseInt(n.facetValue) || 2000
        return {
          x: 1.3 * year,
          y: 0,
          r: radiusMap[n.id],
          id: n.id
        }
      }

      return {
        x: 0,
        y: 0,
        r: radiusMap[n.id],
        id: n.id
      }
    })

    // Pack them nicely.
    const packed = circlepack(circles)

    // Scale the positions so that each group takes about an equal space.
    let xmin = STRATUM_WIDTH // begin with inverted extremes
    let xmax = -STRATUM_WIDTH
    let ymin = STRATUM_WIDTH
    let ymax = -STRATUM_WIDTH
    for (let i = 0; i < packed.length; i++) {
      const c = packed[i]
      xmin = Math.min(xmin, c.x - c.r * nodeRadiusAdjust)
      xmax = Math.max(xmax, c.x + c.r * nodeRadiusAdjust)
      ymin = Math.min(ymin, c.y - c.r * nodeRadiusAdjust)
      ymax = Math.max(ymax, c.y + c.r * nodeRadiusAdjust)
    }
    const xoff = -(xmin + xmax) / 2 // align center
    const yoff = -(ymin + ymax) / 2
    const xdiff = Math.max(xmax - xmin, 1) // force positive
    const ydiff = Math.max(ymax - ymin, 1) // force positive
    const scaler = groupWidth / Math.max(xdiff, ydiff) // map spread between 0..512
    for (let i = 0; i < packed.length; i++) {
      const c = packed[i]
      c.x = groupOrigin.x + (c.x + xoff) * scaler
      c.y = groupOrigin.y + (c.y + yoff) * scaler
      c.r = c.r * scaler * nodeRadiusAdjust
      // TODO clear up the objects during circlepack
      delete c.edges
      delete c.i
    }

    // Build the circle map
    for (let i = 0; i < packed.length; i++) {
      circleMap[packed[i].id] = packed[i]
    }

    // Add the grouping node.
    // Move the grouping node below the group.
    const groupHeight = ydiff * scaler
    const gnode = group.labelNode
    if (gnode) {
      circleMap[gnode.id] = {
        x: groupOrigin.x,
        y: groupOrigin.y + 0.8 * groupHeight,
        r: 0.25 * groupWidth
      }
    }
  })

  return circleMap
}
