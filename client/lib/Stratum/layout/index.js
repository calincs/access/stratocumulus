const polarLayout = require('./polar')
const circlepackLayout = require('./circlepack')
const groupedLayout = require('./groupedCirclepack')

module.exports = function (stratum) {
  // Compute an node arrangement for the given stratum.
  //
  // Parameters:
  //   stratum
  //     an object, a plain stratum model.
  //
  // Return
  //   a map: nodeKey -> {x,y,r}. The graphology layout positions and sizes.
  //

  // Select layout method
  switch (stratum.layout) {
    case 'polar':
      return polarLayout(stratum)
    case 'circlepack':
      return circlepackLayout(stratum)
    case 'grouped-circlepack':
      return groupedLayout(stratum)
    default:
      return circlepackLayout(stratum)
  }
}
