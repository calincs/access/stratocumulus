const config = require('../../config')
const RENDER_WIDTH = config.rendering.stratumNodeSize // px
const RENDER_RADIUS = Math.floor(RENDER_WIDTH / 2)

// const limit = (x, min, max) => Math.min(Math.max(x, min), max)
const max = Math.max
const sqrt = Math.sqrt
const PI = Math.PI

module.exports = (nodes) => {
  // Compute radii of nodes according to entity counts.
  // The radius is ib the rendering pixels.
  // Radius affects how the layout algorithm place the nodes apart.
  //
  // Parameters:
  //   nodes
  //     an array of node models
  //
  // Return:
  //   an object: string nodeKey -> number radius
  //

  // Largest node entity count
  const topCount = nodes.reduce((acc, n) => (acc < n.count ? n.count : acc), 1)
  const topArea = topCount
  // const sumArea = nodes.reduce((acc, n) => acc + n.count, 0)
  // const topRatio = topArea / sumArea
  const topRadius = sqrt(topArea / PI)
  const pixelScale = RENDER_RADIUS / topRadius

  const radiusMap = {}

  for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i]
    // Norm between 0..1
    // Treat as an area.
    const area = max(node.count, 1)
    // const areaRatio = area / sumArea
    const radius = sqrt(area / PI)
    const radiusPx = radius * pixelScale

    radiusMap[node.id] = radiusPx
  }

  return radiusMap
}
