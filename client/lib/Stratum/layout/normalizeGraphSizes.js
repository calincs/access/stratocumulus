const config = require('../../config')
const MIN_NODE_SIZE = config.sizing.minNodeSize
const MAX_NODE_SIZE = config.sizing.maxNodeSize

// This function maps the number of matching entities, the raw entity count,
// to the adjusted count that is used for normalization and sizing.
// Logarithmic scaler is usually good for power-law distributed data.
const scaler = x => (x > 1 ? Math.log(x) : 1)
// Square root scaler is good for comparing physical sizes.
// const scaler = x => Math.sqrt(x)

const normalize = (x, min, max) => {
  // Map value x to position 0..1 in min max range.
  const l = Math.min(max, Math.max(min, x))
  const d = l - min
  const w = max - min
  return d / w
}

module.exports = (graph) => {
  // Compute .size attributes for nodes according to entity counts.
  // The .size is used by the layout algorithm to place nodes apart.
  // Counts represent the number of matching entities.
  //
  // Parameters:
  //   graph
  //     a graphology Graph
  //

  // Find min and max entity counts over the graph for each group.
  const extremeCounts = graph.reduceNodes((acc, key, attrs) => {
    // Skip the grouping nodes, because their count is not real.
    // But init their group count nevertheless. The group can be empty.
    if (attrs.role === 'grouping' || attrs.role === 'root') {
      if (!(key in acc)) {
        acc[key] = {
          maxCount: 0,
          sumCount: 0
        }
      }
      return acc
    }
    // Skip null nodes, because their count is not interesting.
    if (!attrs.facetValue) {
      return acc
    }
    // Init the group
    if (!(attrs.parent in acc)) {
      acc[attrs.parent] = {
        maxCount: 0,
        sumCount: 0
      }
    }
    // Clamp extremes
    const extremes = acc[attrs.parent]
    if (extremes.maxCount < attrs.count) {
      extremes.maxCount = attrs.count
    }
    // Compute group sum also
    extremes.sumCount += attrs.count

    return acc
  }, {})

  // For group-size normalization
  const numGroups = Object.keys(extremeCounts).length
  const totalCount = Object.keys(extremeCounts).reduce((acc, key) => acc + extremeCounts[key].sumCount, 0)

  graph.updateEachNodeAttributes((key, attrs) => {
    // Select extremes.
    let groupKey
    if (attrs.role === 'grouping') {
      groupKey = key
    } else if (attrs.parent) {
      groupKey = attrs.parent
    } else {
      throw new Error('Unexpected node parent: ' + attrs.parent)
    }
    const extremes = extremeCounts[groupKey]

    // Select node count.
    let count
    if (attrs.role === 'grouping') {
      count = extremes.maxCount
    } else {
      count = attrs.count
    }

    // Map the count to the size range.
    // Graphology circlepack uses 'size' property, although poorly documented.
    // A = π*r*r <=> r = ±sqrt(A/π)
    const radius = scaler(count)
    const maxRadius = scaler(extremes.maxCount)
    const normRadius = normalize(radius, 0, maxRadius)
    let size = Math.max(MIN_NODE_SIZE, normRadius * MAX_NODE_SIZE)

    // Adjust for group size.
    if (attrs.role !== 'grouping') {
      const groupShare = scaler(extremes.sumCount) / scaler(totalCount)
      const groupBloat = groupShare * numGroups // ratio between groupShare and even share (1/n)
      size = size / groupBloat
    }

    // HACK graphology circlePack hierarchyAttributes requires self-parent...
    const layoutParent = (attrs.role === 'grouping' ? key : attrs.parent)

    return { ...attrs, size, layoutParent }
  })
}
