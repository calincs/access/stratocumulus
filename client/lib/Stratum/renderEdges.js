const tapspace = require('tapspace')

module.exports = function () {
  // @Stratum:renderEdges()
  //
  // Draw or update edges.
  //

  if (!this.model) {
    // Not yet populated.
    return
  }

  // Current run.
  const now = Date.now() + Math.random()

  this.model.edges.forEach((edgeModel) => {
    // edgeKey, edgeAttrs, sourceKey, targetKey) => {
    const edgeKey = edgeModel.id
    const sourceKey = edgeModel.source
    const targetKey = edgeModel.target
    let edgeItem = this.renderedEdges[edgeKey]
    let animateMove = false

    if (edgeItem) {
      // Rendered edge exists.
      animateMove = true
    } else {
      // No such edge yet rendered. Create
      edgeItem = tapspace.createEdge(3)
      edgeItem.addClass('stratum-edge')
      edgeItem.edgeKey = edgeKey
      // or model.edgeKey
      // and consider sourceKey, targetKey
      this.renderedEdges[edgeKey] = edgeItem
      this.edgePlane.addChild(edgeItem)
    }

    // Keep time since render. Helps in clearing the extra nodes.
    edgeItem.timestamp = now

    // Move edge to position. We need the nodes.
    const sourceNode = this.renderedNodes[sourceKey]
    const targetNode = this.renderedNodes[targetKey]

    // Ensure both exists and are affine
    if (sourceNode && targetNode) {
      const scaler = 0.95 // ensure edge end goes under the node border.
      const sourceRadius = sourceNode.getRadius().scaleBy(scaler)
      const targetRadius = targetNode.getRadius().scaleBy(scaler)
      if (animateMove) {
        edgeItem.animateOnce({ duration: 600 })
      }
      edgeItem.trimPoints(
        sourceNode.getOrigin(),
        targetNode.getOrigin(),
        sourceRadius,
        targetRadius
      )
    } else {
      console.warn('Tried to create edge between non-existent elements.')
    }
  })

  // We remove the old edges which might not exist in the new data.
  // To achieve that, remove all edges with older timestamp.
  // Those edges are not anymore in the data.
  Object.values(this.renderedEdges).forEach(edgeItem => {
    if (edgeItem.timestamp !== now) {
      edgeItem.remove()
      delete this.renderedEdges[edgeItem.edgeKey]
    }
  })
}
