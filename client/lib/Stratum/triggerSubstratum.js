module.exports = function () {
  // @Stratum:triggerSubstratum()
  //
  // This method implements semantic zooming effects.
  // The effects depend on the scale, area, and position of
  // the stratum and its nodes.
  //
  // The method can emit 'substratumrequest' that subsequently initiates
  // the loading of substratum, given that the zoom conditions so allow.
  //

  // NOTE Usage of this method might be a distraction.
  // It is likely that there happens too much for the user to comprehend.
  // Click-to-open might provide better feel of control.

  // On the current stratum, find the nearest openable node.
  const currentNode = this.findNearbyNode()

  // If current node is available and openable, open it.
  if (currentNode && currentNode.isOpenable && !currentNode.isOpened) {
    const subcontext = currentNode.getSubcontext()
    if (subcontext) {
      // Eventually triggers loading of the substratum.
      this.emit('substratumrequest', {
        context: subcontext
      })
    }
  }
}
