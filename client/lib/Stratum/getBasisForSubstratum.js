const facetParameters = window.stratocumulus.facetParameters

module.exports = function (subcontext) {
  // @Stratum:getBasisForSubstratum(subcontext)
  //
  // The method returns a basis for the given subcontext.
  // The basis equals the basis of the openable node that
  // represents the substratum.
  //
  // Parameters:
  //   subcontext
  //     a Context
  //
  // Returns:
  //   a tapspace.geometry.Basis or null if no basis found for the subcontext.
  //
  const openableNode = this.getOpenableNode(subcontext)

  if (!openableNode) {
    // DEBUG
    const facet = subcontext.remove('page').getLast(facetParameters)
    const facetParam = facet.parameter
    const facetValue = facet.value
    const msg = facetParam + '=' + facetValue
    console.warn('Unknown or non-existing openable node: ' + msg)
    return null
  }

  const childBasis = openableNode.component.getBasis()

  // Nodes have constant rendering size that is 10th of stratum size.
  return childBasis.scaleBy(0.1, openableNode.component.at(0, 0))
}
