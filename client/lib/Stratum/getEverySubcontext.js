module.exports = function () {
  // @Stratum:getEverySubcontext()
  //
  // Get the substratum context for each openable node.
  // Does not return subcontext of warp nodes as they do not open deeper.
  //
  // Return
  //   an array of Context. The array can be empty.
  //
  const subcontexts = []

  const nodes = Object.values(this.renderedNodes).filter(n => n.isOpenable)
  const len = nodes.length

  for (let i = 0; i < len; i += 1) {
    try {
      const subctx = nodes[i].getSubcontext()
      if (subctx) {
        subcontexts.push(subctx)
      }
    } catch (err) {
      // Some contexts might be null and throw an error. Skip them.
      console.error(err)
    }
  }

  return subcontexts
}
