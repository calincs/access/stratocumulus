module.exports = function () {
  // @Stratum:enableFaceting()
  //
  // Enable stratum faceting.
  // In other words, begin to listen facet nodes for opening requests.
  //

  // TODO should we do this via state dispatch event in nodes?

  // Define listener once
  if (!this.onopeningrequest) {
    this.onopeningrequest = (ev) => {
      const nodeKey = ev.detail
      const node = this.renderedNodes[nodeKey]
      if (!node) {
        return
      }
      const subcontext = node.getSubcontext()
      if (!subcontext) {
        return
      }

      this.emit('substratumrequest', {
        context: subcontext
      })
    }
  }

  const spaceEl = this.space.element

  // Prevent duplicate listener by removing the previous.
  spaceEl.removeEventListener('openingrequest', this.onopeningrequest)

  // Listen for nodes
  spaceEl.addEventListener('openingrequest', this.onopeningrequest)
}
