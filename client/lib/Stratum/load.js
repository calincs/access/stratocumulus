const io = require('../io')

module.exports = function () {
  // @Stratum:load()
  //
  // Carry out the stratum loading process from the server.
  // A stratum is loaded according to its context.
  // This method is idempotent.
  //
  if (this.loading) {
    // Already loading.
    console.warn('Already loading ' + this.path)
    return
  }

  // Mark that it has or will have content.
  this.loading = true
  // Loading style, e.g. hide edges?
  this.space.addClass('stratum-loading')
  // Start loading animations
  this.spinner.start()

  // First loading should enable viewport to zoom to loading animation.
  io.store.dispatch({
    type: 'stratum/loading'
  })

  io.stratumApi.fetchStratum(this.context, (err, stratum) => {
    // Check if stratum was removed during fetch.
    if (!this.alive) {
      return
    }
    // Register that loading is now finished.
    this.loading = false
    // Stop spinner because no more loading.
    this.spinner.stop()
    // Look so that the loading is now finished.
    this.space.removeClass('stratum-loading')

    // Handle error response
    if (err) {
      console.error(err)
      this.indicator.set('error')
      return
    }

    // Store locally
    this.model = stratum
    // Render the graph and do the layout
    this.render()
    // Reveal visible labels.
    this.revealDetails()

    // Detect empty stratum. Show a message.
    if (stratum.nodes.length === 0) {
      this.indicator.set('empty')
    }

    // Inform that new stratum was loaded.
    // First loading should enable viewport to zoom to loading animation.
    io.store.dispatch({
      type: 'stratum/loaded'
    })

    // Inform the loader generator to load neighborhood.
    this.emit('loaded')
  })
}
