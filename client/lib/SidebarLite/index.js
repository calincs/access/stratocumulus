require('./style.css')
const SearchForm = require('../SearchForm')
const ContextView = require('../ContextView')

const SidebarLite = function () {
  // @SidebarLite()
  //
  // A component for search and breadcrumbs when the sidebar is closed.
  //
  this.element = document.createElement('div')
  this.element.className = 'sidebar-lite'

  this.searchForm = new SearchForm()
  const searchFormEl = this.searchForm.getElement()
  searchFormEl.classList.add('sidebar-lite-row')
  this.element.appendChild(searchFormEl)

  this.contextView = new ContextView()
  const contextViewEl = this.contextView.getElement()
  contextViewEl.classList.add('sidebar-lite-row')
  this.element.appendChild(contextViewEl)
}

module.exports = SidebarLite
const proto = SidebarLite.prototype

// Methods
proto.getElement = require('./getElement')
