const typeConfigMap = window.stratocumulus.contentTypeMap
const fieldValuesToHtml = require('./fieldValuesToHtml')

module.exports = entity => {
  // Build HTML details for entity to be displayed on the sidebar card after the title.
  //
  // Parameters:
  //   an object, an entity
  //
  // Return
  //   a string. The string is empty for nullish or unknown entity.
  //

  if (!entity) {
    return ''
  }

  if (!entity.content_type) {
    console.warn('Missing entity type.')
    return ''
  }

  const typeConfig = typeConfigMap[entity.content_type]
  if (!typeConfig) {
    console.warn('No type configuration found for: ' + entity.content_type)
    return ''
  }

  // Preprocess template fields
  const fields = typeConfig.template.map(fieldConfig => {
    const fieldNames = fieldConfig.fields
    const fieldLabel = fieldConfig.label
    const fieldValues = fieldNames.map(k => entity[k])
    return {
      names: fieldNames,
      label: fieldLabel,
      values: fieldValues,
      html: fieldValuesToHtml(fieldValues, fieldConfig)
    }
  })

  const html = fields.reduce((acc, field) => {
    return acc + `<p><strong title="">${field.label}:</strong> ${field.html}</p>\n`
  }, '')

  return html
}
