const moreJoin = require('./moreJoin')
const typeConfigMap = window.stratocumulus.contentTypeMap

module.exports = stratum => {
  // Get the current context represented in text.
  //
  // Parameters:
  //   an object, a plain stratum
  //
  // Return
  //   a string. The string is empty for empty context.
  //

  if (!stratum.context || stratum.context.length === 0) {
    // Empty context
    return ''
  }

  const entityContext = stratum.context.filter(c => c.parameter === 'entity')
  if (entityContext.length > 0) {
    // Entity context. Display last entity name only.
    // TODO display all entity data.
    const len = entityContext.length
    const lastEntityContext = entityContext[len - 1]
    return lastEntityContext.valueLabel
  }

  let typeLabel = 'Entities'
  const typeContext = stratum.context.filter(c => c.parameter === 'content_type')
  if (typeContext.length > 0) {
    const len = typeContext.length
    const lastTypeContext = typeContext[len - 1]
    const contentType = lastTypeContext.value
    const typeConfig = typeConfigMap[contentType]
    if (typeConfig) {
      typeLabel = typeConfig.label_plural
    }
  }

  const facetContext = stratum.context.filter(c => {
    return !['content_type', 'entity', 'q'].includes(c.parameter)
  })

  let facetsLabel = ''
  if (facetContext.length > 0) {
    const facetLabels = facetContext.map(c => c.valueLabel)
    const commaList = moreJoin(facetLabels)
    facetsLabel = ' in ' + commaList
  }

  return typeLabel + facetsLabel
}
