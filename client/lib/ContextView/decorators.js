exports.dateToYear = (date) => {
  // Parameters:
  //   date
  //     a string, ISO date
  //
  // Returns
  //   a string, the year
  //
  return date.substring(0, 4)
}

exports.datePlaceToYearPlace = (date, place) => {
  // Combine ISO date string and place object.
  // Useful for dense birth and death records.
  //
  // Parameters:
  //   date
  //     a string, ISO date, or null if not known.
  //   place
  //     a string or object with label property
  //
  // Returns
  //   a string
  //

  // Process date
  let yearLabel = ''
  if (date) {
    yearLabel = exports.dateToYear(date)
  }

  // Process place
  let placeLabel = null
  if (place) {
    if (typeof place === 'string') {
      placeLabel = place
    } else if (typeof place === 'object' && place.label) {
      placeLabel = place.label
    }
  }
  return yearLabel + (placeLabel ? ' in ' + placeLabel : '')
}

exports.yearsToRange = (years) => {
  // Parameter:
  //   years
  //     an array of integers
  //
  // Return
  //   a string
  //
  if (years.length === 0) {
    return '-'
  }
  if (years.length === 1) {
    return '' + years[0]
  }

  // Detect range
  let minYear = years[0]
  let maxYear = years[years.length - 1]

  for (let i = 0; i < years.length; i += 1) {
    if (years[i] < minYear) {
      minYear = years[i]
    }
    if (years[i] > maxYear) {
      maxYear = years[i]
    }
  }

  return minYear + '–' + maxYear
}

exports.latLonToGeoHack = (lat, lon, name) => {
  // Create a GeoHack link like ones used in Wikipedia.
  const url = 'https://geohack.toolforge.org/geohack.php?language=en&' +
    `params=${lat.toFixed(6)};${lon.toFixed(6)}&pagename=${name}`
  const latdir = lat >= 0 ? 'N' : 'S'
  const londir = lon >= 0 ? 'E' : 'W'
  const latlabel = Math.abs(lat) + ' ' + latdir
  const lonlabel = Math.abs(lon) + ' ' + londir
  return `<a href="${url}" target="_blank">${latlabel} ${lonlabel}</a>`
}
