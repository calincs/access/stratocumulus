const template = require('./template.ejs')
const buildContextLabel = require('./buildContextLabel')
const buildDetails = require('./buildDetails')
const buildTypeLabel = require('./buildTypeLabel')
const io = require('../io')

module.exports = function () {
  // @ContextView:render()
  //
  // Render the current context.
  //
  const context = io.store.getState().context

  // Stratum provides human-readable labels.
  io.stratumApi.fetchStratum(context, (err, stratum) => {
    if (err) {
      // TODO show error here?
      console.error(err)
      return
    }

    const contextData = stratum.context || []

    const filters = contextData.map(c => {
      const parameter = c.parameter
      const value = c.value
      // Build a technical title to make debugging easier.
      const title = parameter + '=' + value
      // Build human-readable label
      const parameterLabel = c.parameterLabel
      const valueLabel = c.valueLabel
      // Build className
      const classes = []
      if (c.active) { classes.push('active') }

      return { title, classes, parameter, value, parameterLabel, valueLabel }
    })

    // Build context card title and pick background image.
    const contentType = context.getValue('content_type') || 'mixed'
    let imageUrl = null
    let imageTitle = null
    let entityDetails = null
    // Pick last context item with image attached.
    contextData.forEach(c => {
      if (c.imageUrl) {
        imageUrl = c.imageUrl
        if (c.imageTitle) {
          imageTitle = c.imageTitle
        } else {
          imageTitle = null
        }
      }

      if (c.details) {
        entityDetails = c.details
      }
    })
    // Pack
    const card = {
      contentType,
      typeLabel: buildTypeLabel(stratum),
      label: buildContextLabel(stratum),
      imageUrl,
      imageTitle,
      details: buildDetails(entityDetails)
    }

    // Render
    this.element.innerHTML = template({ filters, card })
  })
}
