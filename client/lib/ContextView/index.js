require('./style.css')
const io = require('../io')

const ContextView = function () {
  // @ContextView()
  //
  // ContextView is a viewer and editor for the current Context.
  //

  // Prepare container
  this.element = document.createElement('div')
  this.element.className = 'context-view'

  // Bind once. Delegate via root element.
  this.element.addEventListener('click', (ev) => {
    ev.preventDefault()

    const dataset = ev.target.dataset
    if (!dataset.filterParam) {
      // Clicked outside active elements.
      return
    }

    if (dataset.action === 'remove') {
      io.store.dispatch({
        type: 'navigation/filter',
        parameter: dataset.filterParam,
        value: null // null to remove the parameter
      })
    }
  })

  // Wait for the first context before the first render.
  // Then, react to context changes.
  io.store.subscribe(io.store.select(state => state.context, () => {
    this.render()
  }))
}

module.exports = ContextView
const proto = ContextView.prototype

proto.getElement = require('./getElement')
proto.render = require('./render')
