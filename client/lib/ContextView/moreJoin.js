module.exports = (arr, max) => {
  // Join array using Oxford comma and truncate extra items with 'and N more'.
  //
  if (!max) {
    max = -1
  }
  if (arr.length === 0) {
    return ''
  }
  if (arr.length === 1) {
    return arr[0]
  }
  if (arr.length === 2) {
    return arr[0] + ' and ' + arr[1]
  }
  if (arr.length === 3) {
    const last = arr[arr.length - 1]
    return arr.slice(0, arr.length - 1).join(', ') + ', and ' + last
  }
  if (max < 0 || arr.length <= max + 1) { // max+1 because '1 more' annoys
    const last = arr[arr.length - 1]
    return arr.slice(0, arr.length - 1).join(', ') + ', and ' + last
  }
  // too many items
  const remainder = arr.length - max
  return arr.slice(0, max).join(', ') + ', and ' + remainder + ' more'
}
