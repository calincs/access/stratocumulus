module.exports = stratum => {
  // Get the current type of the context represented in text.
  //
  // Parameters:
  //   an object, a plain stratum
  //
  // Return
  //   a string. The string is empty for empty context or if the type is already present in the main label.
  //

  if (!stratum.context || stratum.context.length === 0) {
    // Empty context
    return ''
  }

  const entityContext = stratum.context.filter(c => c.parameter === 'entity')
  if (entityContext.length > 0) {
    // Entity context. Display last entity type only.
    const len = entityContext.length
    const last = entityContext[len - 1]
    return last.typeLabel || last.type
  }

  return ''
}
