const decorators = require('./decorators')

module.exports = (values, templateConfig) => {
  // Render an data field of an entity.
  //
  // Parameters:
  //   values
  //     an array of values. Can also be an array of arrays.
  //   templateConfig
  //     an object, the server-provided template configuration for the field.
  //
  // Returns
  //   a string
  //
  if (values.length <= 0) {
    return '-'
  }

  if (values.length === 1) {
    const value = values[0]

    if (value === null) {
      return '-'
    }

    const decoratorName = templateConfig.decorator
    if (decoratorName && decorators[decoratorName]) {
      const decorator = decorators[templateConfig.decorator]
      return decorator(value)
    }

    const valueType = typeof value

    if (valueType === 'string') {
      return value
    }

    if (valueType === 'number') {
      return '' + value
    }

    if (valueType === 'object') {
      if (Array.isArray(value)) {
        if (value.length === 0) {
          return '-'
        }
        if (value[0].label) {
          // Is an array of Corpora cross references.
          // TODO make hyperlinks
          return value.map(ref => ref.label).join(', ')
        }
        return value.join(', ') // TODO use moreJoin
      }

      if (value.label) {
        // Is a single Corpora cross reference object
        return value.label
      }
    }

    // Unknown value type
    return '?'
  }

  // Values argument is array with two or more elements.

  // Arrays of values can have a decorator that combines the values.
  // For example decorator can bake latitude and longitude into a GeoHack link.
  const decoratorName = templateConfig.decorator
  if (decoratorName && decorators[decoratorName]) {
    const decorator = decorators[templateConfig.decorator]
    return decorator.apply(null, values)
  }

  // Map to strings and join.
  return values.map(value => {
    const valueType = typeof value

    if (valueType === 'string') {
      return value
    }

    if (valueType === 'number') {
      return '' + value
    }

    if (valueType === 'object') {
      if (value === null) {
        return '-'
      }

      if (Array.isArray(value)) {
        if (value.length === 0) {
          return '-'
        }
        if (value[0].label) {
          // Is an array of Corpora cross references.
          // TODO make hyperlinks
          return value.map(ref => ref.label).join(', ')
        }
        return value.join(', ') // TODO use moreJoin
      }

      if (value.label) {
        // Is a single Corpora cross reference object
        return value.label
      }
    }

    // Unknown value type
    return '?'
  }).join(', ')
}
