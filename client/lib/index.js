const config = require('./config')
const tapspace = require('tapspace')
const io = require('./io')
const Sky = require('./Sky')
const SidebarManager = require('./SidebarManager')
const ViewportManager = require('./ViewportManager')
const PathManager = require('./PathManager')
const clientVersion = require('./version')

exports.start = function () {
  // DEBUG log messages to help developer to differentiate between:
  // - app bundle is ok but we are offline (ok messages, no UI action)
  // - app bundle is broken (no messages, no UI action)
  // - app bundle is cached (ok messages, old versions)
  console.log('stratocumulus-client v' + clientVersion)
  console.log('tapspace.js v' + tapspace.version)

  // Validate configuration received from the server.
  config.validate()

  // Setup tapspace viewport
  const viewportManager = new ViewportManager()
  const viewport = viewportManager.getViewport()

  // Setup sidebar and behavior
  const sidebarManager = new SidebarManager(viewport) // eslint-disable-line

  // Setup page URL management
  const pathManager = new PathManager()

  // Create loader for a dynamic tree of strata.
  // The loader listens context changes in the state.
  Sky.create(viewport)

  // Begin loading the first stratum by setting the first context.
  // Do this after all components are created, because most of them will render
  // only after the first context change.
  io.store.dispatch({
    type: 'init',
    context: pathManager.readContext()
  })
}
