const findLastIndex = require('./model/findLastIndex')

module.exports = function (selection) {
  // @Context:getLast([selection])
  //
  // Get the last parameter-value pair as an object.
  //
  // Parameters:
  //   selection
  //     optional array of string, the subset of parameters.
  //
  // Return
  //   an object { parameter, value } or null if empty
  //

  // Pick the last parameter by default
  let lastIndex = this.keys.length - 1
  if (Array.isArray(selection)) {
    // A subset given. Find last in the subset.
    lastIndex = findLastIndex(this.keys, selection)
  }

  if (lastIndex < 0) {
    // No parameters found.
    return null
  }

  const parameter = this.keys[lastIndex]
  const value = this.values[lastIndex]

  return { parameter, value }
}
