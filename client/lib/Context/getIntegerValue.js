module.exports = function (param, defaultValue) {
  // @Context:getIntegerValue(param, defaultValue)
  //
  // Get integer from the parameter.
  //
  // Parameters:
  //   param
  //     a string
  //   defaultValue
  //     an integer, the default value to use if the real value is invalid
  //     .. or does not exist.
  //
  // Returns
  //   an integer or the default value, regardless of is the default value
  //   .. an integer or not.
  //
  const i = this.keys.indexOf(param)

  if (i < 0) {
    // Not found, thus default
    return defaultValue
  }

  const valueString = this.values[i]
  const intCandidate = parseInt(valueString, 10)

  if (isNaN(intCandidate)) {
    return defaultValue
  }

  return intCandidate
}
