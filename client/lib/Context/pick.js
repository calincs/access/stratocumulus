module.exports = function (selection) {
  // @Context:pick(selection)
  //
  // Get a subset of this context that includes only the selected parameters.
  //
  // Parameters:
  //   selection
  //     an array of string, the list of selected parameters.
  //
  // Return
  //   a Context
  //

  // Pick selected
  const keys = []
  const values = []
  const len = this.keys.length
  for (let i = 0; i < len; i += 1) {
    const key = this.keys[i]
    if (selection.includes(key)) {
      keys.push(key)
      values.push(this.values[i])
    }
  }

  const Context = this.constructor
  return new Context(keys, values)
}
