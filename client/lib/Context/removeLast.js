const findLastIndex = require('./model/findLastIndex')

module.exports = function (selection) {
  // @Context:removeLast([selection])
  //
  // Remove the last parameter, if any.
  // Useful for building broader context step by step.
  // Creates a new Context.
  //
  // Parameters:
  //   selection
  //     optional array of string. If not given, returns the last parameter
  //     .. of them all. If given, returns the last parameter matching
  //     .. the selection.
  //
  // Return
  //   a Context.
  //

  // Find last parameter.
  let lastIndex = this.keys.length - 1
  if (Array.isArray(selection)) {
    lastIndex = findLastIndex(this.keys, selection)
  }

  if (lastIndex < 0) {
    // No faceting parameters found.
    return this.copy()
  }

  // Copy before splice to maintain immutability.
  const keys = this.keys.slice(0)
  const values = this.values.slice(0)

  keys.splice(lastIndex, 1)
  values.splice(lastIndex, 1)

  const Context = this.constructor
  return new Context(keys, values)
}
