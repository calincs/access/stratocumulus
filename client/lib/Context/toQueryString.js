const encodeURIComponent = window.encodeURIComponent
const cacher = require('../cacher')

module.exports = cacher('queryStringCache', function () {
  // @Context:toQueryString()
  //
  // Get the context as a query string, e.g. "f_genres.id=123&r_years=100to400"
  //
  // Return
  //   a string. Empty context returns empty query string `''`.
  //
  const parts = this.keys.map((key, i) => {
    return key + '=' + encodeURIComponent(this.values[i])
  })

  return parts.join('&')
})
