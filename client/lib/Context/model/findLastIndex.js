module.exports = (keys, selection) => {
  // Find last selected parameter.
  //
  const len = keys.length
  let last = -1
  for (let i = 0; i < len; i += 1) {
    if (selection.includes(keys[i])) {
      last = i
    }
  }

  return last
}
