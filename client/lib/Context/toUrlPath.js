module.exports = function () {
  // @Context:toUrlPath()
  //
  // Build a local URL path and query from the context.
  // A path begins with a slash.
  //
  // Return
  //   a string, for example "/?f_genres.id=123"
  //

  if (this.keys.length === 0) {
    return '/'
  }

  return '/?' + this.toQueryString()
}
