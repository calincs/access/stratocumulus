module.exports = function (selection) {
  // @Context:sort(selection)
  //
  // Get this context where the selected parameters are sorted
  // and placed at the end.
  //
  // Parameters:
  //   selection
  //     an array of string, an ordered list of selected parameters.
  //
  // Return
  //   a Context
  //

  const keys = []
  const values = []

  // Pick non-selected and place them first in their original order.
  const len = this.keys.length
  for (let i = 0; i < len; i += 1) {
    const key = this.keys[i]
    if (!selection.includes(key)) {
      keys.push(key)
      values.push(this.values[i])
    }
  }

  // Pick selected in the given order.
  const sellen = selection.length
  for (let i = 0; i < sellen; i += 1) {
    const key = selection[i]
    const index = this.keys.indexOf(key)
    if (index >= 0) {
      keys.push(key)
      values.push(this.values[index])
    }
  }

  const Context = this.constructor
  return new Context(keys, values)
}
