module.exports = function (param, value) {
  // @Context:hasValue(param, value)
  //
  // Test if given param-value pair exists in the context.
  //
  // Return
  //   boolean
  //
  const len = this.keys.length
  for (let i = 0; i < len; i += 1) {
    if (this.keys[i] === param) {
      if (this.values[i] === value) {
        return true
      }
    }
  }
  return false
}
