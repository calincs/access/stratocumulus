module.exports = function (blacklist) {
  // @Context:drop(blacklist)
  //
  // Get a subset of this context that excludes the blacklisted parameters.
  //
  // Parameters:
  //   blacklist
  //     an array of string, the list of parameters to exclude.
  //
  // Return
  //   a Context
  //

  // Pick non-blacklisted parameters
  const keys = []
  const values = []
  const len = this.keys.length
  for (let i = 0; i < len; i += 1) {
    const key = this.keys[i]
    if (!blacklist.includes(key)) {
      keys.push(key)
      values.push(this.values[i])
    }
  }

  const Context = this.constructor
  return new Context(keys, values)
}
