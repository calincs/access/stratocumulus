const Context = require('../Context')

module.exports = function () {
  // @PathManager:readContext()
  //
  // Read a Context from the current page URL.
  //
  // Returns:
  //   a Context
  //

  // Read the context from the string after the path.
  const query = window.location.search.toString()
  // Strip any leading slashes and question marks to form a plain query string.
  const strippedQuery = query.replace(/^\/?\??/, '')
  // Convert to Context object. This also decodes previously encoded values.
  const context = Context.fromQueryString(strippedQuery)

  return context
}
