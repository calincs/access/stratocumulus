const history = window.history

module.exports = function (context) {
  // @PathManager:writeContext(context)
  //
  // Rewrite the current URL to reflect the given Context.
  //
  // Parameters:
  //   context
  //     a Context
  //

  const query = context.toQueryString()

  let search = ''
  if (query.length > 0) {
    search = '?' + query
  }

  // Current search string for comparison.
  const current = window.location.search.toString()

  // Prevent unnecessary pushing
  if (search === current) {
    // Not changed. Skip.
    return
  }

  const url = '/' + search
  history.pushState({}, '', url)
}
