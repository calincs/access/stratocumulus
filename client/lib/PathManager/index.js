const io = require('../io')

const PathManager = function () {
  // @PathManager
  //
  // The path manager handles URL navigation and browser history.pushState.
  //

  // TODO prevent full page load if user navigates the history
  // window.addEventListener('popstate', (ev) => {
  //   console.log('popstate', ev)
  // })

  io.store.subscribe(io.store.select(state => state.context, (context) => {
    // Update URL
    this.writeContext(context)
  }))
}

module.exports = PathManager
const proto = PathManager.prototype
proto.isPathManager = true

// Methods
proto.readContext = require('./readContext')
proto.writeContext = require('./writeContext')
