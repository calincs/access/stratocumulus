require('./style.css')
const tapspace = require('tapspace')
const io = require('../io')
const Sidebar = require('../Sidebar')
const SidebarHandle = require('../SidebarHandle')
const SidebarLite = require('../SidebarLite')

const SIDEBAR_WIDTH = 400

const SidebarManager = function (viewport) {
  // @SidebarManager(viewport)
  //
  // TODO rename to ControlsManager
  //
  // Parameters:
  //   viewport
  //     a tapspace Viewport
  //
  // The sidebar manager is responsible of interaction between:
  // - Sidebar
  // - SidebarHandle
  // - Slimbar (search tools when sidebar closed)
  //

  // Sidebar panel
  this.sidebar = new Sidebar()
  this.sidebarControl = tapspace.createControl(this.sidebar.getElement())
  this.sidebarControl.setSize(SIDEBAR_WIDTH, window.innerHeight)

  // Sidebar open/close button
  this.sidebarHandle = new SidebarHandle()
  this.sidebarHandleControl = tapspace.createControl(this.sidebarHandle.getElement())
  this.sidebarHandleControl.setSize(25, 65)
  this.sidebarHandleControl.setAnchor(this.sidebarHandleControl.atNorm(0, 0.5))

  // Setup lightweight sidebar
  this.sidebarLite = new SidebarLite()
  this.sidebarLiteControl = tapspace.createControl(this.sidebarLite.getElement())
  this.sidebarLiteControl.setSize(270, 160)

  // Add initial controls according to sidebar visibility
  const uiState = io.store.getState().ui
  if (uiState && uiState.sidebarOpen) {
    // Add sidebar
    viewport.addControl(this.sidebarControl, viewport.at(0, 0))
    // Add sidebar open/close button next to sidebar.
    viewport.addControl(this.sidebarHandleControl, this.sidebarControl.atNorm(1, 0.46))
  } else {
    // Show lightweight sidebar.
    viewport.addControl(this.sidebarLiteControl, viewport.at(12, 12))
    // Add sidebar open/close button at page edge.
    viewport.addControl(this.sidebarHandleControl, viewport.atNorm(0, 0.46))
  }

  // React to sidebar open/close
  io.store.subscribe(io.store.select(state => state.ui.sidebarOpen, (isOpen) => {
    if (isOpen) {
      try {
        // TODO tapspace: make removeControl idempotent or add hasControl method
        viewport.removeControl(this.sidebarLiteControl)
      } catch (e) {
        // noop
      }
      viewport.addControl(this.sidebarControl)
      this.sidebarControl.setSize(400, window.innerHeight)
      this.sidebarHandleControl.translateTo(this.sidebarControl.atNorm(1, 0.46))
    } else {
      try {
        // TODO tapspace: make removeControl idempotent or add hasControl method
        viewport.removeControl(this.sidebarControl)
      } catch (e) {
        // noop
      }
      this.sidebarControl.setSize(0, 0)
      this.sidebarHandleControl.translateTo(viewport.atNorm(0, 0.46))
      viewport.addControl(this.sidebarLiteControl, viewport.at(12, 12))
    }
  }))

  // Keep sidebar in full height.
  viewport.on('resize', () => {
    this.sidebarControl.setSize(SIDEBAR_WIDTH, window.innerHeight)
  })
}

module.exports = SidebarManager
const proto = SidebarManager.prototype
proto.isSidebarManager = true

// Methods
// ? proto.getSidebar = require('./getSidebar')
// ? proto.openSidebar = require('./openSidebar')
