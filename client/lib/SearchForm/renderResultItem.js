const template = require('./resultItem.ejs')

const cleanHint = (hint) => {
  return hint.replace('_', ' ')
}

module.exports = (item, data) => {
  // renderResultItem(item, data)
  //
  // Parameters:
  //   item
  //     an HTMLElement, the container element to populate.
  //   data
  //     an object with properties:
  //       match
  //         a string
  //       value
  //         an object with properties:
  //           field
  //             a string
  //
  const itemLabel = data.match // searchEngine marked result
  const fieldLabel = cleanHint(data.value.hint)

  // NOTE exact values
  // const fieldName = data.value.field
  // const fieldValue = data.value.match

  // TODO normalize plurals
  // if (fieldLabel.endsWith('s')) {
  //   fieldLabel = fieldLabel.substring(0, fieldLabel.length - 1)
  // }

  item.style.display = 'flex'
  item.style.justifyContent = 'space-between'
  item.innerHTML = template({ itemLabel, fieldLabel })
}
