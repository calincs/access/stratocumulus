module.exports = (query) => {
  // @SearchForm:findNodes(query)
  //
  // Find facet nodes that have label matching the query.
  //
  // Parameters:
  //   query
  //     a string
  //
  // Return
  //   array of StratumNode
  //

  // Normalize
  const normQuery = query.toLowerCase().trim()

  // Query DOM for nodes on current stratum matching user query
  // to add to suggestions.
  const qs = 'div.current-stratum div.category-node'
  const currentNodes = Array.from(document.querySelectorAll(qs))
  const currentMatchingNodes = currentNodes.filter(n => {
    const label = n.dataset.label
    return label.toLowerCase().includes(normQuery)
  })

  // Find secondary suggestions from other strata
  const qsAll = 'div.category-node'
  const allNodes = Array.from(document.querySelectorAll(qsAll))
  const allMatchingNodes = allNodes.filter(n => {
    const label = n.dataset.label
    if (label.toLowerCase().includes(normQuery)) {
      // Matching node. Skip duplicates and similar nodes.
      return currentMatchingNodes.every(nn => nn.dataset.label !== label)
    }
    return false
  })

  // Combine matching nodes
  return currentMatchingNodes.concat(allMatchingNodes)
}
