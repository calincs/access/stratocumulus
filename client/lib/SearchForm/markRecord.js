module.exports = (query, record) => {
  // markRecord(query, record)
  //
  // Add a mark element around the part of the record that matches the query.
  // Useful to emphasize matched parts of search results.
  //
  // Parameters:
  //   query
  //     a string
  //   record
  //     a string
  //
  // Return
  //   a string, the given record with added HTML
  //
  const len = query.length
  const markStart = record.toLowerCase().indexOf(query.toLowerCase())
  if (markStart > -1) {
    const beforeMatch = record.slice(0, markStart)
    const matchedText = record.slice(markStart, len + markStart)
    const afterMatch = record.slice(len + markStart, record.length)
    return beforeMatch + '<mark>' + matchedText + '</mark>' + afterMatch
  }
  return record
}
