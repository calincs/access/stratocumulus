const AutoComplete = require('@tarekraafat/autocomplete.js')
const renderResultItem = require('./renderResultItem')
const markRecord = require('./markRecord')
const fetchSuggestions = require('./fetchSuggestions')
const Context = require('../Context')
const io = require('../io')

module.exports = function () {
  // @SearchForm:initialize()
  //
  // Initialize the autocomplete component and related event listeners.
  // This must be called once after element is mounted to the DOM.
  //

  // About to init. Make visible.
  this.element.style.removeProperty('display')

  this.autocomplete = new AutoComplete({
    selector: '#strato-autocomplete',
    data: {
      src: async (query) => {
        const context = io.store.getState().context || new Context()
        const suggestions = await fetchSuggestions(query, context)
        return suggestions
      },
      keys: ['match']
    },
    threshold: 2, // number of characters
    resultsList: {
      maxResults: 10
    },
    resultItem: {
      element: (item, data) => {
        renderResultItem(item, data)
      },
      highlight: true
    },
    searchEngine: (query, record) => {
      // Handler for each user input where
      //   query is a string, the user input text.
      //   record is a string, the full text containing the matching part.
      // Emphasize the query in result labels. This cannot be done in
      // result item renderer because it does not know the query.
      return markRecord(query, record)
    },
    events: {
      input: {
        focus: () => {
          if (this.element.value.length) {
            this.autocomplete.start()
          }
        }
      }
    }
  })

  // The event that gets fired when a user selects
  // a dropdown autocomplete suggestion.
  this.element.addEventListener('selection', (event) => {
    this.autocompleteSelected = true
    const suggestionMatch = event.detail.selection.value.match
    const suggestionField = event.detail.selection.value.field

    // Discard any previous input to prevent cognitive dissonance by
    // acting like a title for the results.
    // TODO update to current keyword filter?
    this.element.value = ''

    // Search for such node.

    // Assuming now that suggestion is a navigable facet node,
    // find matching node in DOM and retrieve
    // facet param and facet value necessary for navigation
    // TODO The node is already found via findNodes. No need to find it again.
    // TODO Therefore remove the following lines, detect that it was a node
    // TODO from some data in selection like 'selection.value.type' and
    // TODO submit navigation/node directly.
    const selector = 'div.category-node' +
      `[data-label='${suggestionMatch}']` // BUG case-sensitive
    const navNode = document.querySelector(selector)

    if (navNode) {
      // Submit
      io.store.dispatch({
        type: 'navigation/node',
        label: navNode.dataset.label
      })
    } else {
      // General parameter / value
      io.store.dispatch({
        type: 'navigation/filter',
        parameter: suggestionField,
        value: suggestionMatch
      })
    }
  })

  // Do free keyword search when the user hits enter in the search box
  // instead of selecting one of the suggestions.
  this.element.addEventListener('keyup', (event) => {
    if (this.autocompleteSelected) {
      this.autocompleteSelected = false
    } else if (event.key === 'Enter') {
      // Submit
      io.store.dispatch({
        type: 'filter/keyword',
        keyword: this.element.value
      })
    }
  })
}
