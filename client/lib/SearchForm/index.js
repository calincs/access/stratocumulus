require('@tarekraafat/autocomplete.js/dist/css/autoComplete.css')
require('./style.css')
const emitter = require('component-emitter')

const SearchForm = function () {
  // @SearchForm()
  //
  // A search bar with autocompletion.
  //
  // Emits
  //   submit
  //     when a non-empty search keyword is submitted
  //

  // Construct input element
  this.element = document.createElement('input')
  this.element.id = 'strato-autocomplete'
  // Hide until initialization to prevent flicker.
  this.element.style.display = 'none'

  // Autocompletion
  this.autocomplete = null
  this.autocompleteSelected = false

  // Automatic DOM init. Required by AutoComplete package.
  const initTimeout = setInterval(() => {
    if (this.element.closest('body') === document.body) {
      // Element in DOM. Now init once.
      clearTimeout(initTimeout)
      this.initialize()
    }
  }, 100)
}

module.exports = SearchForm
const proto = SearchForm.prototype

// Inherit
emitter(proto)

// Methods
proto.getElement = require('./getElement')
proto.initialize = require('./initialize')
