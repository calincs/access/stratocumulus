const findNodes = require('./findNodes')
const findEntities = require('./findEntities')
const contentTypeMap = window.stratocumulus.contentTypeMap
const fetch = window.fetch

// Track and limit concurrent fetch requests.
let lastAutocompleteRequest = Date.now()

module.exports = async (query, context) => {
  // fetchSuggestions(query, context)
  //
  // Fetch an array of suggestions from various sources. Asynchronic.
  //
  // Parameters:
  //   query
  //     a string, the search keyword
  //   context
  //     a Context, the current faceting and filtering context
  //
  // Return:
  //   a Promise with an array of suggestion objects { field, match } where
  //     field
  //       a string, the corpora index field of the matched content
  //     match
  //       a string, the full value of the content including the match.
  //
  const currentAutocompleteRequest = Date.now()
  if (currentAutocompleteRequest < lastAutocompleteRequest) {
    return // The query has already changed. Dismiss the response.
  }
  lastAutocompleteRequest = currentAutocompleteRequest

  // Let's build a list of suggestions
  const suggestions = []

  findNodes(query).forEach(n => {
    suggestions.push({
      hint: n.dataset.field,
      field: n.dataset.field,
      match: n.dataset.label // node label
    })
  })

  findEntities(query).forEach(en => {
    suggestions.push({
      hint: en.dataset.contentType,
      field: 'content_type',
      match: en.dataset.fulltext
    })
  })

  // For backend suggestions, we need a content type.
  const contentType = context.getValue('content_type')
  if (!contentType) {
    // TODO search for all content types if no content type is set.
    return suggestions
  }

  // Search suggestions using facets and filters supported by the content type.
  const typeConfig = contentTypeMap[contentType]
  if (!typeConfig) {
    // Bad content type.
    return suggestions
  }
  const supportedFacets = typeConfig.facets.map(facet => facet.parameter)
  const supportedFilters = typeConfig.filters.map(fil => fil.parameter)
  const subcontextFacets = context.pick(supportedFacets)
  const subcontextFilters = context.pick(supportedFilters)
  const supportedContext = subcontextFacets.merge(subcontextFilters)

  // Apply the current context to filter the autocomplete results.
  // See lib/Context/toQueryString for details.
  let contextQuery = ''
  if (supportedContext.length > 0) {
    contextQuery = '&' + supportedContext.toQueryString()
  }

  // Request autocomplete suggestions from Corpora's "suggest" API.
  const requestPath = `${typeConfig.content_type}/suggest/`
  const requestUrl = `${requestPath}?q=${query}${contextQuery}`
  // Async requests
  const request = await fetch(requestUrl)
  const findings = await request.json()
  // Assume findings is an object { <field>: <array of string> }

  // Add to suggestions any results from Corpora "suggest" API
  Object.keys(findings).forEach(field => {
    findings[field].forEach(match => {
      suggestions.push({ hint: field, field, match })
    })
  })

  return suggestions
}
