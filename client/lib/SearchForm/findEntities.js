module.exports = (query) => {
  // @SearchForm:findEntities(query)
  //
  // Find facet nodes that have label matching the query.
  //
  // Parameters:
  //   query
  //     a string
  //
  // Return
  //   array of StratumNode
  //

  // Normalize
  const normQuery = query.toLowerCase().trim()

  // Search for entity content
  const qe = 'div.current-stratum div.entity-node'
  const currentEntities = Array.from(document.querySelectorAll(qe))
  const currentMatchingEntities = currentEntities.filter(en => {
    const fulltext = en.dataset.fulltext
    return fulltext.toLowerCase().includes(normQuery)
  })

  // TODO find entities from non-current strata?
  // TODO find entities from visible strata?

  return currentMatchingEntities
}
