const Context = require('./Context')
const queryParameters = window.stratocumulus.queryParameters
const facetParameters = window.stratocumulus.facetParameters

exports.super = ctx => {
  // Get supercontext. Can be null.
  //

  // The root context does not have supercontext.
  const facetContext = ctx.pick(facetParameters)
  if (facetContext.length === 0) {
    return null
  }

  // Backtrack to broader set of facets.
  // Preserve filters.
  return ctx.removeLast(facetParameters)
}

exports.sub = (ctx, param, value) => {
  // Get subcontext. Can be null.
  //

  // Skip unknown parameters.
  if (!queryParameters.includes(param)) {
    return null
  }

  // Add the new param to the end.
  return ctx.append(param, value)
}

exports.warp = (ctx, param, value) => {
  // Jump to a wide facet context.
  //
  // Parameters:
  //   ctx
  //     a Context
  //   param
  //     a string
  //   value
  //     a string
  //
  // Return
  //   a Context
  //

  // We try to preserve the context as much as possible
  // so that the jump is semantically as short as possible.
  //
  // However, if the context tail points to a related entity
  // of another entity, none of the context make sense.
  // For example, let's say the user first reached a person by faceting
  // and then navigated to a creative work created by that person.
  // The faceting context that points to the person does not apply
  // to the creative work at all.
  // This is true even if instead of a creative work the related entity
  // is also a person. The facets that pointed to the first person might
  // not anymore apply to this related person.
  //
  // Therefore we can preserve the context only for the first entity.
  // For the related entities, we must discard the context in full.
  const entityContext = ctx.pick(['entity'])
  const numEntities = entityContext.length

  if (numEntities === 0) {
    // Weird situation because warping is for entities.
    console.warn('Bad warp context. Missing entity parameter in ' + ctx.toUrlPath())
    return ctx
  }

  if (numEntities === 1) {
    // This entity was reached via faceting.
    // We can preserve much of the faceting context.

    // Remove the entity from the context.
    // Remove old values of the new parameter.
    const stripped = ctx.remove('entity').remove(param)
    return stripped.append(param, value)
  }
  // Implied: numEntities >= 1

  // The current entity was reached via relations.
  // We cannot preserve the faceting context.
  // We need to replace the content type to match the entity.
  const tailEntity = entityContext.getLast()
  const entitySelector = tailEntity.value
  const typeAndId = exports.extractEntitySelector(entitySelector)
  if (!typeAndId) {
    console.warn('Bad entity selector string: ' + entitySelector)
    return ctx
  }
  const entityType = typeAndId.type
  // Discard old context; create new context.
  return new Context(['content_type', param], [entityType, value])
}

exports.equal = (ca, cb) => {
  // Test if two context are the same in navigational sense.
  //
  // Return
  //   boolean
  //
  const fa = ca.pick(facetParameters)
  const fb = cb.pick(facetParameters)
  return fa.equals(fb)
}

exports.fromUrl = (url) => {
  // Sanitize a URL and convert to context.
  //
  return Context.fromUrlPath(url).pick(queryParameters)
}

exports.extractEntitySelector = (queryValue) => {
  // Extract entity type and id from the query value.
  //
  // Parameters:
  //   queryValue
  //     a string, e.g. 'Thing-6595be681a18712f1f6b135b'
  //
  // Return:
  //   an object { type, id } or null if not valid.
  //
  const parts = queryValue.split('-')
  if (parts.length < 2) {
    return null
  }
  return {
    type: parts[0],
    id: parts[1]
  }
}
