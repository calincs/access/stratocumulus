const template = require('./template.ejs')
const io = require('../io')
const contentTypeMap = window.stratocumulus.contentTypeMap
const contextHelper = require('../contextHelper')

const defaultListOrder = {
  property: 'count',
  direction: 'desc'
}

module.exports = function () {
  // @ListView:render()
  //
  // Render current context as a list.
  //
  const context = io.store.getState().context

  io.stratumApi.fetchStratum(context, (err, stratum) => {
    if (err) {
      console.error(err)
      this.element.innerHTML = `<p class="list-view-error">Error: ${err.message}</p>`
      return
    }

    const listItems = stratum.nodes

    // Upgrade items with URLs
    listItems.forEach(node => {
      node.url = '#'
      if (node.role !== 'grouping') {
        // Read facet properties
        const facetParam = node.facetParam
        const facetValue = '' + node.facetValue
        // Determine next context. Prevent duplicated context.
        let nodeContext
        if (context.hasValue(facetParam, facetValue)) {
          // Duplicate prevented.
          nodeContext = context
        } else {
          if (node.role === 'warp') {
            // Warp up.
            nodeContext = contextHelper.warp(context, facetParam, facetValue)
          } else {
            // Continue deeper.
            nodeContext = contextHelper.sub(context, facetParam, facetValue)
          }
        }
        node.url = nodeContext.toUrlPath()
      }
    })

    // Gather stratum nodes into groups.
    const listGroups = listItems.reduce((groups, item) => {
      if (item.role === 'grouping') {
        if (item.id in groups) {
          // Ensure label exists
          if (!groups[item.id].label) {
            groups[item.id].label = item.label
            groups[item.id].labelPrefix = item.labelPrefix
          }
        } else {
          // Init group
          groups[item.id] = {
            label: item.label,
            labelPrefix: item.labelPrefix,
            items: []
          }
        }

        return groups
      } // else

      if (item.parent) {
        if (item.parent in groups) {
          groups[item.parent].items.push(item)
        } else {
          // Group does not yet exist. It might not exist ever.
          let groupLabel = null
          const groupLabelPrefix = null

          if (item.role === 'entity') {
            const typeConfig = contentTypeMap[item.type]
            if (typeConfig) {
              // Select plural.
              groupLabel = typeConfig.label_plural
            } else {
              // Fall back to item type
              groupLabel = item.type
            }
          }

          groups[item.parent] = {
            label: groupLabel,
            labelPrefix: groupLabelPrefix,
            items: [item]
          }
        }
      }

      return groups
    }, {})

    // Convert the map of groups to a list of groups.
    const groupList = Object.keys(listGroups).map(groupId => {
      const group = listGroups[groupId]
      return {
        id: groupId,
        label: group.label,
        labelPrefix: group.labelPrefix,
        count: group.items.length, // for sorting
        items: group.items
      }
    })

    // HACK the content type label is null because the root node was removed
    // TODO hide the root node instead of removal
    groupList.forEach(group => {
      if (!group.label) {
        group.label = 'Type'
        group.labelPrefix = 'Entities by'
      }
    })

    // Find labels for facet count units.
    const contentTypeToUnit = window.stratocumulus.contentTypes.reduce((acc, config) => {
      acc[config.content_type] = {
        singular: config.label_singular.toLowerCase(),
        plural: config.label_plural.toLowerCase()
      }
      return acc
    }, {})
    // HACK entities count connections
    contentTypeToUnit.Connection = {
      singular: 'linked entity',
      plural: 'linked entities'
    }
    contentTypeToUnit.Country = {
      singular: 'country',
      plural: 'countries'
    }
    contentTypeToUnit.Decade = {
      singular: 'decade',
      plural: 'decades'
    }
    contentTypeToUnit.NamedGraph = {
      singular: 'graph',
      plural: 'graphs'
    }
    contentTypeToUnit.Profession = {
      singular: 'profession',
      plural: 'professions'
    }
    contentTypeToUnit.Class = {
      singular: 'class',
      plural: 'classes'
    }
    contentTypeToUnit.Type = {
      singular: 'genre',
      plural: 'genres'
    }
    contentTypeToUnit.Genre = contentTypeToUnit.Type
    contentTypeToUnit.Material = {
      singular: 'material',
      plural: 'materials'
    }
    // Deal units to items
    groupList.forEach(group => {
      group.items.forEach(item => {
        const unit = contentTypeToUnit[item.contentType]
        if (unit) {
          item.unit = (item.count === 1 ? unit.singular : unit.plural)
        } else {
          console.warn('No unit data found for ' + item.contentType)
        }
      })
    })

    // Compute the percentage share of the total group size for each item.
    groupList.forEach(group => {
      const countSum = group.items.reduce((acc, item) => {
        // Unknown counts are -1
        if (item.count > 0) {
          return acc + item.count
        }
        return acc + 1
      }, 0)

      group.items.forEach(item => {
        item.countRatio = item.count / countSum
      })
    })

    // Sort the items within each group according to set list order.
    const listOrder = io.store.getState().ui.listOrder || defaultListOrder

    const orderProp = listOrder.property
    const dir = (listOrder.direction === 'asc' ? 1 : -1)
    let compareFn
    if (['label'].includes(orderProp)) {
      // natural language properties
      compareFn = (a, b) => dir * a[orderProp].localeCompare(b[orderProp])
    } else if (['count'].includes(orderProp)) {
      // numeric properties
      compareFn = (a, b) => dir * (a[orderProp] - b[orderProp])
    }
    // Sort items within groups. Do not sort the groups themselves; keep their order constant.
    groupList.forEach(group => group.items.sort(compareFn))

    // Collapse lists if there are lots of items and multiple groups.
    const collapse = listItems.length > 10 && groupList.length > 1

    // List order buttons
    const listOrderButtons = [
      {
        property: 'label',
        direction: 'asc',
        title: 'Sort by title, ascending',
        icon: 'fa-solid fa-arrow-down-a-z'
      },
      {
        property: 'label',
        direction: 'desc',
        title: 'Sort by title, descending',
        icon: 'fa-solid fa-arrow-up-z-a'
      },
      {
        property: 'count',
        direction: 'asc',
        title: 'Sort by count, ascending',
        icon: 'fa-solid fa-arrow-down-short-wide'
      },
      {
        property: 'count',
        direction: 'desc',
        title: 'Sort by count, descending',
        icon: 'fa-solid fa-arrow-up-wide-short'
      }
    ]
    listOrderButtons.forEach(btn => {
      if (btn.property === listOrder.property && btn.direction === listOrder.direction) {
        btn.active = true
      }
    })

    // Pick a suitable label for the overall result type.
    // Determine the label from the context.
    let resultLabel = 'Results'
    if (stratum.context.some(c => c.parameter === 'entity')) {
      resultLabel = 'Related'
    }

    this.element.innerHTML = template({
      resultLabel,
      count: stratum.count,
      groupList,
      listOrderButtons,
      collapse
    })
  })
}
