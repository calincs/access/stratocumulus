require('./style.css')
const io = require('../io')
const contextHelper = require('../contextHelper')

const ListView = function () {
  // @ListView
  //
  // Render the stratum as a list.
  //

  // Prepare container
  this.element = document.createElement('div')
  this.element.className = 'list-view'
  this.element.innerHTML = '<p class="list-view-loading">Loading...</p>'

  // Delegate click events via root element.
  // Bind UI events once here instead of at every render to avoid duplicate handling.
  // Groups are collapsed by default.
  this.element.addEventListener('click', ev => {
    if (ev.target.classList.contains('group-heading')) {
      // Prevent anchor action.
      ev.preventDefault()
      // Find collapsible list
      const groupListItem = ev.target.parentElement
      // Open or collapse the list of facets.
      groupListItem.classList.toggle('collapsed')
    }

    const target = ev.target.closest('.facet-heading')
    if (target) {
      // Prevent anchor action.
      ev.preventDefault()
      // Read context from href. Note that the browser might have expanded it to http://
      const nextContext = contextHelper.fromUrl(target.href)
      const nextStratumPath = nextContext.toUrlPath()
      const prevContext = io.store.getState().context
      const prevStratumPath = prevContext.toUrlPath()
      // Signal about the change.
      io.store.dispatch({
        type: 'navigation/link',
        path: nextStratumPath,
        context: nextContext,
        previousPath: prevStratumPath
      })
    }
  })

  this.element.addEventListener('click', ev => {
    const target = ev.target.closest('button')
    if (target) {
      const data = target.dataset
      if ('orderProperty' in data && 'orderDirection' in data) {
        const action = {
          type: 'list/sort',
          property: data.orderProperty,
          direction: data.orderDirection
        }
        io.store.dispatch(action)
      }
    }
  })

  // Wait for the first context before the first render.
  io.store.subscribe(io.store.select(state => state.context, (context) => {
    // Content changed.
    this.render()
  }))

  io.store.subscribe(io.store.select(state => state.ui.listOrder, (listOrder) => {
    // List order changed
    this.render()
  }))
}

module.exports = ListView
const proto = ListView.prototype
proto.isListView = true

// Methods
proto.getElement = require('./getElement')
proto.render = require('./render')
