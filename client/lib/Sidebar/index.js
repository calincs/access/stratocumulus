require('./style.css')
const SearchForm = require('../SearchForm')
const ContextView = require('../ContextView')
const ListView = require('../ListView')
const ExportTools = require('../ExportTools')

const Sidebar = function () {
  // @Sidebar()
  //
  // A component for search tools and list view.
  //
  this.element = document.createElement('div')
  this.element.className = 'sidebar'

  // Search bar
  this.searchForm = new SearchForm()
  const searchFormEl = this.searchForm.getElement()
  searchFormEl.classList.add('sidebar-row')
  this.element.appendChild(searchFormEl)

  // Context viewer
  this.contextView = new ContextView()
  const contextViewEl = this.contextView.getElement()
  contextViewEl.classList.add('sidebar-row')
  this.element.appendChild(contextViewEl)

  // List view
  this.listView = new ListView()
  const listViewEl = this.listView.getElement()
  listViewEl.classList.add('sidebar-row')
  this.element.appendChild(listViewEl)

  // Export tools
  this.exportTools = new ExportTools()
  const exportToolsEl = this.exportTools.getElement()
  exportToolsEl.classList.add('sidebar-row')
  this.element.appendChild(exportToolsEl)
}

module.exports = Sidebar
const proto = Sidebar.prototype
proto.isSidebar = true

// Methods
proto.getElement = require('./getElement')
