# This Dockerfile is targeted for production.
# Dockerfiles for development reside in subdirectories.

# Build frontend with Nodejs + Webpack
FROM node:20-alpine as client
USER node
WORKDIR /home/node/client
# Install dependencies
COPY --chown=node:node client/package*.json .
RUN npm install
# Copy sources for build. Note .dockerignore might exclude some files.
COPY --chown=node:node client/webpack.config.js client/index.js ./
COPY --chown=node:node client/lib ./lib
COPY --chown=node:node client/themes ./themes
RUN npm run build

# Stratocumulus server
FROM node:20-alpine
USER node
WORKDIR /home/node/server
# Install dependencies.
COPY --chown=node:node server/package*.json .
RUN npm install
# Copy sources and config
COPY --chown=node:node ./server/src ./src
COPY --chown=node:node ./corpus-config.yml /home/node/corpus-config.yml
# Copy client
COPY --from=client /home/node/client/dist /usr/src/static

EXPOSE 8000

CMD ["node", "src/index.js"]
